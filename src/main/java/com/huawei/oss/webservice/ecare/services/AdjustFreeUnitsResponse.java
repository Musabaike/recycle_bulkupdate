
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResultOfOperationReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *         &lt;element name="AdjustFreeUnitsReply" type="{http://oss.huawei.com/webservice/ecare/services}FreeAdjustResultValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultOfOperationReply",
    "adjustFreeUnitsReply"
})
@XmlRootElement(name = "adjustFreeUnitsResponse")
public class AdjustFreeUnitsResponse {

    @XmlElement(name = "ResultOfOperationReply", required = true)
    protected ResultOfOperationValue resultOfOperationReply;
    @XmlElement(name = "AdjustFreeUnitsReply", required = true)
    protected FreeAdjustResultValue adjustFreeUnitsReply;

    /**
     * Gets the value of the resultOfOperationReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getResultOfOperationReply() {
        return resultOfOperationReply;
    }

    /**
     * Sets the value of the resultOfOperationReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setResultOfOperationReply(ResultOfOperationValue value) {
        this.resultOfOperationReply = value;
    }

    /**
     * Gets the value of the adjustFreeUnitsReply property.
     * 
     * @return
     *     possible object is
     *     {@link FreeAdjustResultValue }
     *     
     */
    public FreeAdjustResultValue getAdjustFreeUnitsReply() {
        return adjustFreeUnitsReply;
    }

    /**
     * Sets the value of the adjustFreeUnitsReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link FreeAdjustResultValue }
     *     
     */
    public void setAdjustFreeUnitsReply(FreeAdjustResultValue value) {
        this.adjustFreeUnitsReply = value;
    }

}
