
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCorpAPNMembersResultValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCorpAPNMembersResultValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="corpAPNMembers" type="{http://oss.huawei.com/webservice/ecare/services}CorpAPNMemberValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCorpAPNMembersResultValue", propOrder = {
    "corpAPNMembers"
})
public class GetCorpAPNMembersResultValue {

    protected List<CorpAPNMemberValue> corpAPNMembers;

    /**
     * Gets the value of the corpAPNMembers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the corpAPNMembers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorpAPNMembers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorpAPNMemberValue }
     * 
     * 
     */
    public List<CorpAPNMemberValue> getCorpAPNMembers() {
        if (corpAPNMembers == null) {
            corpAPNMembers = new ArrayList<CorpAPNMemberValue>();
        }
        return this.corpAPNMembers;
    }

}
