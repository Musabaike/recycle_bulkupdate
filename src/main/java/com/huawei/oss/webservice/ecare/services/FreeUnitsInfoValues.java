
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FreeUnitsInfoValues complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FreeUnitsInfoValues"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FreeUnitsInfoValue" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="fuType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="fuTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="fuService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="fuTotalAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="fuLeftAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="fuUsedAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="fuMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="startBillCycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="endBillCycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="freeSourceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FreeUnitsInfoValues", propOrder = {
    "freeUnitsInfoValue"
})
public class FreeUnitsInfoValues {

    @XmlElement(name = "FreeUnitsInfoValue")
    protected List<FreeUnitsInfoValues.FreeUnitsInfoValue> freeUnitsInfoValue;

    /**
     * Gets the value of the freeUnitsInfoValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freeUnitsInfoValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreeUnitsInfoValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FreeUnitsInfoValues.FreeUnitsInfoValue }
     * 
     * 
     */
    public List<FreeUnitsInfoValues.FreeUnitsInfoValue> getFreeUnitsInfoValue() {
        if (freeUnitsInfoValue == null) {
            freeUnitsInfoValue = new ArrayList<FreeUnitsInfoValues.FreeUnitsInfoValue>();
        }
        return this.freeUnitsInfoValue;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="fuType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="fuTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="fuService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="fuTotalAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="fuLeftAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="fuUsedAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="fuMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="startBillCycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="endBillCycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="freeSourceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fuType",
        "fuTypeName",
        "fuService",
        "fuTotalAmount",
        "fuLeftAmount",
        "fuUsedAmount",
        "fuMeasure",
        "startBillCycle",
        "endBillCycle",
        "freeSourceId"
    })
    public static class FreeUnitsInfoValue {

        protected String fuType;
        protected String fuTypeName;
        protected String fuService;
        protected String fuTotalAmount;
        protected String fuLeftAmount;
        protected String fuUsedAmount;
        protected String fuMeasure;
        protected String startBillCycle;
        protected String endBillCycle;
        protected String freeSourceId;

        /**
         * Gets the value of the fuType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuType() {
            return fuType;
        }

        /**
         * Sets the value of the fuType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuType(String value) {
            this.fuType = value;
        }

        /**
         * Gets the value of the fuTypeName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuTypeName() {
            return fuTypeName;
        }

        /**
         * Sets the value of the fuTypeName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuTypeName(String value) {
            this.fuTypeName = value;
        }

        /**
         * Gets the value of the fuService property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuService() {
            return fuService;
        }

        /**
         * Sets the value of the fuService property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuService(String value) {
            this.fuService = value;
        }

        /**
         * Gets the value of the fuTotalAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuTotalAmount() {
            return fuTotalAmount;
        }

        /**
         * Sets the value of the fuTotalAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuTotalAmount(String value) {
            this.fuTotalAmount = value;
        }

        /**
         * Gets the value of the fuLeftAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuLeftAmount() {
            return fuLeftAmount;
        }

        /**
         * Sets the value of the fuLeftAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuLeftAmount(String value) {
            this.fuLeftAmount = value;
        }

        /**
         * Gets the value of the fuUsedAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuUsedAmount() {
            return fuUsedAmount;
        }

        /**
         * Sets the value of the fuUsedAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuUsedAmount(String value) {
            this.fuUsedAmount = value;
        }

        /**
         * Gets the value of the fuMeasure property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuMeasure() {
            return fuMeasure;
        }

        /**
         * Sets the value of the fuMeasure property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuMeasure(String value) {
            this.fuMeasure = value;
        }

        /**
         * Gets the value of the startBillCycle property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStartBillCycle() {
            return startBillCycle;
        }

        /**
         * Sets the value of the startBillCycle property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStartBillCycle(String value) {
            this.startBillCycle = value;
        }

        /**
         * Gets the value of the endBillCycle property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndBillCycle() {
            return endBillCycle;
        }

        /**
         * Sets the value of the endBillCycle property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndBillCycle(String value) {
            this.endBillCycle = value;
        }

        /**
         * Gets the value of the freeSourceId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFreeSourceId() {
            return freeSourceId;
        }

        /**
         * Sets the value of the freeSourceId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFreeSourceId(String value) {
            this.freeSourceId = value;
        }

    }

}
