
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChangePrepaidToPostpaidReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *         &lt;element name="NewAccoutReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfCreateNewSubscriberValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "changePrepaidToPostpaidReply",
    "newAccoutReply"
})
@XmlRootElement(name = "changePrepaidToPostpaidResponse")
public class ChangePrepaidToPostpaidResponse {

    @XmlElement(name = "ChangePrepaidToPostpaidReply", required = true)
    protected ResultOfOperationValue changePrepaidToPostpaidReply;
    @XmlElement(name = "NewAccoutReply", required = true)
    protected ResultOfCreateNewSubscriberValue newAccoutReply;

    /**
     * Gets the value of the changePrepaidToPostpaidReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getChangePrepaidToPostpaidReply() {
        return changePrepaidToPostpaidReply;
    }

    /**
     * Sets the value of the changePrepaidToPostpaidReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setChangePrepaidToPostpaidReply(ResultOfOperationValue value) {
        this.changePrepaidToPostpaidReply = value;
    }

    /**
     * Gets the value of the newAccoutReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfCreateNewSubscriberValue }
     *     
     */
    public ResultOfCreateNewSubscriberValue getNewAccoutReply() {
        return newAccoutReply;
    }

    /**
     * Sets the value of the newAccoutReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfCreateNewSubscriberValue }
     *     
     */
    public void setNewAccoutReply(ResultOfCreateNewSubscriberValue value) {
        this.newAccoutReply = value;
    }

}
