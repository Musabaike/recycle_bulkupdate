
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerRelationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerRelationInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="relaType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaName3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaName4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaAddr8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaTel1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaTel2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaTel3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaTel4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relaFax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerRelationInfo", propOrder = {
    "relaType",
    "relaPriority",
    "relaName1",
    "relaName2",
    "relaName3",
    "relaName4",
    "relaAddr1",
    "relaAddr2",
    "relaAddr3",
    "relaAddr4",
    "relaAddr5",
    "relaAddr6",
    "relaAddr7",
    "relaAddr8",
    "relaTel1",
    "relaTel2",
    "relaTel3",
    "relaTel4",
    "relaEmail",
    "relaFax"
})
public class CustomerRelationInfo {

    protected String relaType;
    protected String relaPriority;
    protected String relaName1;
    protected String relaName2;
    protected String relaName3;
    protected String relaName4;
    protected String relaAddr1;
    protected String relaAddr2;
    protected String relaAddr3;
    protected String relaAddr4;
    protected String relaAddr5;
    protected String relaAddr6;
    protected String relaAddr7;
    protected String relaAddr8;
    protected String relaTel1;
    protected String relaTel2;
    protected String relaTel3;
    protected String relaTel4;
    protected String relaEmail;
    protected String relaFax;

    /**
     * Gets the value of the relaType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaType() {
        return relaType;
    }

    /**
     * Sets the value of the relaType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaType(String value) {
        this.relaType = value;
    }

    /**
     * Gets the value of the relaPriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaPriority() {
        return relaPriority;
    }

    /**
     * Sets the value of the relaPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaPriority(String value) {
        this.relaPriority = value;
    }

    /**
     * Gets the value of the relaName1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaName1() {
        return relaName1;
    }

    /**
     * Sets the value of the relaName1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaName1(String value) {
        this.relaName1 = value;
    }

    /**
     * Gets the value of the relaName2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaName2() {
        return relaName2;
    }

    /**
     * Sets the value of the relaName2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaName2(String value) {
        this.relaName2 = value;
    }

    /**
     * Gets the value of the relaName3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaName3() {
        return relaName3;
    }

    /**
     * Sets the value of the relaName3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaName3(String value) {
        this.relaName3 = value;
    }

    /**
     * Gets the value of the relaName4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaName4() {
        return relaName4;
    }

    /**
     * Sets the value of the relaName4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaName4(String value) {
        this.relaName4 = value;
    }

    /**
     * Gets the value of the relaAddr1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr1() {
        return relaAddr1;
    }

    /**
     * Sets the value of the relaAddr1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr1(String value) {
        this.relaAddr1 = value;
    }

    /**
     * Gets the value of the relaAddr2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr2() {
        return relaAddr2;
    }

    /**
     * Sets the value of the relaAddr2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr2(String value) {
        this.relaAddr2 = value;
    }

    /**
     * Gets the value of the relaAddr3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr3() {
        return relaAddr3;
    }

    /**
     * Sets the value of the relaAddr3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr3(String value) {
        this.relaAddr3 = value;
    }

    /**
     * Gets the value of the relaAddr4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr4() {
        return relaAddr4;
    }

    /**
     * Sets the value of the relaAddr4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr4(String value) {
        this.relaAddr4 = value;
    }

    /**
     * Gets the value of the relaAddr5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr5() {
        return relaAddr5;
    }

    /**
     * Sets the value of the relaAddr5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr5(String value) {
        this.relaAddr5 = value;
    }

    /**
     * Gets the value of the relaAddr6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr6() {
        return relaAddr6;
    }

    /**
     * Sets the value of the relaAddr6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr6(String value) {
        this.relaAddr6 = value;
    }

    /**
     * Gets the value of the relaAddr7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr7() {
        return relaAddr7;
    }

    /**
     * Sets the value of the relaAddr7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr7(String value) {
        this.relaAddr7 = value;
    }

    /**
     * Gets the value of the relaAddr8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaAddr8() {
        return relaAddr8;
    }

    /**
     * Sets the value of the relaAddr8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaAddr8(String value) {
        this.relaAddr8 = value;
    }

    /**
     * Gets the value of the relaTel1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaTel1() {
        return relaTel1;
    }

    /**
     * Sets the value of the relaTel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaTel1(String value) {
        this.relaTel1 = value;
    }

    /**
     * Gets the value of the relaTel2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaTel2() {
        return relaTel2;
    }

    /**
     * Sets the value of the relaTel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaTel2(String value) {
        this.relaTel2 = value;
    }

    /**
     * Gets the value of the relaTel3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaTel3() {
        return relaTel3;
    }

    /**
     * Sets the value of the relaTel3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaTel3(String value) {
        this.relaTel3 = value;
    }

    /**
     * Gets the value of the relaTel4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaTel4() {
        return relaTel4;
    }

    /**
     * Sets the value of the relaTel4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaTel4(String value) {
        this.relaTel4 = value;
    }

    /**
     * Gets the value of the relaEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaEmail() {
        return relaEmail;
    }

    /**
     * Sets the value of the relaEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaEmail(String value) {
        this.relaEmail = value;
    }

    /**
     * Gets the value of the relaFax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaFax() {
        return relaFax;
    }

    /**
     * Sets the value of the relaFax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaFax(String value) {
        this.relaFax = value;
    }

}
