
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCDRHistoryResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCDRHistoryResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="getCDRHistoryResult" type="{http://oss.huawei.com/webservice/ecare/services}CDRValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="getCDRHistoryCount" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCDRHistoryResult", propOrder = {
    "getCDRHistoryResult",
    "getCDRHistoryCount"
})
public class GetCDRHistoryResult {

    protected List<CDRValue> getCDRHistoryResult;
    protected int getCDRHistoryCount;

    /**
     * Gets the value of the getCDRHistoryResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCDRHistoryResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCDRHistoryResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CDRValue }
     * 
     * 
     */
    public List<CDRValue> getGetCDRHistoryResult() {
        if (getCDRHistoryResult == null) {
            getCDRHistoryResult = new ArrayList<CDRValue>();
        }
        return this.getCDRHistoryResult;
    }

    /**
     * Gets the value of the getCDRHistoryCount property.
     * 
     */
    public int getGetCDRHistoryCount() {
        return getCDRHistoryCount;
    }

    /**
     * Sets the value of the getCDRHistoryCount property.
     * 
     */
    public void setGetCDRHistoryCount(int value) {
        this.getCDRHistoryCount = value;
    }

}
