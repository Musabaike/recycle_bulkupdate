
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LockIPReply" type="{http://oss.huawei.com/webservice/ecare/services}LockIPReplyDetail"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lockIPReply"
})
@XmlRootElement(name = "lockIPResponse")
public class LockIPResponse {

    @XmlElement(name = "LockIPReply", required = true)
    protected LockIPReplyDetail lockIPReply;

    /**
     * Gets the value of the lockIPReply property.
     * 
     * @return
     *     possible object is
     *     {@link LockIPReplyDetail }
     *     
     */
    public LockIPReplyDetail getLockIPReply() {
        return lockIPReply;
    }

    /**
     * Sets the value of the lockIPReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link LockIPReplyDetail }
     *     
     */
    public void setLockIPReply(LockIPReplyDetail value) {
        this.lockIPReply = value;
    }

}
