
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChangeHybridToPostpaidReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *         &lt;element name="NewAccoutReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfCreateNewSubscriberValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "changeHybridToPostpaidReply",
    "newAccoutReply"
})
@XmlRootElement(name = "changeHybridToPostpaidResponse")
public class ChangeHybridToPostpaidResponse {

    @XmlElement(name = "ChangeHybridToPostpaidReply", required = true)
    protected ResultOfOperationValue changeHybridToPostpaidReply;
    @XmlElement(name = "NewAccoutReply", required = true)
    protected ResultOfCreateNewSubscriberValue newAccoutReply;

    /**
     * Gets the value of the changeHybridToPostpaidReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getChangeHybridToPostpaidReply() {
        return changeHybridToPostpaidReply;
    }

    /**
     * Sets the value of the changeHybridToPostpaidReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setChangeHybridToPostpaidReply(ResultOfOperationValue value) {
        this.changeHybridToPostpaidReply = value;
    }

    /**
     * Gets the value of the newAccoutReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfCreateNewSubscriberValue }
     *     
     */
    public ResultOfCreateNewSubscriberValue getNewAccoutReply() {
        return newAccoutReply;
    }

    /**
     * Sets the value of the newAccoutReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfCreateNewSubscriberValue }
     *     
     */
    public void setNewAccoutReply(ResultOfCreateNewSubscriberValue value) {
        this.newAccoutReply = value;
    }

}
