
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.oss.webservice.ecare.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.oss.webservice.ecare.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSUsageValues }
     * 
     */
    public WSUsageValues createWSUsageValues() {
        return new WSUsageValues();
    }

    /**
     * Create an instance of {@link WSBusiInfoValue }
     * 
     */
    public WSBusiInfoValue createWSBusiInfoValue() {
        return new WSBusiInfoValue();
    }

    /**
     * Create an instance of {@link WSUnbilledInfoValue }
     * 
     */
    public WSUnbilledInfoValue createWSUnbilledInfoValue() {
        return new WSUnbilledInfoValue();
    }

    /**
     * Create an instance of {@link WSPendingBusiInfoValue }
     * 
     */
    public WSPendingBusiInfoValue createWSPendingBusiInfoValue() {
        return new WSPendingBusiInfoValue();
    }

    /**
     * Create an instance of {@link FreeUnitsInfoValues }
     * 
     */
    public FreeUnitsInfoValues createFreeUnitsInfoValues() {
        return new FreeUnitsInfoValues();
    }

    /**
     * Create an instance of {@link QueryBalanceResultValue }
     * 
     */
    public QueryBalanceResultValue createQueryBalanceResultValue() {
        return new QueryBalanceResultValue();
    }

    /**
     * Create an instance of {@link VoucherCardResultValue }
     * 
     */
    public VoucherCardResultValue createVoucherCardResultValue() {
        return new VoucherCardResultValue();
    }

    /**
     * Create an instance of {@link WSConstractSignedInfoValue }
     * 
     */
    public WSConstractSignedInfoValue createWSConstractSignedInfoValue() {
        return new WSConstractSignedInfoValue();
    }

    /**
     * Create an instance of {@link GetSimMsisdnResource }
     * 
     */
    public GetSimMsisdnResource createGetSimMsisdnResource() {
        return new GetSimMsisdnResource();
    }

    /**
     * Create an instance of {@link AccessSessionValue }
     * 
     */
    public AccessSessionValue createAccessSessionValue() {
        return new AccessSessionValue();
    }

    /**
     * Create an instance of {@link QuerySimMsisdnResRequestValue }
     * 
     */
    public QuerySimMsisdnResRequestValue createQuerySimMsisdnResRequestValue() {
        return new QuerySimMsisdnResRequestValue();
    }

    /**
     * Create an instance of {@link ManageSimMsisdnResource }
     * 
     */
    public ManageSimMsisdnResource createManageSimMsisdnResource() {
        return new ManageSimMsisdnResource();
    }

    /**
     * Create an instance of {@link ManageResourceRequestValue }
     * 
     */
    public ManageResourceRequestValue createManageResourceRequestValue() {
        return new ManageResourceRequestValue();
    }

    /**
     * Create an instance of {@link SuspendGroup }
     * 
     */
    public SuspendGroup createSuspendGroup() {
        return new SuspendGroup();
    }

    /**
     * Create an instance of {@link SuspendGroupValue }
     * 
     */
    public SuspendGroupValue createSuspendGroupValue() {
        return new SuspendGroupValue();
    }

    /**
     * Create an instance of {@link ResumeGroup }
     * 
     */
    public ResumeGroup createResumeGroup() {
        return new ResumeGroup();
    }

    /**
     * Create an instance of {@link ResumeGroupValue }
     * 
     */
    public ResumeGroupValue createResumeGroupValue() {
        return new ResumeGroupValue();
    }

    /**
     * Create an instance of {@link RemoveHGShortCode }
     * 
     */
    public RemoveHGShortCode createRemoveHGShortCode() {
        return new RemoveHGShortCode();
    }

    /**
     * Create an instance of {@link RemoveHGShortCodeRequestValue }
     * 
     */
    public RemoveHGShortCodeRequestValue createRemoveHGShortCodeRequestValue() {
        return new RemoveHGShortCodeRequestValue();
    }

    /**
     * Create an instance of {@link GetResources }
     * 
     */
    public GetResources createGetResources() {
        return new GetResources();
    }

    /**
     * Create an instance of {@link GetResourceRequestValue }
     * 
     */
    public GetResourceRequestValue createGetResourceRequestValue() {
        return new GetResourceRequestValue();
    }

    /**
     * Create an instance of {@link GetSignedContracts }
     * 
     */
    public GetSignedContracts createGetSignedContracts() {
        return new GetSignedContracts();
    }

    /**
     * Create an instance of {@link WSQueryContractSignConditionValue }
     * 
     */
    public WSQueryContractSignConditionValue createWSQueryContractSignConditionValue() {
        return new WSQueryContractSignConditionValue();
    }

    /**
     * Create an instance of {@link ChangeHybridToPrepaid }
     * 
     */
    public ChangeHybridToPrepaid createChangeHybridToPrepaid() {
        return new ChangeHybridToPrepaid();
    }

    /**
     * Create an instance of {@link HybridToPrepaidValue }
     * 
     */
    public HybridToPrepaidValue createHybridToPrepaidValue() {
        return new HybridToPrepaidValue();
    }

    /**
     * Create an instance of {@link MNPPortIn }
     * 
     */
    public MNPPortIn createMNPPortIn() {
        return new MNPPortIn();
    }

    /**
     * Create an instance of {@link MNPPortInValue }
     * 
     */
    public MNPPortInValue createMNPPortInValue() {
        return new MNPPortInValue();
    }

    /**
     * Create an instance of {@link ModifyAccountInfo }
     * 
     */
    public ModifyAccountInfo createModifyAccountInfo() {
        return new ModifyAccountInfo();
    }

    /**
     * Create an instance of {@link ModifyAccountInfoValue }
     * 
     */
    public ModifyAccountInfoValue createModifyAccountInfoValue() {
        return new ModifyAccountInfoValue();
    }

    /**
     * Create an instance of {@link QuerySubscribeAccessories }
     * 
     */
    public QuerySubscribeAccessories createQuerySubscribeAccessories() {
        return new QuerySubscribeAccessories();
    }

    /**
     * Create an instance of {@link QuerySubscribeAccessoriesRequestValue }
     * 
     */
    public QuerySubscribeAccessoriesRequestValue createQuerySubscribeAccessoriesRequestValue() {
        return new QuerySubscribeAccessoriesRequestValue();
    }

    /**
     * Create an instance of {@link DeactivateSubscriber }
     * 
     */
    public DeactivateSubscriber createDeactivateSubscriber() {
        return new DeactivateSubscriber();
    }

    /**
     * Create an instance of {@link DeactivateSubscriberValue }
     * 
     */
    public DeactivateSubscriberValue createDeactivateSubscriberValue() {
        return new DeactivateSubscriberValue();
    }

    /**
     * Create an instance of {@link GetCUGMembers }
     * 
     */
    public GetCUGMembers createGetCUGMembers() {
        return new GetCUGMembers();
    }

    /**
     * Create an instance of {@link QueryCUGMembersRequestValue }
     * 
     */
    public QueryCUGMembersRequestValue createQueryCUGMembersRequestValue() {
        return new QueryCUGMembersRequestValue();
    }

    /**
     * Create an instance of {@link GetZGCs }
     * 
     */
    public GetZGCs createGetZGCs() {
        return new GetZGCs();
    }

    /**
     * Create an instance of {@link QueryZGCConditionValue }
     * 
     */
    public QueryZGCConditionValue createQueryZGCConditionValue() {
        return new QueryZGCConditionValue();
    }

    /**
     * Create an instance of {@link GetXMLBill }
     * 
     */
    public GetXMLBill createGetXMLBill() {
        return new GetXMLBill();
    }

    /**
     * Create an instance of {@link GetXMLBillRequestDetails }
     * 
     */
    public GetXMLBillRequestDetails createGetXMLBillRequestDetails() {
        return new GetXMLBillRequestDetails();
    }

    /**
     * Create an instance of {@link AddCUGMember }
     * 
     */
    public AddCUGMember createAddCUGMember() {
        return new AddCUGMember();
    }

    /**
     * Create an instance of {@link AddCUGMemberRequestValue }
     * 
     */
    public AddCUGMemberRequestValue createAddCUGMemberRequestValue() {
        return new AddCUGMemberRequestValue();
    }

    /**
     * Create an instance of {@link MNPRegisterSTP }
     * 
     */
    public MNPRegisterSTP createMNPRegisterSTP() {
        return new MNPRegisterSTP();
    }

    /**
     * Create an instance of {@link MNPRegisterSTPValue }
     * 
     */
    public MNPRegisterSTPValue createMNPRegisterSTPValue() {
        return new MNPRegisterSTPValue();
    }

    /**
     * Create an instance of {@link UnBarringSubscriber }
     * 
     */
    public UnBarringSubscriber createUnBarringSubscriber() {
        return new UnBarringSubscriber();
    }

    /**
     * Create an instance of {@link UnBarringSubscriberValue }
     * 
     */
    public UnBarringSubscriberValue createUnBarringSubscriberValue() {
        return new UnBarringSubscriberValue();
    }

    /**
     * Create an instance of {@link GetVoucherCards }
     * 
     */
    public GetVoucherCards createGetVoucherCards() {
        return new GetVoucherCards();
    }

    /**
     * Create an instance of {@link QueryVoucherCardRequestValue }
     * 
     */
    public QueryVoucherCardRequestValue createQueryVoucherCardRequestValue() {
        return new QueryVoucherCardRequestValue();
    }

    /**
     * Create an instance of {@link ModifyCustomerInfo }
     * 
     */
    public ModifyCustomerInfo createModifyCustomerInfo() {
        return new ModifyCustomerInfo();
    }

    /**
     * Create an instance of {@link ModifyCustomerInfoValue }
     * 
     */
    public ModifyCustomerInfoValue createModifyCustomerInfoValue() {
        return new ModifyCustomerInfoValue();
    }

    /**
     * Create an instance of {@link QueryBalance }
     * 
     */
    public QueryBalance createQueryBalance() {
        return new QueryBalance();
    }

    /**
     * Create an instance of {@link QueryBalanceRequestValue }
     * 
     */
    public QueryBalanceRequestValue createQueryBalanceRequestValue() {
        return new QueryBalanceRequestValue();
    }

    /**
     * Create an instance of {@link RechargeByVoucherCard }
     * 
     */
    public RechargeByVoucherCard createRechargeByVoucherCard() {
        return new RechargeByVoucherCard();
    }

    /**
     * Create an instance of {@link RechargeRequestValue }
     * 
     */
    public RechargeRequestValue createRechargeRequestValue() {
        return new RechargeRequestValue();
    }

    /**
     * Create an instance of {@link ChangeSubscriberProducts }
     * 
     */
    public ChangeSubscriberProducts createChangeSubscriberProducts() {
        return new ChangeSubscriberProducts();
    }

    /**
     * Create an instance of {@link ChangeProductValue }
     * 
     */
    public ChangeProductValue createChangeProductValue() {
        return new ChangeProductValue();
    }

    /**
     * Create an instance of {@link HandlePending }
     * 
     */
    public HandlePending createHandlePending() {
        return new HandlePending();
    }

    /**
     * Create an instance of {@link WSHandlePendingConditionValue }
     * 
     */
    public WSHandlePendingConditionValue createWSHandlePendingConditionValue() {
        return new WSHandlePendingConditionValue();
    }

    /**
     * Create an instance of {@link RemoveCUGMember }
     * 
     */
    public RemoveCUGMember createRemoveCUGMember() {
        return new RemoveCUGMember();
    }

    /**
     * Create an instance of {@link RemoveCUGMemberRequestValue }
     * 
     */
    public RemoveCUGMemberRequestValue createRemoveCUGMemberRequestValue() {
        return new RemoveCUGMemberRequestValue();
    }

    /**
     * Create an instance of {@link CreateNewSubscriber }
     * 
     */
    public CreateNewSubscriber createCreateNewSubscriber() {
        return new CreateNewSubscriber();
    }

    /**
     * Create an instance of {@link CreateNewSubscriberValue }
     * 
     */
    public CreateNewSubscriberValue createCreateNewSubscriberValue() {
        return new CreateNewSubscriberValue();
    }

    /**
     * Create an instance of {@link GetUsageAmount }
     * 
     */
    public GetUsageAmount createGetUsageAmount() {
        return new GetUsageAmount();
    }

    /**
     * Create an instance of {@link GetUsageAmountRequestDetails }
     * 
     */
    public GetUsageAmountRequestDetails createGetUsageAmountRequestDetails() {
        return new GetUsageAmountRequestDetails();
    }

    /**
     * Create an instance of {@link GetReceiptData }
     * 
     */
    public GetReceiptData createGetReceiptData() {
        return new GetReceiptData();
    }

    /**
     * Create an instance of {@link GetReceiptDataRequestDetails }
     * 
     */
    public GetReceiptDataRequestDetails createGetReceiptDataRequestDetails() {
        return new GetReceiptDataRequestDetails();
    }

    /**
     * Create an instance of {@link IsOrderZone }
     * 
     */
    public IsOrderZone createIsOrderZone() {
        return new IsOrderZone();
    }

    /**
     * Create an instance of {@link IsOrderZoneRequestValue }
     * 
     */
    public IsOrderZoneRequestValue createIsOrderZoneRequestValue() {
        return new IsOrderZoneRequestValue();
    }

    /**
     * Create an instance of {@link LockMSISDN }
     * 
     */
    public LockMSISDN createLockMSISDN() {
        return new LockMSISDN();
    }

    /**
     * Create an instance of {@link LockMSISDNRequestDetail }
     * 
     */
    public LockMSISDNRequestDetail createLockMSISDNRequestDetail() {
        return new LockMSISDNRequestDetail();
    }

    /**
     * Create an instance of {@link GetCDRHistory }
     * 
     */
    public GetCDRHistory createGetCDRHistory() {
        return new GetCDRHistory();
    }

    /**
     * Create an instance of {@link QueryCDRConditionValue }
     * 
     */
    public QueryCDRConditionValue createQueryCDRConditionValue() {
        return new QueryCDRConditionValue();
    }

    /**
     * Create an instance of {@link AdjustCredit }
     * 
     */
    public AdjustCredit createAdjustCredit() {
        return new AdjustCredit();
    }

    /**
     * Create an instance of {@link CreditLimitAdjustValue }
     * 
     */
    public CreditLimitAdjustValue createCreditLimitAdjustValue() {
        return new CreditLimitAdjustValue();
    }

    /**
     * Create an instance of {@link GetPhoneNumbers }
     * 
     */
    public GetPhoneNumbers createGetPhoneNumbers() {
        return new GetPhoneNumbers();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersRequestDetails }
     * 
     */
    public GetPhoneNumbersRequestDetails createGetPhoneNumbersRequestDetails() {
        return new GetPhoneNumbersRequestDetails();
    }

    /**
     * Create an instance of {@link ReportLost }
     * 
     */
    public ReportLost createReportLost() {
        return new ReportLost();
    }

    /**
     * Create an instance of {@link ReportLostValue }
     * 
     */
    public ReportLostValue createReportLostValue() {
        return new ReportLostValue();
    }

    /**
     * Create an instance of {@link OrderZone }
     * 
     */
    public OrderZone createOrderZone() {
        return new OrderZone();
    }

    /**
     * Create an instance of {@link OrderZoneRequestValue }
     * 
     */
    public OrderZoneRequestValue createOrderZoneRequestValue() {
        return new OrderZoneRequestValue();
    }

    /**
     * Create an instance of {@link ChangePostpaidToHybrid }
     * 
     */
    public ChangePostpaidToHybrid createChangePostpaidToHybrid() {
        return new ChangePostpaidToHybrid();
    }

    /**
     * Create an instance of {@link PostpaidToHybridValue }
     * 
     */
    public PostpaidToHybridValue createPostpaidToHybridValue() {
        return new PostpaidToHybridValue();
    }

    /**
     * Create an instance of {@link PaymentFromCustomer }
     * 
     */
    public PaymentFromCustomer createPaymentFromCustomer() {
        return new PaymentFromCustomer();
    }

    /**
     * Create an instance of {@link PaymentFromCustomerRequestDetails }
     * 
     */
    public PaymentFromCustomerRequestDetails createPaymentFromCustomerRequestDetails() {
        return new PaymentFromCustomerRequestDetails();
    }

    /**
     * Create an instance of {@link ChangePostpaidToPrepaid }
     * 
     */
    public ChangePostpaidToPrepaid createChangePostpaidToPrepaid() {
        return new ChangePostpaidToPrepaid();
    }

    /**
     * Create an instance of {@link PostpaidToPrepaidValue }
     * 
     */
    public PostpaidToPrepaidValue createPostpaidToPrepaidValue() {
        return new PostpaidToPrepaidValue();
    }

    /**
     * Create an instance of {@link ChangePassword }
     * 
     */
    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    /**
     * Create an instance of {@link ChangePasswordRequestValue }
     * 
     */
    public ChangePasswordRequestValue createChangePasswordRequestValue() {
        return new ChangePasswordRequestValue();
    }

    /**
     * Create an instance of {@link UpdateAdditionalNumber }
     * 
     */
    public UpdateAdditionalNumber createUpdateAdditionalNumber() {
        return new UpdateAdditionalNumber();
    }

    /**
     * Create an instance of {@link UpdateAdditionalNumberRequestValue }
     * 
     */
    public UpdateAdditionalNumberRequestValue createUpdateAdditionalNumberRequestValue() {
        return new UpdateAdditionalNumberRequestValue();
    }

    /**
     * Create an instance of {@link BarringSubscriber }
     * 
     */
    public BarringSubscriber createBarringSubscriber() {
        return new BarringSubscriber();
    }

    /**
     * Create an instance of {@link BarringSubscriberValue }
     * 
     */
    public BarringSubscriberValue createBarringSubscriberValue() {
        return new BarringSubscriberValue();
    }

    /**
     * Create an instance of {@link GetCustomers }
     * 
     */
    public GetCustomers createGetCustomers() {
        return new GetCustomers();
    }

    /**
     * Create an instance of {@link QueryCustomerConditionValue }
     * 
     */
    public QueryCustomerConditionValue createQueryCustomerConditionValue() {
        return new QueryCustomerConditionValue();
    }

    /**
     * Create an instance of {@link ChangeHybridToPostpaid }
     * 
     */
    public ChangeHybridToPostpaid createChangeHybridToPostpaid() {
        return new ChangeHybridToPostpaid();
    }

    /**
     * Create an instance of {@link HybridToPostpaidValue }
     * 
     */
    public HybridToPostpaidValue createHybridToPostpaidValue() {
        return new HybridToPostpaidValue();
    }

    /**
     * Create an instance of {@link GetFreeUnits }
     * 
     */
    public GetFreeUnits createGetFreeUnits() {
        return new GetFreeUnits();
    }

    /**
     * Create an instance of {@link QueryFreeUnitsConditionValue }
     * 
     */
    public QueryFreeUnitsConditionValue createQueryFreeUnitsConditionValue() {
        return new QueryFreeUnitsConditionValue();
    }

    /**
     * Create an instance of {@link GetValidRatePlans }
     * 
     */
    public GetValidRatePlans createGetValidRatePlans() {
        return new GetValidRatePlans();
    }

    /**
     * Create an instance of {@link EAIWSQueryValidProductConditionValue }
     * 
     */
    public EAIWSQueryValidProductConditionValue createEAIWSQueryValidProductConditionValue() {
        return new EAIWSQueryValidProductConditionValue();
    }

    /**
     * Create an instance of {@link ResumeSubscriber }
     * 
     */
    public ResumeSubscriber createResumeSubscriber() {
        return new ResumeSubscriber();
    }

    /**
     * Create an instance of {@link ResumeSubscriberValue }
     * 
     */
    public ResumeSubscriberValue createResumeSubscriberValue() {
        return new ResumeSubscriberValue();
    }

    /**
     * Create an instance of {@link GetCUGs }
     * 
     */
    public GetCUGs createGetCUGs() {
        return new GetCUGs();
    }

    /**
     * Create an instance of {@link QueryCUGRequestValue }
     * 
     */
    public QueryCUGRequestValue createQueryCUGRequestValue() {
        return new QueryCUGRequestValue();
    }

    /**
     * Create an instance of {@link GetSimCards }
     * 
     */
    public GetSimCards createGetSimCards() {
        return new GetSimCards();
    }

    /**
     * Create an instance of {@link GetSimCardsRequestDetails }
     * 
     */
    public GetSimCardsRequestDetails createGetSimCardsRequestDetails() {
        return new GetSimCardsRequestDetails();
    }

    /**
     * Create an instance of {@link QueryCreditLimit }
     * 
     */
    public QueryCreditLimit createQueryCreditLimit() {
        return new QueryCreditLimit();
    }

    /**
     * Create an instance of {@link QueryCreditLimitConditionValue }
     * 
     */
    public QueryCreditLimitConditionValue createQueryCreditLimitConditionValue() {
        return new QueryCreditLimitConditionValue();
    }

    /**
     * Create an instance of {@link AddHGShortCode }
     * 
     */
    public AddHGShortCode createAddHGShortCode() {
        return new AddHGShortCode();
    }

    /**
     * Create an instance of {@link AddHGShortCodeRequestValue }
     * 
     */
    public AddHGShortCodeRequestValue createAddHGShortCodeRequestValue() {
        return new AddHGShortCodeRequestValue();
    }

    /**
     * Create an instance of {@link GetDictTable }
     * 
     */
    public GetDictTable createGetDictTable() {
        return new GetDictTable();
    }

    /**
     * Create an instance of {@link QueryDictTableValue }
     * 
     */
    public QueryDictTableValue createQueryDictTableValue() {
        return new QueryDictTableValue();
    }

    /**
     * Create an instance of {@link PrepaidActivation }
     * 
     */
    public PrepaidActivation createPrepaidActivation() {
        return new PrepaidActivation();
    }

    /**
     * Create an instance of {@link PrepaidActivationValue }
     * 
     */
    public PrepaidActivationValue createPrepaidActivationValue() {
        return new PrepaidActivationValue();
    }

    /**
     * Create an instance of {@link GetPendingBusiInfos }
     * 
     */
    public GetPendingBusiInfos createGetPendingBusiInfos() {
        return new GetPendingBusiInfos();
    }

    /**
     * Create an instance of {@link WSQueryPendingBusiConditionValue }
     * 
     */
    public WSQueryPendingBusiConditionValue createWSQueryPendingBusiConditionValue() {
        return new WSQueryPendingBusiConditionValue();
    }

    /**
     * Create an instance of {@link GetFreeUnitOfCumulation }
     * 
     */
    public GetFreeUnitOfCumulation createGetFreeUnitOfCumulation() {
        return new GetFreeUnitOfCumulation();
    }

    /**
     * Create an instance of {@link SetAccountRelation }
     * 
     */
    public SetAccountRelation createSetAccountRelation() {
        return new SetAccountRelation();
    }

    /**
     * Create an instance of {@link SetAccountRelationRequestValue }
     * 
     */
    public SetAccountRelationRequestValue createSetAccountRelationRequestValue() {
        return new SetAccountRelationRequestValue();
    }

    /**
     * Create an instance of {@link GetUnbilledInfo }
     * 
     */
    public GetUnbilledInfo createGetUnbilledInfo() {
        return new GetUnbilledInfo();
    }

    /**
     * Create an instance of {@link WSQueryUnbilledInfoConditionValue }
     * 
     */
    public WSQueryUnbilledInfoConditionValue createWSQueryUnbilledInfoConditionValue() {
        return new WSQueryUnbilledInfoConditionValue();
    }

    /**
     * Create an instance of {@link GetSubscriberStatusHistory }
     * 
     */
    public GetSubscriberStatusHistory createGetSubscriberStatusHistory() {
        return new GetSubscriberStatusHistory();
    }

    /**
     * Create an instance of {@link QueryBySubIdValue }
     * 
     */
    public QueryBySubIdValue createQueryBySubIdValue() {
        return new QueryBySubIdValue();
    }

    /**
     * Create an instance of {@link CreateCUG }
     * 
     */
    public CreateCUG createCreateCUG() {
        return new CreateCUG();
    }

    /**
     * Create an instance of {@link CreateCUGRequestValue }
     * 
     */
    public CreateCUGRequestValue createCreateCUGRequestValue() {
        return new CreateCUGRequestValue();
    }

    /**
     * Create an instance of {@link UpdateZone }
     * 
     */
    public UpdateZone createUpdateZone() {
        return new UpdateZone();
    }

    /**
     * Create an instance of {@link UpdateZoneRequestValue }
     * 
     */
    public UpdateZoneRequestValue createUpdateZoneRequestValue() {
        return new UpdateZoneRequestValue();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersQuantity }
     * 
     */
    public GetPhoneNumbersQuantity createGetPhoneNumbersQuantity() {
        return new GetPhoneNumbersQuantity();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersQuantityRequestDetails }
     * 
     */
    public GetPhoneNumbersQuantityRequestDetails createGetPhoneNumbersQuantityRequestDetails() {
        return new GetPhoneNumbersQuantityRequestDetails();
    }

    /**
     * Create an instance of {@link AutoRefillRegister }
     * 
     */
    public AutoRefillRegister createAutoRefillRegister() {
        return new AutoRefillRegister();
    }

    /**
     * Create an instance of {@link ModifyPaymentModeValue }
     * 
     */
    public ModifyPaymentModeValue createModifyPaymentModeValue() {
        return new ModifyPaymentModeValue();
    }

    /**
     * Create an instance of {@link PurchaseGoods }
     * 
     */
    public PurchaseGoods createPurchaseGoods() {
        return new PurchaseGoods();
    }

    /**
     * Create an instance of {@link PurchaseGoodsValue }
     * 
     */
    public PurchaseGoodsValue createPurchaseGoodsValue() {
        return new PurchaseGoodsValue();
    }

    /**
     * Create an instance of {@link ChangeSimCard }
     * 
     */
    public ChangeSimCard createChangeSimCard() {
        return new ChangeSimCard();
    }

    /**
     * Create an instance of {@link ChangeSimCardValue }
     * 
     */
    public ChangeSimCardValue createChangeSimCardValue() {
        return new ChangeSimCardValue();
    }

    /**
     * Create an instance of {@link CancelZone }
     * 
     */
    public CancelZone createCancelZone() {
        return new CancelZone();
    }

    /**
     * Create an instance of {@link CancelZoneRequestValue }
     * 
     */
    public CancelZoneRequestValue createCancelZoneRequestValue() {
        return new CancelZoneRequestValue();
    }

    /**
     * Create an instance of {@link GetUserSubscribeState }
     * 
     */
    public GetUserSubscribeState createGetUserSubscribeState() {
        return new GetUserSubscribeState();
    }

    /**
     * Create an instance of {@link QueryUserSubscribeStateRequestValue }
     * 
     */
    public QueryUserSubscribeStateRequestValue createQueryUserSubscribeStateRequestValue() {
        return new QueryUserSubscribeStateRequestValue();
    }

    /**
     * Create an instance of {@link SendSMS }
     * 
     */
    public SendSMS createSendSMS() {
        return new SendSMS();
    }

    /**
     * Create an instance of {@link SendSMSRequestValue }
     * 
     */
    public SendSMSRequestValue createSendSMSRequestValue() {
        return new SendSMSRequestValue();
    }

    /**
     * Create an instance of {@link GetBusiInfos }
     * 
     */
    public GetBusiInfos createGetBusiInfos() {
        return new GetBusiInfos();
    }

    /**
     * Create an instance of {@link WSQueryBusiConditionValue }
     * 
     */
    public WSQueryBusiConditionValue createWSQueryBusiConditionValue() {
        return new WSQueryBusiConditionValue();
    }

    /**
     * Create an instance of {@link LockSIM }
     * 
     */
    public LockSIM createLockSIM() {
        return new LockSIM();
    }

    /**
     * Create an instance of {@link LockSIMRequestDetail }
     * 
     */
    public LockSIMRequestDetail createLockSIMRequestDetail() {
        return new LockSIMRequestDetail();
    }

    /**
     * Create an instance of {@link ChangePrepaidToPostpaid }
     * 
     */
    public ChangePrepaidToPostpaid createChangePrepaidToPostpaid() {
        return new ChangePrepaidToPostpaid();
    }

    /**
     * Create an instance of {@link PrepaidToPostpaidValue }
     * 
     */
    public PrepaidToPostpaidValue createPrepaidToPostpaidValue() {
        return new PrepaidToPostpaidValue();
    }

    /**
     * Create an instance of {@link UnLockMSISDN }
     * 
     */
    public UnLockMSISDN createUnLockMSISDN() {
        return new UnLockMSISDN();
    }

    /**
     * Create an instance of {@link UnLockMSISDNRequestDetail }
     * 
     */
    public UnLockMSISDNRequestDetail createUnLockMSISDNRequestDetail() {
        return new UnLockMSISDNRequestDetail();
    }

    /**
     * Create an instance of {@link GetFeeforService }
     * 
     */
    public GetFeeforService createGetFeeforService() {
        return new GetFeeforService();
    }

    /**
     * Create an instance of {@link EAIWSQueryServiceFeeConditionValue }
     * 
     */
    public EAIWSQueryServiceFeeConditionValue createEAIWSQueryServiceFeeConditionValue() {
        return new EAIWSQueryServiceFeeConditionValue();
    }

    /**
     * Create an instance of {@link QueryDCCHistory }
     * 
     */
    public QueryDCCHistory createQueryDCCHistory() {
        return new QueryDCCHistory();
    }

    /**
     * Create an instance of {@link QueryDCCHistoryRequestValue }
     * 
     */
    public QueryDCCHistoryRequestValue createQueryDCCHistoryRequestValue() {
        return new QueryDCCHistoryRequestValue();
    }

    /**
     * Create an instance of {@link GetGoodsSaleHistory }
     * 
     */
    public GetGoodsSaleHistory createGetGoodsSaleHistory() {
        return new GetGoodsSaleHistory();
    }

    /**
     * Create an instance of {@link SubsciberIDValue }
     * 
     */
    public SubsciberIDValue createSubsciberIDValue() {
        return new SubsciberIDValue();
    }

    /**
     * Create an instance of {@link MNPOutComeBack }
     * 
     */
    public MNPOutComeBack createMNPOutComeBack() {
        return new MNPOutComeBack();
    }

    /**
     * Create an instance of {@link MNPOutComeBackValue }
     * 
     */
    public MNPOutComeBackValue createMNPOutComeBackValue() {
        return new MNPOutComeBackValue();
    }

    /**
     * Create an instance of {@link ModifyActualUser }
     * 
     */
    public ModifyActualUser createModifyActualUser() {
        return new ModifyActualUser();
    }

    /**
     * Create an instance of {@link ModifyActualUserValue }
     * 
     */
    public ModifyActualUserValue createModifyActualUserValue() {
        return new ModifyActualUserValue();
    }

    /**
     * Create an instance of {@link ChangePrepaidToHybrid }
     * 
     */
    public ChangePrepaidToHybrid createChangePrepaidToHybrid() {
        return new ChangePrepaidToHybrid();
    }

    /**
     * Create an instance of {@link PrepaidToHybridValue }
     * 
     */
    public PrepaidToHybridValue createPrepaidToHybridValue() {
        return new PrepaidToHybridValue();
    }

    /**
     * Create an instance of {@link ChangePaymentModeInfo }
     * 
     */
    public ChangePaymentModeInfo createChangePaymentModeInfo() {
        return new ChangePaymentModeInfo();
    }

    /**
     * Create an instance of {@link ChangeMsisdn }
     * 
     */
    public ChangeMsisdn createChangeMsisdn() {
        return new ChangeMsisdn();
    }

    /**
     * Create an instance of {@link ChangeMsisdnValue }
     * 
     */
    public ChangeMsisdnValue createChangeMsisdnValue() {
        return new ChangeMsisdnValue();
    }

    /**
     * Create an instance of {@link SuspendSubscriber }
     * 
     */
    public SuspendSubscriber createSuspendSubscriber() {
        return new SuspendSubscriber();
    }

    /**
     * Create an instance of {@link SuspendSubscriberValue }
     * 
     */
    public SuspendSubscriberValue createSuspendSubscriberValue() {
        return new SuspendSubscriberValue();
    }

    /**
     * Create an instance of {@link GetOutstanding }
     * 
     */
    public GetOutstanding createGetOutstanding() {
        return new GetOutstanding();
    }

    /**
     * Create an instance of {@link GetOutstandingRequestDetails }
     * 
     */
    public GetOutstandingRequestDetails createGetOutstandingRequestDetails() {
        return new GetOutstandingRequestDetails();
    }

    /**
     * Create an instance of {@link AdjustFreeUnits }
     * 
     */
    public AdjustFreeUnits createAdjustFreeUnits() {
        return new AdjustFreeUnits();
    }

    /**
     * Create an instance of {@link FreeAdjustValue }
     * 
     */
    public FreeAdjustValue createFreeAdjustValue() {
        return new FreeAdjustValue();
    }

    /**
     * Create an instance of {@link VoidPaymentTransaction }
     * 
     */
    public VoidPaymentTransaction createVoidPaymentTransaction() {
        return new VoidPaymentTransaction();
    }

    /**
     * Create an instance of {@link VoidPaymentRequestValue }
     * 
     */
    public VoidPaymentRequestValue createVoidPaymentRequestValue() {
        return new VoidPaymentRequestValue();
    }

    /**
     * Create an instance of {@link GetUsingProducts }
     * 
     */
    public GetUsingProducts createGetUsingProducts() {
        return new GetUsingProducts();
    }

    /**
     * Create an instance of {@link GetDunningHistory }
     * 
     */
    public GetDunningHistory createGetDunningHistory() {
        return new GetDunningHistory();
    }

    /**
     * Create an instance of {@link GetDunningHistoryIn }
     * 
     */
    public GetDunningHistoryIn createGetDunningHistoryIn() {
        return new GetDunningHistoryIn();
    }

    /**
     * Create an instance of {@link GetValidGoodsProgram }
     * 
     */
    public GetValidGoodsProgram createGetValidGoodsProgram() {
        return new GetValidGoodsProgram();
    }

    /**
     * Create an instance of {@link EAIWSQueryGoodsListConditionValue }
     * 
     */
    public EAIWSQueryGoodsListConditionValue createEAIWSQueryGoodsListConditionValue() {
        return new EAIWSQueryGoodsListConditionValue();
    }

    /**
     * Create an instance of {@link TransferOwnership }
     * 
     */
    public TransferOwnership createTransferOwnership() {
        return new TransferOwnership();
    }

    /**
     * Create an instance of {@link TransferOwnershipValue }
     * 
     */
    public TransferOwnershipValue createTransferOwnershipValue() {
        return new TransferOwnershipValue();
    }

    /**
     * Create an instance of {@link ModifySubscriberInfo }
     * 
     */
    public ModifySubscriberInfo createModifySubscriberInfo() {
        return new ModifySubscriberInfo();
    }

    /**
     * Create an instance of {@link ModifySubscriberInfoValue }
     * 
     */
    public ModifySubscriberInfoValue createModifySubscriberInfoValue() {
        return new ModifySubscriberInfoValue();
    }

    /**
     * Create an instance of {@link GetSubscribers }
     * 
     */
    public GetSubscribers createGetSubscribers() {
        return new GetSubscribers();
    }

    /**
     * Create an instance of {@link QuerySubscirberConditionValue }
     * 
     */
    public QuerySubscirberConditionValue createQuerySubscirberConditionValue() {
        return new QuerySubscirberConditionValue();
    }

    /**
     * Create an instance of {@link ChangeResourceStatus }
     * 
     */
    public ChangeResourceStatus createChangeResourceStatus() {
        return new ChangeResourceStatus();
    }

    /**
     * Create an instance of {@link ChangeResourceStatusRequestValue }
     * 
     */
    public ChangeResourceStatusRequestValue createChangeResourceStatusRequestValue() {
        return new ChangeResourceStatusRequestValue();
    }

    /**
     * Create an instance of {@link UnLockSIM }
     * 
     */
    public UnLockSIM createUnLockSIM() {
        return new UnLockSIM();
    }

    /**
     * Create an instance of {@link UnLockSIMRequestDetail }
     * 
     */
    public UnLockSIMRequestDetail createUnLockSIMRequestDetail() {
        return new UnLockSIMRequestDetail();
    }

    /**
     * Create an instance of {@link GetValidServicePackages }
     * 
     */
    public GetValidServicePackages createGetValidServicePackages() {
        return new GetValidServicePackages();
    }

    /**
     * Create an instance of {@link MNPDeactivateSub }
     * 
     */
    public MNPDeactivateSub createMNPDeactivateSub() {
        return new MNPDeactivateSub();
    }

    /**
     * Create an instance of {@link MNPPortOutValue }
     * 
     */
    public MNPPortOutValue createMNPPortOutValue() {
        return new MNPPortOutValue();
    }

    /**
     * Create an instance of {@link QuerySMSNoticeHistory }
     * 
     */
    public QuerySMSNoticeHistory createQuerySMSNoticeHistory() {
        return new QuerySMSNoticeHistory();
    }

    /**
     * Create an instance of {@link QuerySMSNoticeHistoryRequestValue }
     * 
     */
    public QuerySMSNoticeHistoryRequestValue createQuerySMSNoticeHistoryRequestValue() {
        return new QuerySMSNoticeHistoryRequestValue();
    }

    /**
     * Create an instance of {@link GetAccounts }
     * 
     */
    public GetAccounts createGetAccounts() {
        return new GetAccounts();
    }

    /**
     * Create an instance of {@link QueryAccountConditionValue }
     * 
     */
    public QueryAccountConditionValue createQueryAccountConditionValue() {
        return new QueryAccountConditionValue();
    }

    /**
     * Create an instance of {@link ReportUnlost }
     * 
     */
    public ReportUnlost createReportUnlost() {
        return new ReportUnlost();
    }

    /**
     * Create an instance of {@link ReportUnlostValue }
     * 
     */
    public ReportUnlostValue createReportUnlostValue() {
        return new ReportUnlostValue();
    }

    /**
     * Create an instance of {@link ModifyHGShortCode }
     * 
     */
    public ModifyHGShortCode createModifyHGShortCode() {
        return new ModifyHGShortCode();
    }

    /**
     * Create an instance of {@link ModifyHGShortCodeRequestValue }
     * 
     */
    public ModifyHGShortCodeRequestValue createModifyHGShortCodeRequestValue() {
        return new ModifyHGShortCodeRequestValue();
    }

    /**
     * Create an instance of {@link GetPaymentHistory }
     * 
     */
    public GetPaymentHistory createGetPaymentHistory() {
        return new GetPaymentHistory();
    }

    /**
     * Create an instance of {@link GetPaymentHistoryRequestDetails }
     * 
     */
    public GetPaymentHistoryRequestDetails createGetPaymentHistoryRequestDetails() {
        return new GetPaymentHistoryRequestDetails();
    }

    /**
     * Create an instance of {@link GetFnFNumber }
     * 
     */
    public GetFnFNumber createGetFnFNumber() {
        return new GetFnFNumber();
    }

    /**
     * Create an instance of {@link GetFnFNumberRequestValue }
     * 
     */
    public GetFnFNumberRequestValue createGetFnFNumberRequestValue() {
        return new GetFnFNumberRequestValue();
    }

    /**
     * Create an instance of {@link AddFnFNumber }
     * 
     */
    public AddFnFNumber createAddFnFNumber() {
        return new AddFnFNumber();
    }

    /**
     * Create an instance of {@link AddFnFNumberRequestValue }
     * 
     */
    public AddFnFNumberRequestValue createAddFnFNumberRequestValue() {
        return new AddFnFNumberRequestValue();
    }

    /**
     * Create an instance of {@link ModifyFnFNumber }
     * 
     */
    public ModifyFnFNumber createModifyFnFNumber() {
        return new ModifyFnFNumber();
    }

    /**
     * Create an instance of {@link ModifyFnFNumberRequestValue }
     * 
     */
    public ModifyFnFNumberRequestValue createModifyFnFNumberRequestValue() {
        return new ModifyFnFNumberRequestValue();
    }

    /**
     * Create an instance of {@link CancelFnFNumber }
     * 
     */
    public CancelFnFNumber createCancelFnFNumber() {
        return new CancelFnFNumber();
    }

    /**
     * Create an instance of {@link CancelFnFNumberRequestValue }
     * 
     */
    public CancelFnFNumberRequestValue createCancelFnFNumberRequestValue() {
        return new CancelFnFNumberRequestValue();
    }

    /**
     * Create an instance of {@link GetHistoryFnFNumber }
     * 
     */
    public GetHistoryFnFNumber createGetHistoryFnFNumber() {
        return new GetHistoryFnFNumber();
    }

    /**
     * Create an instance of {@link GetHistoryFnFNumberRequestValue }
     * 
     */
    public GetHistoryFnFNumberRequestValue createGetHistoryFnFNumberRequestValue() {
        return new GetHistoryFnFNumberRequestValue();
    }

    /**
     * Create an instance of {@link CreateCorpCustomer }
     * 
     */
    public CreateCorpCustomer createCreateCorpCustomer() {
        return new CreateCorpCustomer();
    }

    /**
     * Create an instance of {@link CreateCorpCustomerRequestValue }
     * 
     */
    public CreateCorpCustomerRequestValue createCreateCorpCustomerRequestValue() {
        return new CreateCorpCustomerRequestValue();
    }

    /**
     * Create an instance of {@link CreateCorpAPN }
     * 
     */
    public CreateCorpAPN createCreateCorpAPN() {
        return new CreateCorpAPN();
    }

    /**
     * Create an instance of {@link CreateCorpAPNRequestValue }
     * 
     */
    public CreateCorpAPNRequestValue createCreateCorpAPNRequestValue() {
        return new CreateCorpAPNRequestValue();
    }

    /**
     * Create an instance of {@link DeleteCorpAPN }
     * 
     */
    public DeleteCorpAPN createDeleteCorpAPN() {
        return new DeleteCorpAPN();
    }

    /**
     * Create an instance of {@link DeleteCorpAPNRequestValue }
     * 
     */
    public DeleteCorpAPNRequestValue createDeleteCorpAPNRequestValue() {
        return new DeleteCorpAPNRequestValue();
    }

    /**
     * Create an instance of {@link AddCorpAPNMember }
     * 
     */
    public AddCorpAPNMember createAddCorpAPNMember() {
        return new AddCorpAPNMember();
    }

    /**
     * Create an instance of {@link AddCorpAPNMemberRequestValue }
     * 
     */
    public AddCorpAPNMemberRequestValue createAddCorpAPNMemberRequestValue() {
        return new AddCorpAPNMemberRequestValue();
    }

    /**
     * Create an instance of {@link ModifyCorpAPNMemberIP }
     * 
     */
    public ModifyCorpAPNMemberIP createModifyCorpAPNMemberIP() {
        return new ModifyCorpAPNMemberIP();
    }

    /**
     * Create an instance of {@link ModifyCorpAPNMemberIPRequestValue }
     * 
     */
    public ModifyCorpAPNMemberIPRequestValue createModifyCorpAPNMemberIPRequestValue() {
        return new ModifyCorpAPNMemberIPRequestValue();
    }

    /**
     * Create an instance of {@link RemoveCorpAPNMember }
     * 
     */
    public RemoveCorpAPNMember createRemoveCorpAPNMember() {
        return new RemoveCorpAPNMember();
    }

    /**
     * Create an instance of {@link RemoveCorpAPNMemberRequestValue }
     * 
     */
    public RemoveCorpAPNMemberRequestValue createRemoveCorpAPNMemberRequestValue() {
        return new RemoveCorpAPNMemberRequestValue();
    }

    /**
     * Create an instance of {@link GetCorpAPNs }
     * 
     */
    public GetCorpAPNs createGetCorpAPNs() {
        return new GetCorpAPNs();
    }

    /**
     * Create an instance of {@link GetCorpAPNsRequestValue }
     * 
     */
    public GetCorpAPNsRequestValue createGetCorpAPNsRequestValue() {
        return new GetCorpAPNsRequestValue();
    }

    /**
     * Create an instance of {@link GetCorpAPNMembers }
     * 
     */
    public GetCorpAPNMembers createGetCorpAPNMembers() {
        return new GetCorpAPNMembers();
    }

    /**
     * Create an instance of {@link GetCorpAPNMembersRequestValue }
     * 
     */
    public GetCorpAPNMembersRequestValue createGetCorpAPNMembersRequestValue() {
        return new GetCorpAPNMembersRequestValue();
    }

    /**
     * Create an instance of {@link GetCorpAPNHistoryMembers }
     * 
     */
    public GetCorpAPNHistoryMembers createGetCorpAPNHistoryMembers() {
        return new GetCorpAPNHistoryMembers();
    }

    /**
     * Create an instance of {@link GetCorpAPNHistoryMembersRequestValue }
     * 
     */
    public GetCorpAPNHistoryMembersRequestValue createGetCorpAPNHistoryMembersRequestValue() {
        return new GetCorpAPNHistoryMembersRequestValue();
    }

    /**
     * Create an instance of {@link GetAvailableIPs }
     * 
     */
    public GetAvailableIPs createGetAvailableIPs() {
        return new GetAvailableIPs();
    }

    /**
     * Create an instance of {@link GetAvailableIPsRequestDetails }
     * 
     */
    public GetAvailableIPsRequestDetails createGetAvailableIPsRequestDetails() {
        return new GetAvailableIPsRequestDetails();
    }

    /**
     * Create an instance of {@link LockIP }
     * 
     */
    public LockIP createLockIP() {
        return new LockIP();
    }

    /**
     * Create an instance of {@link LockIPRequestDetail }
     * 
     */
    public LockIPRequestDetail createLockIPRequestDetail() {
        return new LockIPRequestDetail();
    }

    /**
     * Create an instance of {@link UnLockIP }
     * 
     */
    public UnLockIP createUnLockIP() {
        return new UnLockIP();
    }

    /**
     * Create an instance of {@link UnLockIPRequestDetail }
     * 
     */
    public UnLockIPRequestDetail createUnLockIPRequestDetail() {
        return new UnLockIPRequestDetail();
    }

    /**
     * Create an instance of {@link CreateCorpVPN }
     * 
     */
    public CreateCorpVPN createCreateCorpVPN() {
        return new CreateCorpVPN();
    }

    /**
     * Create an instance of {@link CreateCorpVPNRequestValue }
     * 
     */
    public CreateCorpVPNRequestValue createCreateCorpVPNRequestValue() {
        return new CreateCorpVPNRequestValue();
    }

    /**
     * Create an instance of {@link AddCorpVPNMember }
     * 
     */
    public AddCorpVPNMember createAddCorpVPNMember() {
        return new AddCorpVPNMember();
    }

    /**
     * Create an instance of {@link AddCorpVPNMemberRequestValue }
     * 
     */
    public AddCorpVPNMemberRequestValue createAddCorpVPNMemberRequestValue() {
        return new AddCorpVPNMemberRequestValue();
    }

    /**
     * Create an instance of {@link RemoveCorpVPNMember }
     * 
     */
    public RemoveCorpVPNMember createRemoveCorpVPNMember() {
        return new RemoveCorpVPNMember();
    }

    /**
     * Create an instance of {@link RemoveCorpVPNMemberRequestValue }
     * 
     */
    public RemoveCorpVPNMemberRequestValue createRemoveCorpVPNMemberRequestValue() {
        return new RemoveCorpVPNMemberRequestValue();
    }

    /**
     * Create an instance of {@link DeleteCorpVPN }
     * 
     */
    public DeleteCorpVPN createDeleteCorpVPN() {
        return new DeleteCorpVPN();
    }

    /**
     * Create an instance of {@link DeleteCorpVPNRequestValue }
     * 
     */
    public DeleteCorpVPNRequestValue createDeleteCorpVPNRequestValue() {
        return new DeleteCorpVPNRequestValue();
    }

    /**
     * Create an instance of {@link GetUsageInfo }
     * 
     */
    public GetUsageInfo createGetUsageInfo() {
        return new GetUsageInfo();
    }

    /**
     * Create an instance of {@link WSQueryUsageConditionValue }
     * 
     */
    public WSQueryUsageConditionValue createWSQueryUsageConditionValue() {
        return new WSQueryUsageConditionValue();
    }

    /**
     * Create an instance of {@link GetMsisdnByIccid }
     * 
     */
    public GetMsisdnByIccid createGetMsisdnByIccid() {
        return new GetMsisdnByIccid();
    }

    /**
     * Create an instance of {@link QueryMsisdnByIccidRequestValue }
     * 
     */
    public QueryMsisdnByIccidRequestValue createQueryMsisdnByIccidRequestValue() {
        return new QueryMsisdnByIccidRequestValue();
    }

    /**
     * Create an instance of {@link ModifyCorpAPN }
     * 
     */
    public ModifyCorpAPN createModifyCorpAPN() {
        return new ModifyCorpAPN();
    }

    /**
     * Create an instance of {@link ModifyCorpAPNRequestValue }
     * 
     */
    public ModifyCorpAPNRequestValue createModifyCorpAPNRequestValue() {
        return new ModifyCorpAPNRequestValue();
    }

    /**
     * Create an instance of {@link TransferAccount }
     * 
     */
    public TransferAccount createTransferAccount() {
        return new TransferAccount();
    }

    /**
     * Create an instance of {@link TransferAccountRequestValue }
     * 
     */
    public TransferAccountRequestValue createTransferAccountRequestValue() {
        return new TransferAccountRequestValue();
    }

    /**
     * Create an instance of {@link AddFreeUnits }
     * 
     */
    public AddFreeUnits createAddFreeUnits() {
        return new AddFreeUnits();
    }

    /**
     * Create an instance of {@link AddFreeUnitsRequestValue }
     * 
     */
    public AddFreeUnitsRequestValue createAddFreeUnitsRequestValue() {
        return new AddFreeUnitsRequestValue();
    }

    /**
     * Create an instance of {@link GetMultiSimSubInfo }
     * 
     */
    public GetMultiSimSubInfo createGetMultiSimSubInfo() {
        return new GetMultiSimSubInfo();
    }

    /**
     * Create an instance of {@link GetMultiSimSubInfoRequestValue }
     * 
     */
    public GetMultiSimSubInfoRequestValue createGetMultiSimSubInfoRequestValue() {
        return new GetMultiSimSubInfoRequestValue();
    }

    /**
     * Create an instance of {@link ChangeMainAccount }
     * 
     */
    public ChangeMainAccount createChangeMainAccount() {
        return new ChangeMainAccount();
    }

    /**
     * Create an instance of {@link ChangeMainAccountValue }
     * 
     */
    public ChangeMainAccountValue createChangeMainAccountValue() {
        return new ChangeMainAccountValue();
    }

    /**
     * Create an instance of {@link SuspendGroupResponse }
     * 
     */
    public SuspendGroupResponse createSuspendGroupResponse() {
        return new SuspendGroupResponse();
    }

    /**
     * Create an instance of {@link ResultOfOperationValue }
     * 
     */
    public ResultOfOperationValue createResultOfOperationValue() {
        return new ResultOfOperationValue();
    }

    /**
     * Create an instance of {@link ResumeGroupResponse }
     * 
     */
    public ResumeGroupResponse createResumeGroupResponse() {
        return new ResumeGroupResponse();
    }

    /**
     * Create an instance of {@link GetSimMsisdnResourceResponse }
     * 
     */
    public GetSimMsisdnResourceResponse createGetSimMsisdnResourceResponse() {
        return new GetSimMsisdnResourceResponse();
    }

    /**
     * Create an instance of {@link QuerySimMsisdnResResultValue }
     * 
     */
    public QuerySimMsisdnResResultValue createQuerySimMsisdnResResultValue() {
        return new QuerySimMsisdnResResultValue();
    }

    /**
     * Create an instance of {@link ManageSimMsisdnResourceResponse }
     * 
     */
    public ManageSimMsisdnResourceResponse createManageSimMsisdnResourceResponse() {
        return new ManageSimMsisdnResourceResponse();
    }

    /**
     * Create an instance of {@link RemoveHGShortCodeResponse }
     * 
     */
    public RemoveHGShortCodeResponse createRemoveHGShortCodeResponse() {
        return new RemoveHGShortCodeResponse();
    }

    /**
     * Create an instance of {@link ResultOfRemoveHGShortCodeValue }
     * 
     */
    public ResultOfRemoveHGShortCodeValue createResultOfRemoveHGShortCodeValue() {
        return new ResultOfRemoveHGShortCodeValue();
    }

    /**
     * Create an instance of {@link GetResourcesResponse }
     * 
     */
    public GetResourcesResponse createGetResourcesResponse() {
        return new GetResourcesResponse();
    }

    /**
     * Create an instance of {@link GetWSResourceValueList }
     * 
     */
    public GetWSResourceValueList createGetWSResourceValueList() {
        return new GetWSResourceValueList();
    }

    /**
     * Create an instance of {@link GetSignedContractsResponse }
     * 
     */
    public GetSignedContractsResponse createGetSignedContractsResponse() {
        return new GetSignedContractsResponse();
    }

    /**
     * Create an instance of {@link ChangeHybridToPrepaidResponse }
     * 
     */
    public ChangeHybridToPrepaidResponse createChangeHybridToPrepaidResponse() {
        return new ChangeHybridToPrepaidResponse();
    }

    /**
     * Create an instance of {@link ResultOfCreateNewSubscriberValue }
     * 
     */
    public ResultOfCreateNewSubscriberValue createResultOfCreateNewSubscriberValue() {
        return new ResultOfCreateNewSubscriberValue();
    }

    /**
     * Create an instance of {@link MNPPortInResponse }
     * 
     */
    public MNPPortInResponse createMNPPortInResponse() {
        return new MNPPortInResponse();
    }

    /**
     * Create an instance of {@link ModifyAccountInfoResponse }
     * 
     */
    public ModifyAccountInfoResponse createModifyAccountInfoResponse() {
        return new ModifyAccountInfoResponse();
    }

    /**
     * Create an instance of {@link QuerySubscribeAccessoriesResponse }
     * 
     */
    public QuerySubscribeAccessoriesResponse createQuerySubscribeAccessoriesResponse() {
        return new QuerySubscribeAccessoriesResponse();
    }

    /**
     * Create an instance of {@link QuerySubscribeAccessoriesResultList }
     * 
     */
    public QuerySubscribeAccessoriesResultList createQuerySubscribeAccessoriesResultList() {
        return new QuerySubscribeAccessoriesResultList();
    }

    /**
     * Create an instance of {@link DeactivateSubscriberResponse }
     * 
     */
    public DeactivateSubscriberResponse createDeactivateSubscriberResponse() {
        return new DeactivateSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetCUGMembersResponse }
     * 
     */
    public GetCUGMembersResponse createGetCUGMembersResponse() {
        return new GetCUGMembersResponse();
    }

    /**
     * Create an instance of {@link QueryCUGMembersResultValue }
     * 
     */
    public QueryCUGMembersResultValue createQueryCUGMembersResultValue() {
        return new QueryCUGMembersResultValue();
    }

    /**
     * Create an instance of {@link GetZGCsResponse }
     * 
     */
    public GetZGCsResponse createGetZGCsResponse() {
        return new GetZGCsResponse();
    }

    /**
     * Create an instance of {@link GetZGCResultValue }
     * 
     */
    public GetZGCResultValue createGetZGCResultValue() {
        return new GetZGCResultValue();
    }

    /**
     * Create an instance of {@link GetXMLBillResponse }
     * 
     */
    public GetXMLBillResponse createGetXMLBillResponse() {
        return new GetXMLBillResponse();
    }

    /**
     * Create an instance of {@link GetXMLBillReplyDetails }
     * 
     */
    public GetXMLBillReplyDetails createGetXMLBillReplyDetails() {
        return new GetXMLBillReplyDetails();
    }

    /**
     * Create an instance of {@link AddCUGMemberResponse }
     * 
     */
    public AddCUGMemberResponse createAddCUGMemberResponse() {
        return new AddCUGMemberResponse();
    }

    /**
     * Create an instance of {@link MNPRegisterSTPResponse }
     * 
     */
    public MNPRegisterSTPResponse createMNPRegisterSTPResponse() {
        return new MNPRegisterSTPResponse();
    }

    /**
     * Create an instance of {@link UnBarringSubscriberResponse }
     * 
     */
    public UnBarringSubscriberResponse createUnBarringSubscriberResponse() {
        return new UnBarringSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetVoucherCardsResponse }
     * 
     */
    public GetVoucherCardsResponse createGetVoucherCardsResponse() {
        return new GetVoucherCardsResponse();
    }

    /**
     * Create an instance of {@link ModifyCustomerInfoResponse }
     * 
     */
    public ModifyCustomerInfoResponse createModifyCustomerInfoResponse() {
        return new ModifyCustomerInfoResponse();
    }

    /**
     * Create an instance of {@link QueryBalanceResponse }
     * 
     */
    public QueryBalanceResponse createQueryBalanceResponse() {
        return new QueryBalanceResponse();
    }

    /**
     * Create an instance of {@link RechargeByVoucherCardResponse }
     * 
     */
    public RechargeByVoucherCardResponse createRechargeByVoucherCardResponse() {
        return new RechargeByVoucherCardResponse();
    }

    /**
     * Create an instance of {@link ChangeSubscriberProductsResponse }
     * 
     */
    public ChangeSubscriberProductsResponse createChangeSubscriberProductsResponse() {
        return new ChangeSubscriberProductsResponse();
    }

    /**
     * Create an instance of {@link HandlePendingResponse }
     * 
     */
    public HandlePendingResponse createHandlePendingResponse() {
        return new HandlePendingResponse();
    }

    /**
     * Create an instance of {@link RemoveCUGMemberResponse }
     * 
     */
    public RemoveCUGMemberResponse createRemoveCUGMemberResponse() {
        return new RemoveCUGMemberResponse();
    }

    /**
     * Create an instance of {@link CreateNewSubscriberResponse }
     * 
     */
    public CreateNewSubscriberResponse createCreateNewSubscriberResponse() {
        return new CreateNewSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetUsageAmountResponse }
     * 
     */
    public GetUsageAmountResponse createGetUsageAmountResponse() {
        return new GetUsageAmountResponse();
    }

    /**
     * Create an instance of {@link GetUsageAmountReplyDetails }
     * 
     */
    public GetUsageAmountReplyDetails createGetUsageAmountReplyDetails() {
        return new GetUsageAmountReplyDetails();
    }

    /**
     * Create an instance of {@link GetReceiptDataResponse }
     * 
     */
    public GetReceiptDataResponse createGetReceiptDataResponse() {
        return new GetReceiptDataResponse();
    }

    /**
     * Create an instance of {@link GetReceiptDataReplyDetails }
     * 
     */
    public GetReceiptDataReplyDetails createGetReceiptDataReplyDetails() {
        return new GetReceiptDataReplyDetails();
    }

    /**
     * Create an instance of {@link IsOrderZoneResponse }
     * 
     */
    public IsOrderZoneResponse createIsOrderZoneResponse() {
        return new IsOrderZoneResponse();
    }

    /**
     * Create an instance of {@link ResultOfIsOrderZoneValue }
     * 
     */
    public ResultOfIsOrderZoneValue createResultOfIsOrderZoneValue() {
        return new ResultOfIsOrderZoneValue();
    }

    /**
     * Create an instance of {@link LockMSISDNResponse }
     * 
     */
    public LockMSISDNResponse createLockMSISDNResponse() {
        return new LockMSISDNResponse();
    }

    /**
     * Create an instance of {@link LockMSISDNResplyDetail }
     * 
     */
    public LockMSISDNResplyDetail createLockMSISDNResplyDetail() {
        return new LockMSISDNResplyDetail();
    }

    /**
     * Create an instance of {@link GetCDRHistoryResponse }
     * 
     */
    public GetCDRHistoryResponse createGetCDRHistoryResponse() {
        return new GetCDRHistoryResponse();
    }

    /**
     * Create an instance of {@link GetCDRHistoryResult }
     * 
     */
    public GetCDRHistoryResult createGetCDRHistoryResult() {
        return new GetCDRHistoryResult();
    }

    /**
     * Create an instance of {@link AdjustCreditResponse }
     * 
     */
    public AdjustCreditResponse createAdjustCreditResponse() {
        return new AdjustCreditResponse();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersResponse }
     * 
     */
    public GetPhoneNumbersResponse createGetPhoneNumbersResponse() {
        return new GetPhoneNumbersResponse();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersReplyDetails }
     * 
     */
    public GetPhoneNumbersReplyDetails createGetPhoneNumbersReplyDetails() {
        return new GetPhoneNumbersReplyDetails();
    }

    /**
     * Create an instance of {@link ReportLostResponse }
     * 
     */
    public ReportLostResponse createReportLostResponse() {
        return new ReportLostResponse();
    }

    /**
     * Create an instance of {@link OrderZoneResponse }
     * 
     */
    public OrderZoneResponse createOrderZoneResponse() {
        return new OrderZoneResponse();
    }

    /**
     * Create an instance of {@link ResultOfOrderZoneValue }
     * 
     */
    public ResultOfOrderZoneValue createResultOfOrderZoneValue() {
        return new ResultOfOrderZoneValue();
    }

    /**
     * Create an instance of {@link ChangePostpaidToHybridResponse }
     * 
     */
    public ChangePostpaidToHybridResponse createChangePostpaidToHybridResponse() {
        return new ChangePostpaidToHybridResponse();
    }

    /**
     * Create an instance of {@link PaymentFromCustomerResponse }
     * 
     */
    public PaymentFromCustomerResponse createPaymentFromCustomerResponse() {
        return new PaymentFromCustomerResponse();
    }

    /**
     * Create an instance of {@link PaymentFromCustomerReplyDetails }
     * 
     */
    public PaymentFromCustomerReplyDetails createPaymentFromCustomerReplyDetails() {
        return new PaymentFromCustomerReplyDetails();
    }

    /**
     * Create an instance of {@link ChangePostpaidToPrepaidResponse }
     * 
     */
    public ChangePostpaidToPrepaidResponse createChangePostpaidToPrepaidResponse() {
        return new ChangePostpaidToPrepaidResponse();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     * 
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    /**
     * Create an instance of {@link UpdateAdditionalNumberResponse }
     * 
     */
    public UpdateAdditionalNumberResponse createUpdateAdditionalNumberResponse() {
        return new UpdateAdditionalNumberResponse();
    }

    /**
     * Create an instance of {@link ResultOfUpdateAdditionalNumberValue }
     * 
     */
    public ResultOfUpdateAdditionalNumberValue createResultOfUpdateAdditionalNumberValue() {
        return new ResultOfUpdateAdditionalNumberValue();
    }

    /**
     * Create an instance of {@link BarringSubscriberResponse }
     * 
     */
    public BarringSubscriberResponse createBarringSubscriberResponse() {
        return new BarringSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetCustomersResponse }
     * 
     */
    public GetCustomersResponse createGetCustomersResponse() {
        return new GetCustomersResponse();
    }

    /**
     * Create an instance of {@link GetCustomersResult }
     * 
     */
    public GetCustomersResult createGetCustomersResult() {
        return new GetCustomersResult();
    }

    /**
     * Create an instance of {@link ChangeHybridToPostpaidResponse }
     * 
     */
    public ChangeHybridToPostpaidResponse createChangeHybridToPostpaidResponse() {
        return new ChangeHybridToPostpaidResponse();
    }

    /**
     * Create an instance of {@link GetFreeUnitsResponse }
     * 
     */
    public GetFreeUnitsResponse createGetFreeUnitsResponse() {
        return new GetFreeUnitsResponse();
    }

    /**
     * Create an instance of {@link GetValidRatePlansResponse }
     * 
     */
    public GetValidRatePlansResponse createGetValidRatePlansResponse() {
        return new GetValidRatePlansResponse();
    }

    /**
     * Create an instance of {@link GetValidProductsResponseDetail }
     * 
     */
    public GetValidProductsResponseDetail createGetValidProductsResponseDetail() {
        return new GetValidProductsResponseDetail();
    }

    /**
     * Create an instance of {@link ResumeSubscriberResponse }
     * 
     */
    public ResumeSubscriberResponse createResumeSubscriberResponse() {
        return new ResumeSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetCUGsResponse }
     * 
     */
    public GetCUGsResponse createGetCUGsResponse() {
        return new GetCUGsResponse();
    }

    /**
     * Create an instance of {@link QueryCUGResultValue }
     * 
     */
    public QueryCUGResultValue createQueryCUGResultValue() {
        return new QueryCUGResultValue();
    }

    /**
     * Create an instance of {@link GetSimCardsResponse }
     * 
     */
    public GetSimCardsResponse createGetSimCardsResponse() {
        return new GetSimCardsResponse();
    }

    /**
     * Create an instance of {@link GetSimCardsReplyDetails }
     * 
     */
    public GetSimCardsReplyDetails createGetSimCardsReplyDetails() {
        return new GetSimCardsReplyDetails();
    }

    /**
     * Create an instance of {@link QueryCreditLimitResponse }
     * 
     */
    public QueryCreditLimitResponse createQueryCreditLimitResponse() {
        return new QueryCreditLimitResponse();
    }

    /**
     * Create an instance of {@link ResultOfCreditLimitValue }
     * 
     */
    public ResultOfCreditLimitValue createResultOfCreditLimitValue() {
        return new ResultOfCreditLimitValue();
    }

    /**
     * Create an instance of {@link AddHGShortCodeResponse }
     * 
     */
    public AddHGShortCodeResponse createAddHGShortCodeResponse() {
        return new AddHGShortCodeResponse();
    }

    /**
     * Create an instance of {@link ResultOfAddHGShortCodeValue }
     * 
     */
    public ResultOfAddHGShortCodeValue createResultOfAddHGShortCodeValue() {
        return new ResultOfAddHGShortCodeValue();
    }

    /**
     * Create an instance of {@link GetDictTableResponse }
     * 
     */
    public GetDictTableResponse createGetDictTableResponse() {
        return new GetDictTableResponse();
    }

    /**
     * Create an instance of {@link GetDictTableResultList }
     * 
     */
    public GetDictTableResultList createGetDictTableResultList() {
        return new GetDictTableResultList();
    }

    /**
     * Create an instance of {@link PrepaidActivationResponse }
     * 
     */
    public PrepaidActivationResponse createPrepaidActivationResponse() {
        return new PrepaidActivationResponse();
    }

    /**
     * Create an instance of {@link GetPendingBusiInfosResponse }
     * 
     */
    public GetPendingBusiInfosResponse createGetPendingBusiInfosResponse() {
        return new GetPendingBusiInfosResponse();
    }

    /**
     * Create an instance of {@link GetFreeUnitOfCumulationResponse }
     * 
     */
    public GetFreeUnitOfCumulationResponse createGetFreeUnitOfCumulationResponse() {
        return new GetFreeUnitOfCumulationResponse();
    }

    /**
     * Create an instance of {@link SetAccountRelationResponse }
     * 
     */
    public SetAccountRelationResponse createSetAccountRelationResponse() {
        return new SetAccountRelationResponse();
    }

    /**
     * Create an instance of {@link GetUnbilledInfoResponse }
     * 
     */
    public GetUnbilledInfoResponse createGetUnbilledInfoResponse() {
        return new GetUnbilledInfoResponse();
    }

    /**
     * Create an instance of {@link GetSubscriberStatusHistoryResponse }
     * 
     */
    public GetSubscriberStatusHistoryResponse createGetSubscriberStatusHistoryResponse() {
        return new GetSubscriberStatusHistoryResponse();
    }

    /**
     * Create an instance of {@link GetSubscriberStatusHistoryResult }
     * 
     */
    public GetSubscriberStatusHistoryResult createGetSubscriberStatusHistoryResult() {
        return new GetSubscriberStatusHistoryResult();
    }

    /**
     * Create an instance of {@link CreateCUGResponse }
     * 
     */
    public CreateCUGResponse createCreateCUGResponse() {
        return new CreateCUGResponse();
    }

    /**
     * Create an instance of {@link UpdateZoneResponse }
     * 
     */
    public UpdateZoneResponse createUpdateZoneResponse() {
        return new UpdateZoneResponse();
    }

    /**
     * Create an instance of {@link ResultOfUpdateZoneValue }
     * 
     */
    public ResultOfUpdateZoneValue createResultOfUpdateZoneValue() {
        return new ResultOfUpdateZoneValue();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersQuantityResponse }
     * 
     */
    public GetPhoneNumbersQuantityResponse createGetPhoneNumbersQuantityResponse() {
        return new GetPhoneNumbersQuantityResponse();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersQuantityReplyDetail }
     * 
     */
    public GetPhoneNumbersQuantityReplyDetail createGetPhoneNumbersQuantityReplyDetail() {
        return new GetPhoneNumbersQuantityReplyDetail();
    }

    /**
     * Create an instance of {@link AutoRefillRegisterResponse }
     * 
     */
    public AutoRefillRegisterResponse createAutoRefillRegisterResponse() {
        return new AutoRefillRegisterResponse();
    }

    /**
     * Create an instance of {@link PurchaseGoodsResponse }
     * 
     */
    public PurchaseGoodsResponse createPurchaseGoodsResponse() {
        return new PurchaseGoodsResponse();
    }

    /**
     * Create an instance of {@link ChangeSimCardResponse }
     * 
     */
    public ChangeSimCardResponse createChangeSimCardResponse() {
        return new ChangeSimCardResponse();
    }

    /**
     * Create an instance of {@link CancelZoneResponse }
     * 
     */
    public CancelZoneResponse createCancelZoneResponse() {
        return new CancelZoneResponse();
    }

    /**
     * Create an instance of {@link ResultOfCancelZoneValue }
     * 
     */
    public ResultOfCancelZoneValue createResultOfCancelZoneValue() {
        return new ResultOfCancelZoneValue();
    }

    /**
     * Create an instance of {@link GetUserSubscribeStateResponse }
     * 
     */
    public GetUserSubscribeStateResponse createGetUserSubscribeStateResponse() {
        return new GetUserSubscribeStateResponse();
    }

    /**
     * Create an instance of {@link EAIWSResultOfSubscribeStateValue }
     * 
     */
    public EAIWSResultOfSubscribeStateValue createEAIWSResultOfSubscribeStateValue() {
        return new EAIWSResultOfSubscribeStateValue();
    }

    /**
     * Create an instance of {@link SendSMSResponse }
     * 
     */
    public SendSMSResponse createSendSMSResponse() {
        return new SendSMSResponse();
    }

    /**
     * Create an instance of {@link GetBusiInfosResponse }
     * 
     */
    public GetBusiInfosResponse createGetBusiInfosResponse() {
        return new GetBusiInfosResponse();
    }

    /**
     * Create an instance of {@link LockSIMResponse }
     * 
     */
    public LockSIMResponse createLockSIMResponse() {
        return new LockSIMResponse();
    }

    /**
     * Create an instance of {@link LockSIMResplyDetail }
     * 
     */
    public LockSIMResplyDetail createLockSIMResplyDetail() {
        return new LockSIMResplyDetail();
    }

    /**
     * Create an instance of {@link ChangePrepaidToPostpaidResponse }
     * 
     */
    public ChangePrepaidToPostpaidResponse createChangePrepaidToPostpaidResponse() {
        return new ChangePrepaidToPostpaidResponse();
    }

    /**
     * Create an instance of {@link UnLockMSISDNResponse }
     * 
     */
    public UnLockMSISDNResponse createUnLockMSISDNResponse() {
        return new UnLockMSISDNResponse();
    }

    /**
     * Create an instance of {@link UnLockMSISDNResplyDetail }
     * 
     */
    public UnLockMSISDNResplyDetail createUnLockMSISDNResplyDetail() {
        return new UnLockMSISDNResplyDetail();
    }

    /**
     * Create an instance of {@link GetFeeforServiceResponse }
     * 
     */
    public GetFeeforServiceResponse createGetFeeforServiceResponse() {
        return new GetFeeforServiceResponse();
    }

    /**
     * Create an instance of {@link EAIWSResultOfServiceFeeValue }
     * 
     */
    public EAIWSResultOfServiceFeeValue createEAIWSResultOfServiceFeeValue() {
        return new EAIWSResultOfServiceFeeValue();
    }

    /**
     * Create an instance of {@link QueryDCCHistoryResponse }
     * 
     */
    public QueryDCCHistoryResponse createQueryDCCHistoryResponse() {
        return new QueryDCCHistoryResponse();
    }

    /**
     * Create an instance of {@link ResultOfQueryDCCHistoryValue }
     * 
     */
    public ResultOfQueryDCCHistoryValue createResultOfQueryDCCHistoryValue() {
        return new ResultOfQueryDCCHistoryValue();
    }

    /**
     * Create an instance of {@link GetGoodsSaleHistoryResponse }
     * 
     */
    public GetGoodsSaleHistoryResponse createGetGoodsSaleHistoryResponse() {
        return new GetGoodsSaleHistoryResponse();
    }

    /**
     * Create an instance of {@link GoodsSaleHistoryValues }
     * 
     */
    public GoodsSaleHistoryValues createGoodsSaleHistoryValues() {
        return new GoodsSaleHistoryValues();
    }

    /**
     * Create an instance of {@link MNPOutComeBackResponse }
     * 
     */
    public MNPOutComeBackResponse createMNPOutComeBackResponse() {
        return new MNPOutComeBackResponse();
    }

    /**
     * Create an instance of {@link ModifyActualUserResponse }
     * 
     */
    public ModifyActualUserResponse createModifyActualUserResponse() {
        return new ModifyActualUserResponse();
    }

    /**
     * Create an instance of {@link ChangePrepaidToHybridResponse }
     * 
     */
    public ChangePrepaidToHybridResponse createChangePrepaidToHybridResponse() {
        return new ChangePrepaidToHybridResponse();
    }

    /**
     * Create an instance of {@link ChangePaymentModeInfoResponse }
     * 
     */
    public ChangePaymentModeInfoResponse createChangePaymentModeInfoResponse() {
        return new ChangePaymentModeInfoResponse();
    }

    /**
     * Create an instance of {@link ChangeMsisdnResponse }
     * 
     */
    public ChangeMsisdnResponse createChangeMsisdnResponse() {
        return new ChangeMsisdnResponse();
    }

    /**
     * Create an instance of {@link SuspendSubscriberResponse }
     * 
     */
    public SuspendSubscriberResponse createSuspendSubscriberResponse() {
        return new SuspendSubscriberResponse();
    }

    /**
     * Create an instance of {@link GetOutstandingResponse }
     * 
     */
    public GetOutstandingResponse createGetOutstandingResponse() {
        return new GetOutstandingResponse();
    }

    /**
     * Create an instance of {@link GetOutstandingReplyDetails }
     * 
     */
    public GetOutstandingReplyDetails createGetOutstandingReplyDetails() {
        return new GetOutstandingReplyDetails();
    }

    /**
     * Create an instance of {@link AdjustFreeUnitsResponse }
     * 
     */
    public AdjustFreeUnitsResponse createAdjustFreeUnitsResponse() {
        return new AdjustFreeUnitsResponse();
    }

    /**
     * Create an instance of {@link FreeAdjustResultValue }
     * 
     */
    public FreeAdjustResultValue createFreeAdjustResultValue() {
        return new FreeAdjustResultValue();
    }

    /**
     * Create an instance of {@link VoidPaymentTransactionResponse }
     * 
     */
    public VoidPaymentTransactionResponse createVoidPaymentTransactionResponse() {
        return new VoidPaymentTransactionResponse();
    }

    /**
     * Create an instance of {@link GetUsingProductsResponse }
     * 
     */
    public GetUsingProductsResponse createGetUsingProductsResponse() {
        return new GetUsingProductsResponse();
    }

    /**
     * Create an instance of {@link GetUsingProductsResult }
     * 
     */
    public GetUsingProductsResult createGetUsingProductsResult() {
        return new GetUsingProductsResult();
    }

    /**
     * Create an instance of {@link GetDunningHistoryResponse }
     * 
     */
    public GetDunningHistoryResponse createGetDunningHistoryResponse() {
        return new GetDunningHistoryResponse();
    }

    /**
     * Create an instance of {@link GetDunningHistoryReplyDetails }
     * 
     */
    public GetDunningHistoryReplyDetails createGetDunningHistoryReplyDetails() {
        return new GetDunningHistoryReplyDetails();
    }

    /**
     * Create an instance of {@link GetValidGoodsProgramResponse }
     * 
     */
    public GetValidGoodsProgramResponse createGetValidGoodsProgramResponse() {
        return new GetValidGoodsProgramResponse();
    }

    /**
     * Create an instance of {@link EAIWSResultOfGoodsListValue }
     * 
     */
    public EAIWSResultOfGoodsListValue createEAIWSResultOfGoodsListValue() {
        return new EAIWSResultOfGoodsListValue();
    }

    /**
     * Create an instance of {@link TransferOwnershipResponse }
     * 
     */
    public TransferOwnershipResponse createTransferOwnershipResponse() {
        return new TransferOwnershipResponse();
    }

    /**
     * Create an instance of {@link ModifySubscriberInfoResponse }
     * 
     */
    public ModifySubscriberInfoResponse createModifySubscriberInfoResponse() {
        return new ModifySubscriberInfoResponse();
    }

    /**
     * Create an instance of {@link GetSubscribersResponse }
     * 
     */
    public GetSubscribersResponse createGetSubscribersResponse() {
        return new GetSubscribersResponse();
    }

    /**
     * Create an instance of {@link GetSubscribersResult }
     * 
     */
    public GetSubscribersResult createGetSubscribersResult() {
        return new GetSubscribersResult();
    }

    /**
     * Create an instance of {@link ChangeResourceStatusResponse }
     * 
     */
    public ChangeResourceStatusResponse createChangeResourceStatusResponse() {
        return new ChangeResourceStatusResponse();
    }

    /**
     * Create an instance of {@link UnLockSIMResponse }
     * 
     */
    public UnLockSIMResponse createUnLockSIMResponse() {
        return new UnLockSIMResponse();
    }

    /**
     * Create an instance of {@link UnLockSIMResplyDetail }
     * 
     */
    public UnLockSIMResplyDetail createUnLockSIMResplyDetail() {
        return new UnLockSIMResplyDetail();
    }

    /**
     * Create an instance of {@link GetValidServicePackagesResponse }
     * 
     */
    public GetValidServicePackagesResponse createGetValidServicePackagesResponse() {
        return new GetValidServicePackagesResponse();
    }

    /**
     * Create an instance of {@link MNPDeactivateSubResponse }
     * 
     */
    public MNPDeactivateSubResponse createMNPDeactivateSubResponse() {
        return new MNPDeactivateSubResponse();
    }

    /**
     * Create an instance of {@link QuerySMSNoticeHistoryResponse }
     * 
     */
    public QuerySMSNoticeHistoryResponse createQuerySMSNoticeHistoryResponse() {
        return new QuerySMSNoticeHistoryResponse();
    }

    /**
     * Create an instance of {@link ResultOfQuerySMSNoticeHistoryValue }
     * 
     */
    public ResultOfQuerySMSNoticeHistoryValue createResultOfQuerySMSNoticeHistoryValue() {
        return new ResultOfQuerySMSNoticeHistoryValue();
    }

    /**
     * Create an instance of {@link GetAccountsResponse }
     * 
     */
    public GetAccountsResponse createGetAccountsResponse() {
        return new GetAccountsResponse();
    }

    /**
     * Create an instance of {@link GetAccountResult }
     * 
     */
    public GetAccountResult createGetAccountResult() {
        return new GetAccountResult();
    }

    /**
     * Create an instance of {@link ReportUnlostResponse }
     * 
     */
    public ReportUnlostResponse createReportUnlostResponse() {
        return new ReportUnlostResponse();
    }

    /**
     * Create an instance of {@link ModifyHGShortCodeResponse }
     * 
     */
    public ModifyHGShortCodeResponse createModifyHGShortCodeResponse() {
        return new ModifyHGShortCodeResponse();
    }

    /**
     * Create an instance of {@link ResultOfModifyHGShortCodeValue }
     * 
     */
    public ResultOfModifyHGShortCodeValue createResultOfModifyHGShortCodeValue() {
        return new ResultOfModifyHGShortCodeValue();
    }

    /**
     * Create an instance of {@link GetPaymentHistoryResponse }
     * 
     */
    public GetPaymentHistoryResponse createGetPaymentHistoryResponse() {
        return new GetPaymentHistoryResponse();
    }

    /**
     * Create an instance of {@link GetPaymentHistoryReplyDetails }
     * 
     */
    public GetPaymentHistoryReplyDetails createGetPaymentHistoryReplyDetails() {
        return new GetPaymentHistoryReplyDetails();
    }

    /**
     * Create an instance of {@link GetFnFNumberResponse }
     * 
     */
    public GetFnFNumberResponse createGetFnFNumberResponse() {
        return new GetFnFNumberResponse();
    }

    /**
     * Create an instance of {@link GetFnFNumberResult }
     * 
     */
    public GetFnFNumberResult createGetFnFNumberResult() {
        return new GetFnFNumberResult();
    }

    /**
     * Create an instance of {@link AddFnFNumberResponse }
     * 
     */
    public AddFnFNumberResponse createAddFnFNumberResponse() {
        return new AddFnFNumberResponse();
    }

    /**
     * Create an instance of {@link ModifyFnFNumberResponse }
     * 
     */
    public ModifyFnFNumberResponse createModifyFnFNumberResponse() {
        return new ModifyFnFNumberResponse();
    }

    /**
     * Create an instance of {@link CancelFnFNumberResponse }
     * 
     */
    public CancelFnFNumberResponse createCancelFnFNumberResponse() {
        return new CancelFnFNumberResponse();
    }

    /**
     * Create an instance of {@link GetHistoryFnFNumberResponse }
     * 
     */
    public GetHistoryFnFNumberResponse createGetHistoryFnFNumberResponse() {
        return new GetHistoryFnFNumberResponse();
    }

    /**
     * Create an instance of {@link GetHistoryFnFNumberResult }
     * 
     */
    public GetHistoryFnFNumberResult createGetHistoryFnFNumberResult() {
        return new GetHistoryFnFNumberResult();
    }

    /**
     * Create an instance of {@link CreateCorpCustomerResponse }
     * 
     */
    public CreateCorpCustomerResponse createCreateCorpCustomerResponse() {
        return new CreateCorpCustomerResponse();
    }

    /**
     * Create an instance of {@link ResultOfCreateCorpValue }
     * 
     */
    public ResultOfCreateCorpValue createResultOfCreateCorpValue() {
        return new ResultOfCreateCorpValue();
    }

    /**
     * Create an instance of {@link CreateCorpAPNResponse }
     * 
     */
    public CreateCorpAPNResponse createCreateCorpAPNResponse() {
        return new CreateCorpAPNResponse();
    }

    /**
     * Create an instance of {@link DeleteCorpAPNResponse }
     * 
     */
    public DeleteCorpAPNResponse createDeleteCorpAPNResponse() {
        return new DeleteCorpAPNResponse();
    }

    /**
     * Create an instance of {@link AddCorpAPNMemberResponse }
     * 
     */
    public AddCorpAPNMemberResponse createAddCorpAPNMemberResponse() {
        return new AddCorpAPNMemberResponse();
    }

    /**
     * Create an instance of {@link ModifyCorpAPNMemberIPResponse }
     * 
     */
    public ModifyCorpAPNMemberIPResponse createModifyCorpAPNMemberIPResponse() {
        return new ModifyCorpAPNMemberIPResponse();
    }

    /**
     * Create an instance of {@link RemoveCorpAPNMemberResponse }
     * 
     */
    public RemoveCorpAPNMemberResponse createRemoveCorpAPNMemberResponse() {
        return new RemoveCorpAPNMemberResponse();
    }

    /**
     * Create an instance of {@link GetCorpAPNsResponse }
     * 
     */
    public GetCorpAPNsResponse createGetCorpAPNsResponse() {
        return new GetCorpAPNsResponse();
    }

    /**
     * Create an instance of {@link GetCorpAPNsResultValue }
     * 
     */
    public GetCorpAPNsResultValue createGetCorpAPNsResultValue() {
        return new GetCorpAPNsResultValue();
    }

    /**
     * Create an instance of {@link GetCorpAPNMembersResponse }
     * 
     */
    public GetCorpAPNMembersResponse createGetCorpAPNMembersResponse() {
        return new GetCorpAPNMembersResponse();
    }

    /**
     * Create an instance of {@link GetCorpAPNMembersResultValue }
     * 
     */
    public GetCorpAPNMembersResultValue createGetCorpAPNMembersResultValue() {
        return new GetCorpAPNMembersResultValue();
    }

    /**
     * Create an instance of {@link GetCorpAPNHistoryMembersResponse }
     * 
     */
    public GetCorpAPNHistoryMembersResponse createGetCorpAPNHistoryMembersResponse() {
        return new GetCorpAPNHistoryMembersResponse();
    }

    /**
     * Create an instance of {@link GetCorpAPNHistoryMembersResultValue }
     * 
     */
    public GetCorpAPNHistoryMembersResultValue createGetCorpAPNHistoryMembersResultValue() {
        return new GetCorpAPNHistoryMembersResultValue();
    }

    /**
     * Create an instance of {@link GetAvailableIPsResponse }
     * 
     */
    public GetAvailableIPsResponse createGetAvailableIPsResponse() {
        return new GetAvailableIPsResponse();
    }

    /**
     * Create an instance of {@link GetAvailableIPsReplyDetails }
     * 
     */
    public GetAvailableIPsReplyDetails createGetAvailableIPsReplyDetails() {
        return new GetAvailableIPsReplyDetails();
    }

    /**
     * Create an instance of {@link LockIPResponse }
     * 
     */
    public LockIPResponse createLockIPResponse() {
        return new LockIPResponse();
    }

    /**
     * Create an instance of {@link LockIPReplyDetail }
     * 
     */
    public LockIPReplyDetail createLockIPReplyDetail() {
        return new LockIPReplyDetail();
    }

    /**
     * Create an instance of {@link UnLockIPResponse }
     * 
     */
    public UnLockIPResponse createUnLockIPResponse() {
        return new UnLockIPResponse();
    }

    /**
     * Create an instance of {@link UnLockIPReplyDetail }
     * 
     */
    public UnLockIPReplyDetail createUnLockIPReplyDetail() {
        return new UnLockIPReplyDetail();
    }

    /**
     * Create an instance of {@link CreateCorpVPNResponse }
     * 
     */
    public CreateCorpVPNResponse createCreateCorpVPNResponse() {
        return new CreateCorpVPNResponse();
    }

    /**
     * Create an instance of {@link AddCorpVPNMemberResponse }
     * 
     */
    public AddCorpVPNMemberResponse createAddCorpVPNMemberResponse() {
        return new AddCorpVPNMemberResponse();
    }

    /**
     * Create an instance of {@link RemoveCorpVPNMemberResponse }
     * 
     */
    public RemoveCorpVPNMemberResponse createRemoveCorpVPNMemberResponse() {
        return new RemoveCorpVPNMemberResponse();
    }

    /**
     * Create an instance of {@link DeleteCorpVPNResponse }
     * 
     */
    public DeleteCorpVPNResponse createDeleteCorpVPNResponse() {
        return new DeleteCorpVPNResponse();
    }

    /**
     * Create an instance of {@link GetUsageInfoResponse }
     * 
     */
    public GetUsageInfoResponse createGetUsageInfoResponse() {
        return new GetUsageInfoResponse();
    }

    /**
     * Create an instance of {@link GetMsisdnByIccidResponse }
     * 
     */
    public GetMsisdnByIccidResponse createGetMsisdnByIccidResponse() {
        return new GetMsisdnByIccidResponse();
    }

    /**
     * Create an instance of {@link QueryMsisdnByIccidResultValue }
     * 
     */
    public QueryMsisdnByIccidResultValue createQueryMsisdnByIccidResultValue() {
        return new QueryMsisdnByIccidResultValue();
    }

    /**
     * Create an instance of {@link ModifyCorpAPNResponse }
     * 
     */
    public ModifyCorpAPNResponse createModifyCorpAPNResponse() {
        return new ModifyCorpAPNResponse();
    }

    /**
     * Create an instance of {@link TransferAccountResponse }
     * 
     */
    public TransferAccountResponse createTransferAccountResponse() {
        return new TransferAccountResponse();
    }

    /**
     * Create an instance of {@link AddFreeUnitsResponse }
     * 
     */
    public AddFreeUnitsResponse createAddFreeUnitsResponse() {
        return new AddFreeUnitsResponse();
    }

    /**
     * Create an instance of {@link GetMultiSimSubInfoResponse }
     * 
     */
    public GetMultiSimSubInfoResponse createGetMultiSimSubInfoResponse() {
        return new GetMultiSimSubInfoResponse();
    }

    /**
     * Create an instance of {@link GetMultiSimSubInfoReplyValue }
     * 
     */
    public GetMultiSimSubInfoReplyValue createGetMultiSimSubInfoReplyValue() {
        return new GetMultiSimSubInfoReplyValue();
    }

    /**
     * Create an instance of {@link ChangeMainAccountResponse }
     * 
     */
    public ChangeMainAccountResponse createChangeMainAccountResponse() {
        return new ChangeMainAccountResponse();
    }

    /**
     * Create an instance of {@link UserUsageAmountDetails }
     * 
     */
    public UserUsageAmountDetails createUserUsageAmountDetails() {
        return new UserUsageAmountDetails();
    }

    /**
     * Create an instance of {@link GetOutstandingIn }
     * 
     */
    public GetOutstandingIn createGetOutstandingIn() {
        return new GetOutstandingIn();
    }

    /**
     * Create an instance of {@link GetOutstandingOut }
     * 
     */
    public GetOutstandingOut createGetOutstandingOut() {
        return new GetOutstandingOut();
    }

    /**
     * Create an instance of {@link GetPaymentHistoryIn }
     * 
     */
    public GetPaymentHistoryIn createGetPaymentHistoryIn() {
        return new GetPaymentHistoryIn();
    }

    /**
     * Create an instance of {@link GetPaymentHistoryOut }
     * 
     */
    public GetPaymentHistoryOut createGetPaymentHistoryOut() {
        return new GetPaymentHistoryOut();
    }

    /**
     * Create an instance of {@link GetReceiptDataIn }
     * 
     */
    public GetReceiptDataIn createGetReceiptDataIn() {
        return new GetReceiptDataIn();
    }

    /**
     * Create an instance of {@link GetReceiptDataOut }
     * 
     */
    public GetReceiptDataOut createGetReceiptDataOut() {
        return new GetReceiptDataOut();
    }

    /**
     * Create an instance of {@link PaymentFromCustomerIn }
     * 
     */
    public PaymentFromCustomerIn createPaymentFromCustomerIn() {
        return new PaymentFromCustomerIn();
    }

    /**
     * Create an instance of {@link PaymentFromCustomerOut }
     * 
     */
    public PaymentFromCustomerOut createPaymentFromCustomerOut() {
        return new PaymentFromCustomerOut();
    }

    /**
     * Create an instance of {@link EAIWSBillDetailValue }
     * 
     */
    public EAIWSBillDetailValue createEAIWSBillDetailValue() {
        return new EAIWSBillDetailValue();
    }

    /**
     * Create an instance of {@link EAIWSPaymentDetailValue }
     * 
     */
    public EAIWSPaymentDetailValue createEAIWSPaymentDetailValue() {
        return new EAIWSPaymentDetailValue();
    }

    /**
     * Create an instance of {@link GetXMLBillIn }
     * 
     */
    public GetXMLBillIn createGetXMLBillIn() {
        return new GetXMLBillIn();
    }

    /**
     * Create an instance of {@link GetXMLBillOut }
     * 
     */
    public GetXMLBillOut createGetXMLBillOut() {
        return new GetXMLBillOut();
    }

    /**
     * Create an instance of {@link GetDunningHistoryOut }
     * 
     */
    public GetDunningHistoryOut createGetDunningHistoryOut() {
        return new GetDunningHistoryOut();
    }

    /**
     * Create an instance of {@link CustomerValue }
     * 
     */
    public CustomerValue createCustomerValue() {
        return new CustomerValue();
    }

    /**
     * Create an instance of {@link SubscriberValue }
     * 
     */
    public SubscriberValue createSubscriberValue() {
        return new SubscriberValue();
    }

    /**
     * Create an instance of {@link AccountValue }
     * 
     */
    public AccountValue createAccountValue() {
        return new AccountValue();
    }

    /**
     * Create an instance of {@link CustomerAddressValue }
     * 
     */
    public CustomerAddressValue createCustomerAddressValue() {
        return new CustomerAddressValue();
    }

    /**
     * Create an instance of {@link CustomerRelationInfo }
     * 
     */
    public CustomerRelationInfo createCustomerRelationInfo() {
        return new CustomerRelationInfo();
    }

    /**
     * Create an instance of {@link PaymentModeValue }
     * 
     */
    public PaymentModeValue createPaymentModeValue() {
        return new PaymentModeValue();
    }

    /**
     * Create an instance of {@link OrderProductValue }
     * 
     */
    public OrderProductValue createOrderProductValue() {
        return new OrderProductValue();
    }

    /**
     * Create an instance of {@link OrderServiceValue }
     * 
     */
    public OrderServiceValue createOrderServiceValue() {
        return new OrderServiceValue();
    }

    /**
     * Create an instance of {@link OrderParameterValue }
     * 
     */
    public OrderParameterValue createOrderParameterValue() {
        return new OrderParameterValue();
    }

    /**
     * Create an instance of {@link BusinessFeeValue }
     * 
     */
    public BusinessFeeValue createBusinessFeeValue() {
        return new BusinessFeeValue();
    }

    /**
     * Create an instance of {@link MNPValue }
     * 
     */
    public MNPValue createMNPValue() {
        return new MNPValue();
    }

    /**
     * Create an instance of {@link CDRValue }
     * 
     */
    public CDRValue createCDRValue() {
        return new CDRValue();
    }

    /**
     * Create an instance of {@link SubscriberParameterValue }
     * 
     */
    public SubscriberParameterValue createSubscriberParameterValue() {
        return new SubscriberParameterValue();
    }

    /**
     * Create an instance of {@link SubscriberServiceValue }
     * 
     */
    public SubscriberServiceValue createSubscriberServiceValue() {
        return new SubscriberServiceValue();
    }

    /**
     * Create an instance of {@link SubscriberProductValue }
     * 
     */
    public SubscriberProductValue createSubscriberProductValue() {
        return new SubscriberProductValue();
    }

    /**
     * Create an instance of {@link ResultOfSubStatusHistoryValue }
     * 
     */
    public ResultOfSubStatusHistoryValue createResultOfSubStatusHistoryValue() {
        return new ResultOfSubStatusHistoryValue();
    }

    /**
     * Create an instance of {@link OrderContractValue }
     * 
     */
    public OrderContractValue createOrderContractValue() {
        return new OrderContractValue();
    }

    /**
     * Create an instance of {@link GoodsSaleHistoryValue }
     * 
     */
    public GoodsSaleHistoryValue createGoodsSaleHistoryValue() {
        return new GoodsSaleHistoryValue();
    }

    /**
     * Create an instance of {@link GoodsSaleValue }
     * 
     */
    public GoodsSaleValue createGoodsSaleValue() {
        return new GoodsSaleValue();
    }

    /**
     * Create an instance of {@link AccountRelationValue }
     * 
     */
    public AccountRelationValue createAccountRelationValue() {
        return new AccountRelationValue();
    }

    /**
     * Create an instance of {@link CUGValue }
     * 
     */
    public CUGValue createCUGValue() {
        return new CUGValue();
    }

    /**
     * Create an instance of {@link CUGMemberValue }
     * 
     */
    public CUGMemberValue createCUGMemberValue() {
        return new CUGMemberValue();
    }

    /**
     * Create an instance of {@link ModifyAutoRefillConfigRequestValue }
     * 
     */
    public ModifyAutoRefillConfigRequestValue createModifyAutoRefillConfigRequestValue() {
        return new ModifyAutoRefillConfigRequestValue();
    }

    /**
     * Create an instance of {@link AutoRefillConfigValue }
     * 
     */
    public AutoRefillConfigValue createAutoRefillConfigValue() {
        return new AutoRefillConfigValue();
    }

    /**
     * Create an instance of {@link GetSimCardsIn }
     * 
     */
    public GetSimCardsIn createGetSimCardsIn() {
        return new GetSimCardsIn();
    }

    /**
     * Create an instance of {@link GetSimCardsOut }
     * 
     */
    public GetSimCardsOut createGetSimCardsOut() {
        return new GetSimCardsOut();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersQuantityIn }
     * 
     */
    public GetPhoneNumbersQuantityIn createGetPhoneNumbersQuantityIn() {
        return new GetPhoneNumbersQuantityIn();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersIn }
     * 
     */
    public GetPhoneNumbersIn createGetPhoneNumbersIn() {
        return new GetPhoneNumbersIn();
    }

    /**
     * Create an instance of {@link GetPhoneNumbersOut }
     * 
     */
    public GetPhoneNumbersOut createGetPhoneNumbersOut() {
        return new GetPhoneNumbersOut();
    }

    /**
     * Create an instance of {@link EAIWSResultOfValidProductValue }
     * 
     */
    public EAIWSResultOfValidProductValue createEAIWSResultOfValidProductValue() {
        return new EAIWSResultOfValidProductValue();
    }

    /**
     * Create an instance of {@link EAIWSServiceDetailValue }
     * 
     */
    public EAIWSServiceDetailValue createEAIWSServiceDetailValue() {
        return new EAIWSServiceDetailValue();
    }

    /**
     * Create an instance of {@link EAIWSServiceParaDetailValue }
     * 
     */
    public EAIWSServiceParaDetailValue createEAIWSServiceParaDetailValue() {
        return new EAIWSServiceParaDetailValue();
    }

    /**
     * Create an instance of {@link EAIServiceParameterEnumerationValue }
     * 
     */
    public EAIServiceParameterEnumerationValue createEAIServiceParameterEnumerationValue() {
        return new EAIServiceParameterEnumerationValue();
    }

    /**
     * Create an instance of {@link EAIWSProductFeeValue }
     * 
     */
    public EAIWSProductFeeValue createEAIWSProductFeeValue() {
        return new EAIWSProductFeeValue();
    }

    /**
     * Create an instance of {@link EAIWSGoodsIntValue }
     * 
     */
    public EAIWSGoodsIntValue createEAIWSGoodsIntValue() {
        return new EAIWSGoodsIntValue();
    }

    /**
     * Create an instance of {@link EAIWSGoodsValue }
     * 
     */
    public EAIWSGoodsValue createEAIWSGoodsValue() {
        return new EAIWSGoodsValue();
    }

    /**
     * Create an instance of {@link EAIWSBusinessChargeAction }
     * 
     */
    public EAIWSBusinessChargeAction createEAIWSBusinessChargeAction() {
        return new EAIWSBusinessChargeAction();
    }

    /**
     * Create an instance of {@link EAIWSContractValue }
     * 
     */
    public EAIWSContractValue createEAIWSContractValue() {
        return new EAIWSContractValue();
    }

    /**
     * Create an instance of {@link EAIWSInstallmentValue }
     * 
     */
    public EAIWSInstallmentValue createEAIWSInstallmentValue() {
        return new EAIWSInstallmentValue();
    }

    /**
     * Create an instance of {@link GetDictTableResult }
     * 
     */
    public GetDictTableResult createGetDictTableResult() {
        return new GetDictTableResult();
    }

    /**
     * Create an instance of {@link DictTableValue }
     * 
     */
    public DictTableValue createDictTableValue() {
        return new DictTableValue();
    }

    /**
     * Create an instance of {@link QuerySubscribeAccessoriesResult }
     * 
     */
    public QuerySubscribeAccessoriesResult createQuerySubscribeAccessoriesResult() {
        return new QuerySubscribeAccessoriesResult();
    }

    /**
     * Create an instance of {@link WSGetResourceValue }
     * 
     */
    public WSGetResourceValue createWSGetResourceValue() {
        return new WSGetResourceValue();
    }

    /**
     * Create an instance of {@link ZGCValue }
     * 
     */
    public ZGCValue createZGCValue() {
        return new ZGCValue();
    }

    /**
     * Create an instance of {@link DCCHistory }
     * 
     */
    public DCCHistory createDCCHistory() {
        return new DCCHistory();
    }

    /**
     * Create an instance of {@link SMSNoticeHistory }
     * 
     */
    public SMSNoticeHistory createSMSNoticeHistory() {
        return new SMSNoticeHistory();
    }

    /**
     * Create an instance of {@link AcctSplitValue }
     * 
     */
    public AcctSplitValue createAcctSplitValue() {
        return new AcctSplitValue();
    }

    /**
     * Create an instance of {@link FnFNumberValue }
     * 
     */
    public FnFNumberValue createFnFNumberValue() {
        return new FnFNumberValue();
    }

    /**
     * Create an instance of {@link GetAvailableIPsIn }
     * 
     */
    public GetAvailableIPsIn createGetAvailableIPsIn() {
        return new GetAvailableIPsIn();
    }

    /**
     * Create an instance of {@link GetAvailableIPsOut }
     * 
     */
    public GetAvailableIPsOut createGetAvailableIPsOut() {
        return new GetAvailableIPsOut();
    }

    /**
     * Create an instance of {@link CorpCustomerValue }
     * 
     */
    public CorpCustomerValue createCorpCustomerValue() {
        return new CorpCustomerValue();
    }

    /**
     * Create an instance of {@link PICInfoValue }
     * 
     */
    public PICInfoValue createPICInfoValue() {
        return new PICInfoValue();
    }

    /**
     * Create an instance of {@link CorpAPNValue }
     * 
     */
    public CorpAPNValue createCorpAPNValue() {
        return new CorpAPNValue();
    }

    /**
     * Create an instance of {@link CorpVPNValue }
     * 
     */
    public CorpVPNValue createCorpVPNValue() {
        return new CorpVPNValue();
    }

    /**
     * Create an instance of {@link CorpAPNMemberValue }
     * 
     */
    public CorpAPNMemberValue createCorpAPNMemberValue() {
        return new CorpAPNMemberValue();
    }

    /**
     * Create an instance of {@link CorpAPNHisMemberValue }
     * 
     */
    public CorpAPNHisMemberValue createCorpAPNHisMemberValue() {
        return new CorpAPNHisMemberValue();
    }

    /**
     * Create an instance of {@link ResultOfSubNumberValue }
     * 
     */
    public ResultOfSubNumberValue createResultOfSubNumberValue() {
        return new ResultOfSubNumberValue();
    }

    /**
     * Create an instance of {@link WSUsageValues.WSUsageValue }
     * 
     */
    public WSUsageValues.WSUsageValue createWSUsageValuesWSUsageValue() {
        return new WSUsageValues.WSUsageValue();
    }

    /**
     * Create an instance of {@link WSBusiInfoValue.PendingBusiInfo }
     * 
     */
    public WSBusiInfoValue.PendingBusiInfo createWSBusiInfoValuePendingBusiInfo() {
        return new WSBusiInfoValue.PendingBusiInfo();
    }

    /**
     * Create an instance of {@link WSUnbilledInfoValue.UnbilledInfoValues }
     * 
     */
    public WSUnbilledInfoValue.UnbilledInfoValues createWSUnbilledInfoValueUnbilledInfoValues() {
        return new WSUnbilledInfoValue.UnbilledInfoValues();
    }

    /**
     * Create an instance of {@link WSPendingBusiInfoValue.PendingBusiInfo }
     * 
     */
    public WSPendingBusiInfoValue.PendingBusiInfo createWSPendingBusiInfoValuePendingBusiInfo() {
        return new WSPendingBusiInfoValue.PendingBusiInfo();
    }

    /**
     * Create an instance of {@link FreeUnitsInfoValues.FreeUnitsInfoValue }
     * 
     */
    public FreeUnitsInfoValues.FreeUnitsInfoValue createFreeUnitsInfoValuesFreeUnitsInfoValue() {
        return new FreeUnitsInfoValues.FreeUnitsInfoValue();
    }

    /**
     * Create an instance of {@link QueryBalanceResultValue.Balances }
     * 
     */
    public QueryBalanceResultValue.Balances createQueryBalanceResultValueBalances() {
        return new QueryBalanceResultValue.Balances();
    }

    /**
     * Create an instance of {@link VoucherCardResultValue.VoucherCardValues }
     * 
     */
    public VoucherCardResultValue.VoucherCardValues createVoucherCardResultValueVoucherCardValues() {
        return new VoucherCardResultValue.VoucherCardValues();
    }

    /**
     * Create an instance of {@link WSConstractSignedInfoValue.ConstractInfos }
     * 
     */
    public WSConstractSignedInfoValue.ConstractInfos createWSConstractSignedInfoValueConstractInfos() {
        return new WSConstractSignedInfoValue.ConstractInfos();
    }

}
