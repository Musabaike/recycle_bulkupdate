
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnLockMSISDNResply" type="{http://oss.huawei.com/webservice/ecare/services}UnLockMSISDNResplyDetail"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "unLockMSISDNResply"
})
@XmlRootElement(name = "unLockMSISDNResponse")
public class UnLockMSISDNResponse {

    @XmlElement(name = "UnLockMSISDNResply", required = true)
    protected UnLockMSISDNResplyDetail unLockMSISDNResply;

    /**
     * Gets the value of the unLockMSISDNResply property.
     * 
     * @return
     *     possible object is
     *     {@link UnLockMSISDNResplyDetail }
     *     
     */
    public UnLockMSISDNResplyDetail getUnLockMSISDNResply() {
        return unLockMSISDNResply;
    }

    /**
     * Sets the value of the unLockMSISDNResply property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnLockMSISDNResplyDetail }
     *     
     */
    public void setUnLockMSISDNResply(UnLockMSISDNResplyDetail value) {
        this.unLockMSISDNResply = value;
    }

}
