
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeMsisdnValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeMsisdnValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriberID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="newMsisdn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="msisdnMethod" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="msisdnPosition" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MNPFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Dno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Rno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="feeInfos" type="{http://oss.huawei.com/webservice/ecare/services}BusinessFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeMsisdnValue", propOrder = {
    "subscriberID",
    "newMsisdn",
    "msisdnMethod",
    "msisdnPosition",
    "mnpFlag",
    "dno",
    "rno",
    "feeInfos"
})
public class ChangeMsisdnValue {

    @XmlElement(required = true)
    protected String subscriberID;
    @XmlElement(required = true)
    protected String newMsisdn;
    @XmlElement(required = true)
    protected String msisdnMethod;
    @XmlElement(required = true)
    protected String msisdnPosition;
    @XmlElement(name = "MNPFlag")
    protected String mnpFlag;
    @XmlElement(name = "Dno")
    protected String dno;
    @XmlElement(name = "Rno")
    protected String rno;
    protected List<BusinessFeeValue> feeInfos;

    /**
     * Gets the value of the subscriberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberID() {
        return subscriberID;
    }

    /**
     * Sets the value of the subscriberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberID(String value) {
        this.subscriberID = value;
    }

    /**
     * Gets the value of the newMsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewMsisdn() {
        return newMsisdn;
    }

    /**
     * Sets the value of the newMsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewMsisdn(String value) {
        this.newMsisdn = value;
    }

    /**
     * Gets the value of the msisdnMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnMethod() {
        return msisdnMethod;
    }

    /**
     * Sets the value of the msisdnMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnMethod(String value) {
        this.msisdnMethod = value;
    }

    /**
     * Gets the value of the msisdnPosition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnPosition() {
        return msisdnPosition;
    }

    /**
     * Sets the value of the msisdnPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnPosition(String value) {
        this.msisdnPosition = value;
    }

    /**
     * Gets the value of the mnpFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMNPFlag() {
        return mnpFlag;
    }

    /**
     * Sets the value of the mnpFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMNPFlag(String value) {
        this.mnpFlag = value;
    }

    /**
     * Gets the value of the dno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDno() {
        return dno;
    }

    /**
     * Sets the value of the dno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDno(String value) {
        this.dno = value;
    }

    /**
     * Gets the value of the rno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRno() {
        return rno;
    }

    /**
     * Sets the value of the rno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRno(String value) {
        this.rno = value;
    }

    /**
     * Gets the value of the feeInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessFeeValue }
     * 
     * 
     */
    public List<BusinessFeeValue> getFeeInfos() {
        if (feeInfos == null) {
            feeInfos = new ArrayList<BusinessFeeValue>();
        }
        return this.feeInfos;
    }

}
