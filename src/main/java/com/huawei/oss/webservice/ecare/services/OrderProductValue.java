
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderProductValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderProductValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fllaFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="zgcId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serviceList" type="{http://oss.huawei.com/webservice/ecare/services}OrderServiceValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderProductValue", propOrder = {
    "productSeq",
    "productId",
    "fllaFlag",
    "zgcId",
    "serviceList"
})
public class OrderProductValue {

    protected String productSeq;
    protected String productId;
    protected String fllaFlag;
    protected String zgcId;
    protected List<OrderServiceValue> serviceList;

    /**
     * Gets the value of the productSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductSeq() {
        return productSeq;
    }

    /**
     * Sets the value of the productSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductSeq(String value) {
        this.productSeq = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the fllaFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFllaFlag() {
        return fllaFlag;
    }

    /**
     * Sets the value of the fllaFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFllaFlag(String value) {
        this.fllaFlag = value;
    }

    /**
     * Gets the value of the zgcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZgcId() {
        return zgcId;
    }

    /**
     * Sets the value of the zgcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZgcId(String value) {
        this.zgcId = value;
    }

    /**
     * Gets the value of the serviceList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderServiceValue }
     * 
     * 
     */
    public List<OrderServiceValue> getServiceList() {
        if (serviceList == null) {
            serviceList = new ArrayList<OrderServiceValue>();
        }
        return this.serviceList;
    }

}
