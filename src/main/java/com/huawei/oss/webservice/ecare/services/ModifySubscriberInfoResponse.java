
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ModifySubscriberInfoReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "modifySubscriberInfoReply"
})
@XmlRootElement(name = "modifySubscriberInfoResponse")
public class ModifySubscriberInfoResponse {

    @XmlElement(name = "ModifySubscriberInfoReply", required = true)
    protected ResultOfOperationValue modifySubscriberInfoReply;

    /**
     * Gets the value of the modifySubscriberInfoReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getModifySubscriberInfoReply() {
        return modifySubscriberInfoReply;
    }

    /**
     * Sets the value of the modifySubscriberInfoReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setModifySubscriberInfoReply(ResultOfOperationValue value) {
        this.modifySubscriberInfoReply = value;
    }

}
