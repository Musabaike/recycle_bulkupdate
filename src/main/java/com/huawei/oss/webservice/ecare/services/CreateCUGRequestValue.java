
package com.huawei.oss.webservice.ecare.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateCUGRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCUGRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="withOwnerFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ownerSubscriberId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="infoCUG" type="{http://oss.huawei.com/webservice/ecare/services}CUGValue" minOccurs="0"/&gt;
 *         &lt;element name="groupRatePlan" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" minOccurs="0"/&gt;
 *         &lt;element name="groupServicePacks" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="validTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="feeInfos" type="{http://oss.huawei.com/webservice/ecare/services}BusinessFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCUGRequestValue", propOrder = {
    "withOwnerFlag",
    "ownerSubscriberId",
    "infoCUG",
    "groupRatePlan",
    "groupServicePacks",
    "validTime",
    "feeInfos"
})
public class CreateCUGRequestValue {

    protected Boolean withOwnerFlag;
    protected BigDecimal ownerSubscriberId;
    protected CUGValue infoCUG;
    protected OrderProductValue groupRatePlan;
    protected List<OrderProductValue> groupServicePacks;
    protected String validTime;
    protected List<BusinessFeeValue> feeInfos;

    /**
     * Gets the value of the withOwnerFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithOwnerFlag() {
        return withOwnerFlag;
    }

    /**
     * Sets the value of the withOwnerFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithOwnerFlag(Boolean value) {
        this.withOwnerFlag = value;
    }

    /**
     * Gets the value of the ownerSubscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOwnerSubscriberId() {
        return ownerSubscriberId;
    }

    /**
     * Sets the value of the ownerSubscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOwnerSubscriberId(BigDecimal value) {
        this.ownerSubscriberId = value;
    }

    /**
     * Gets the value of the infoCUG property.
     * 
     * @return
     *     possible object is
     *     {@link CUGValue }
     *     
     */
    public CUGValue getInfoCUG() {
        return infoCUG;
    }

    /**
     * Sets the value of the infoCUG property.
     * 
     * @param value
     *     allowed object is
     *     {@link CUGValue }
     *     
     */
    public void setInfoCUG(CUGValue value) {
        this.infoCUG = value;
    }

    /**
     * Gets the value of the groupRatePlan property.
     * 
     * @return
     *     possible object is
     *     {@link OrderProductValue }
     *     
     */
    public OrderProductValue getGroupRatePlan() {
        return groupRatePlan;
    }

    /**
     * Sets the value of the groupRatePlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderProductValue }
     *     
     */
    public void setGroupRatePlan(OrderProductValue value) {
        this.groupRatePlan = value;
    }

    /**
     * Gets the value of the groupServicePacks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupServicePacks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupServicePacks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getGroupServicePacks() {
        if (groupServicePacks == null) {
            groupServicePacks = new ArrayList<OrderProductValue>();
        }
        return this.groupServicePacks;
    }

    /**
     * Gets the value of the validTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidTime() {
        return validTime;
    }

    /**
     * Sets the value of the validTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidTime(String value) {
        this.validTime = value;
    }

    /**
     * Gets the value of the feeInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessFeeValue }
     * 
     * 
     */
    public List<BusinessFeeValue> getFeeInfos() {
        if (feeInfos == null) {
            feeInfos = new ArrayList<BusinessFeeValue>();
        }
        return this.feeInfos;
    }

}
