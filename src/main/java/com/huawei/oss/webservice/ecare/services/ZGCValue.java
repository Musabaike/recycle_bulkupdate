
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ZGCValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ZGCValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="zgcId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="zgcName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZGCValue", propOrder = {
    "zgcId",
    "zgcName"
})
public class ZGCValue {

    @XmlElement(required = true)
    protected String zgcId;
    @XmlElement(required = true)
    protected String zgcName;

    /**
     * Gets the value of the zgcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZgcId() {
        return zgcId;
    }

    /**
     * Sets the value of the zgcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZgcId(String value) {
        this.zgcId = value;
    }

    /**
     * Gets the value of the zgcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZgcName() {
        return zgcName;
    }

    /**
     * Sets the value of the zgcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZgcName(String value) {
        this.zgcName = value;
    }

}
