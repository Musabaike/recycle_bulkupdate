package com.yannitech.recycle;

import org.apache.camel.spring.Main;

import com.yannitech.recycle.bean.MyRouteBuilder;

public class RecycleApp {
	
	/**
	 * Allow this route to be run as an application
	 */
	public static void main(String[] args) throws Exception {
	
		Main main = new Main();
        main.setApplicationContextUri("META-INF/spring/camel-context.xml");
        main.addRouteBuilder(new MyRouteBuilder());
        main.run(args);
	}


}
