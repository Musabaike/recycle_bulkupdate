
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AccessSessionRequest" type="{http://oss.huawei.com/webservice/ecare/services}AccessSessionValue"/&gt;
 *         &lt;element name="ChangeHybridToPrepaidRequest" type="{http://oss.huawei.com/webservice/ecare/services}HybridToPrepaidValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accessSessionRequest",
    "changeHybridToPrepaidRequest"
})
@XmlRootElement(name = "changeHybridToPrepaid")
public class ChangeHybridToPrepaid {

    @XmlElement(name = "AccessSessionRequest", required = true)
    protected AccessSessionValue accessSessionRequest;
    @XmlElement(name = "ChangeHybridToPrepaidRequest", required = true)
    protected HybridToPrepaidValue changeHybridToPrepaidRequest;

    /**
     * Gets the value of the accessSessionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AccessSessionValue }
     *     
     */
    public AccessSessionValue getAccessSessionRequest() {
        return accessSessionRequest;
    }

    /**
     * Sets the value of the accessSessionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessSessionValue }
     *     
     */
    public void setAccessSessionRequest(AccessSessionValue value) {
        this.accessSessionRequest = value;
    }

    /**
     * Gets the value of the changeHybridToPrepaidRequest property.
     * 
     * @return
     *     possible object is
     *     {@link HybridToPrepaidValue }
     *     
     */
    public HybridToPrepaidValue getChangeHybridToPrepaidRequest() {
        return changeHybridToPrepaidRequest;
    }

    /**
     * Sets the value of the changeHybridToPrepaidRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link HybridToPrepaidValue }
     *     
     */
    public void setChangeHybridToPrepaidRequest(HybridToPrepaidValue value) {
        this.changeHybridToPrepaidRequest = value;
    }

}
