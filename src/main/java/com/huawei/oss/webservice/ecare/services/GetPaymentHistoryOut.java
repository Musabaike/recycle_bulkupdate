
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPaymentHistoryOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPaymentHistoryOut"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arTransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="paymentTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="paymentDetail" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSPaymentDetailValue" minOccurs="0"/&gt;
 *         &lt;element name="paymentChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPaymentHistoryOut", propOrder = {
    "arTransactionID",
    "paymentTime",
    "paymentDetail",
    "paymentChannel"
})
public class GetPaymentHistoryOut {

    protected String arTransactionID;
    protected String paymentTime;
    protected EAIWSPaymentDetailValue paymentDetail;
    protected String paymentChannel;

    /**
     * Gets the value of the arTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArTransactionID() {
        return arTransactionID;
    }

    /**
     * Sets the value of the arTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArTransactionID(String value) {
        this.arTransactionID = value;
    }

    /**
     * Gets the value of the paymentTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTime() {
        return paymentTime;
    }

    /**
     * Sets the value of the paymentTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTime(String value) {
        this.paymentTime = value;
    }

    /**
     * Gets the value of the paymentDetail property.
     * 
     * @return
     *     possible object is
     *     {@link EAIWSPaymentDetailValue }
     *     
     */
    public EAIWSPaymentDetailValue getPaymentDetail() {
        return paymentDetail;
    }

    /**
     * Sets the value of the paymentDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAIWSPaymentDetailValue }
     *     
     */
    public void setPaymentDetail(EAIWSPaymentDetailValue value) {
        this.paymentDetail = value;
    }

    /**
     * Gets the value of the paymentChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentChannel() {
        return paymentChannel;
    }

    /**
     * Sets the value of the paymentChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentChannel(String value) {
        this.paymentChannel = value;
    }

}
