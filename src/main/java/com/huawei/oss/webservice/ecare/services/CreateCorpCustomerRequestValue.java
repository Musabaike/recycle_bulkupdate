
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateCorpCustomerRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCorpCustomerRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="corpCustomerInfo" type="{http://oss.huawei.com/webservice/ecare/services}CorpCustomerValue" minOccurs="0"/&gt;
 *         &lt;element name="accountInfo" type="{http://oss.huawei.com/webservice/ecare/services}AccountValue" minOccurs="0"/&gt;
 *         &lt;element name="PICInfo" type="{http://oss.huawei.com/webservice/ecare/services}PICInfoValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCorpCustomerRequestValue", propOrder = {
    "corpCustomerInfo",
    "accountInfo",
    "picInfo"
})
public class CreateCorpCustomerRequestValue {

    protected CorpCustomerValue corpCustomerInfo;
    protected AccountValue accountInfo;
    @XmlElement(name = "PICInfo")
    protected PICInfoValue picInfo;

    /**
     * Gets the value of the corpCustomerInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CorpCustomerValue }
     *     
     */
    public CorpCustomerValue getCorpCustomerInfo() {
        return corpCustomerInfo;
    }

    /**
     * Sets the value of the corpCustomerInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorpCustomerValue }
     *     
     */
    public void setCorpCustomerInfo(CorpCustomerValue value) {
        this.corpCustomerInfo = value;
    }

    /**
     * Gets the value of the accountInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountValue }
     *     
     */
    public AccountValue getAccountInfo() {
        return accountInfo;
    }

    /**
     * Sets the value of the accountInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountValue }
     *     
     */
    public void setAccountInfo(AccountValue value) {
        this.accountInfo = value;
    }

    /**
     * Gets the value of the picInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PICInfoValue }
     *     
     */
    public PICInfoValue getPICInfo() {
        return picInfo;
    }

    /**
     * Sets the value of the picInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PICInfoValue }
     *     
     */
    public void setPICInfo(PICInfoValue value) {
        this.picInfo = value;
    }

}
