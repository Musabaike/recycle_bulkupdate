
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateAdditionalNumberRequest" type="{http://oss.huawei.com/webservice/ecare/services}UpdateAdditionalNumberRequestValue"/&gt;
 *         &lt;element name="AccessSessionRequest" type="{http://oss.huawei.com/webservice/ecare/services}AccessSessionValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateAdditionalNumberRequest",
    "accessSessionRequest"
})
@XmlRootElement(name = "updateAdditionalNumber")
public class UpdateAdditionalNumber {

    @XmlElement(name = "UpdateAdditionalNumberRequest", required = true)
    protected UpdateAdditionalNumberRequestValue updateAdditionalNumberRequest;
    @XmlElement(name = "AccessSessionRequest", required = true)
    protected AccessSessionValue accessSessionRequest;

    /**
     * Gets the value of the updateAdditionalNumberRequest property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateAdditionalNumberRequestValue }
     *     
     */
    public UpdateAdditionalNumberRequestValue getUpdateAdditionalNumberRequest() {
        return updateAdditionalNumberRequest;
    }

    /**
     * Sets the value of the updateAdditionalNumberRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateAdditionalNumberRequestValue }
     *     
     */
    public void setUpdateAdditionalNumberRequest(UpdateAdditionalNumberRequestValue value) {
        this.updateAdditionalNumberRequest = value;
    }

    /**
     * Gets the value of the accessSessionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AccessSessionValue }
     *     
     */
    public AccessSessionValue getAccessSessionRequest() {
        return accessSessionRequest;
    }

    /**
     * Sets the value of the accessSessionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessSessionValue }
     *     
     */
    public void setAccessSessionRequest(AccessSessionValue value) {
        this.accessSessionRequest = value;
    }

}
