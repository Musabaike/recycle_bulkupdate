
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderZoneRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderZoneRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sMSISDN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nZoneIndex" type="{http://www.w3.org/2001/XMLSchema}short"/&gt;
 *         &lt;element name="nXCoordinate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nYCoordinate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderZoneRequestValue", propOrder = {
    "smsisdn",
    "productId",
    "nZoneIndex",
    "nxCoordinate",
    "nyCoordinate"
})
public class OrderZoneRequestValue {

    @XmlElement(name = "sMSISDN")
    protected String smsisdn;
    protected String productId;
    protected short nZoneIndex;
    @XmlElement(name = "nXCoordinate")
    protected String nxCoordinate;
    @XmlElement(name = "nYCoordinate")
    protected String nyCoordinate;

    /**
     * Gets the value of the smsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMSISDN() {
        return smsisdn;
    }

    /**
     * Sets the value of the smsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMSISDN(String value) {
        this.smsisdn = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the nZoneIndex property.
     * 
     */
    public short getNZoneIndex() {
        return nZoneIndex;
    }

    /**
     * Sets the value of the nZoneIndex property.
     * 
     */
    public void setNZoneIndex(short value) {
        this.nZoneIndex = value;
    }

    /**
     * Gets the value of the nxCoordinate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNXCoordinate() {
        return nxCoordinate;
    }

    /**
     * Sets the value of the nxCoordinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNXCoordinate(String value) {
        this.nxCoordinate = value;
    }

    /**
     * Gets the value of the nyCoordinate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNYCoordinate() {
        return nyCoordinate;
    }

    /**
     * Sets the value of the nyCoordinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNYCoordinate(String value) {
        this.nyCoordinate = value;
    }

}
