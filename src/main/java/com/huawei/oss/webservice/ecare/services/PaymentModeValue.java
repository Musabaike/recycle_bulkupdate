
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentModeValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentModeValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="paymentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="paymentMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creditCardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bankCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bankAcctno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bankIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creditcardeffdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creditcardexpdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="autoRefillConfig" type="{http://oss.huawei.com/webservice/ecare/services}AutoRefillConfigValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentModeValue", propOrder = {
    "paymentId",
    "paymentMode",
    "creditCardType",
    "bankCode",
    "bankAcctno",
    "bankIssuer",
    "creditcardeffdate",
    "creditcardexpdate",
    "autoRefillConfig"
})
public class PaymentModeValue {

    protected String paymentId;
    protected String paymentMode;
    protected String creditCardType;
    protected String bankCode;
    protected String bankAcctno;
    protected String bankIssuer;
    protected String creditcardeffdate;
    protected String creditcardexpdate;
    protected AutoRefillConfigValue autoRefillConfig;

    /**
     * Gets the value of the paymentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * Sets the value of the paymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentId(String value) {
        this.paymentId = value;
    }

    /**
     * Gets the value of the paymentMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Sets the value of the paymentMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMode(String value) {
        this.paymentMode = value;
    }

    /**
     * Gets the value of the creditCardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardType() {
        return creditCardType;
    }

    /**
     * Sets the value of the creditCardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardType(String value) {
        this.creditCardType = value;
    }

    /**
     * Gets the value of the bankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Sets the value of the bankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCode(String value) {
        this.bankCode = value;
    }

    /**
     * Gets the value of the bankAcctno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAcctno() {
        return bankAcctno;
    }

    /**
     * Sets the value of the bankAcctno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAcctno(String value) {
        this.bankAcctno = value;
    }

    /**
     * Gets the value of the bankIssuer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankIssuer() {
        return bankIssuer;
    }

    /**
     * Sets the value of the bankIssuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankIssuer(String value) {
        this.bankIssuer = value;
    }

    /**
     * Gets the value of the creditcardeffdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditcardeffdate() {
        return creditcardeffdate;
    }

    /**
     * Sets the value of the creditcardeffdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditcardeffdate(String value) {
        this.creditcardeffdate = value;
    }

    /**
     * Gets the value of the creditcardexpdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditcardexpdate() {
        return creditcardexpdate;
    }

    /**
     * Sets the value of the creditcardexpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditcardexpdate(String value) {
        this.creditcardexpdate = value;
    }

    /**
     * Gets the value of the autoRefillConfig property.
     * 
     * @return
     *     possible object is
     *     {@link AutoRefillConfigValue }
     *     
     */
    public AutoRefillConfigValue getAutoRefillConfig() {
        return autoRefillConfig;
    }

    /**
     * Sets the value of the autoRefillConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoRefillConfigValue }
     *     
     */
    public void setAutoRefillConfig(AutoRefillConfigValue value) {
        this.autoRefillConfig = value;
    }

}
