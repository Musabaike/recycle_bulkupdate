
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PaymentFromCustomerReply" type="{http://oss.huawei.com/webservice/ecare/services}PaymentFromCustomerReplyDetails"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paymentFromCustomerReply"
})
@XmlRootElement(name = "paymentFromCustomerResponse")
public class PaymentFromCustomerResponse {

    @XmlElement(name = "PaymentFromCustomerReply", required = true)
    protected PaymentFromCustomerReplyDetails paymentFromCustomerReply;

    /**
     * Gets the value of the paymentFromCustomerReply property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentFromCustomerReplyDetails }
     *     
     */
    public PaymentFromCustomerReplyDetails getPaymentFromCustomerReply() {
        return paymentFromCustomerReply;
    }

    /**
     * Sets the value of the paymentFromCustomerReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentFromCustomerReplyDetails }
     *     
     */
    public void setPaymentFromCustomerReply(PaymentFromCustomerReplyDetails value) {
        this.paymentFromCustomerReply = value;
    }

}
