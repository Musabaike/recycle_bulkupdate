
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeactivateSubscriberValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeactivateSubscriberValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriberId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdnMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdnPositioin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="iccidMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="iccidPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="remark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="penaltyFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="feeInfos" type="{http://oss.huawei.com/webservice/ecare/services}BusinessFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="activationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeactivateSubscriberValue", propOrder = {
    "subscriberId",
    "mdnMethod",
    "mdnPositioin",
    "iccidMethod",
    "iccidPosition",
    "reason",
    "remark",
    "penaltyFlag",
    "feeInfos",
    "activationDate"
})
public class DeactivateSubscriberValue {

    protected String subscriberId;
    protected String mdnMethod;
    protected String mdnPositioin;
    protected String iccidMethod;
    protected String iccidPosition;
    protected String reason;
    protected String remark;
    protected String penaltyFlag;
    protected List<BusinessFeeValue> feeInfos;
    protected String activationDate;

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberId(String value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the mdnMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdnMethod() {
        return mdnMethod;
    }

    /**
     * Sets the value of the mdnMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdnMethod(String value) {
        this.mdnMethod = value;
    }

    /**
     * Gets the value of the mdnPositioin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdnPositioin() {
        return mdnPositioin;
    }

    /**
     * Sets the value of the mdnPositioin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdnPositioin(String value) {
        this.mdnPositioin = value;
    }

    /**
     * Gets the value of the iccidMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIccidMethod() {
        return iccidMethod;
    }

    /**
     * Sets the value of the iccidMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIccidMethod(String value) {
        this.iccidMethod = value;
    }

    /**
     * Gets the value of the iccidPosition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIccidPosition() {
        return iccidPosition;
    }

    /**
     * Sets the value of the iccidPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIccidPosition(String value) {
        this.iccidPosition = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the penaltyFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPenaltyFlag() {
        return penaltyFlag;
    }

    /**
     * Sets the value of the penaltyFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPenaltyFlag(String value) {
        this.penaltyFlag = value;
    }

    /**
     * Gets the value of the feeInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessFeeValue }
     * 
     * 
     */
    public List<BusinessFeeValue> getFeeInfos() {
        if (feeInfos == null) {
            feeInfos = new ArrayList<BusinessFeeValue>();
        }
        return this.feeInfos;
    }

    /**
     * Gets the value of the activationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationDate() {
        return activationDate;
    }

    /**
     * Sets the value of the activationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationDate(String value) {
        this.activationDate = value;
    }

}
