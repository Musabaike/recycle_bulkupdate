
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HybridToPrepaidValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HybridToPrepaidValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriberId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="subscriberInfo" type="{http://oss.huawei.com/webservice/ecare/services}SubscriberValue"/&gt;
 *         &lt;element name="rateplanInfo" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue"/&gt;
 *         &lt;element name="servicePackageInfosRemoved" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="servicePackageInfosAdded" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="activationDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="feeInfos" type="{http://oss.huawei.com/webservice/ecare/services}BusinessFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="contractInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderContractValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HybridToPrepaidValue", propOrder = {
    "subscriberId",
    "subscriberInfo",
    "rateplanInfo",
    "servicePackageInfosRemoved",
    "servicePackageInfosAdded",
    "activationDate",
    "feeInfos",
    "contractInfos"
})
public class HybridToPrepaidValue {

    @XmlElement(required = true)
    protected String subscriberId;
    @XmlElement(required = true)
    protected SubscriberValue subscriberInfo;
    @XmlElement(required = true)
    protected OrderProductValue rateplanInfo;
    protected List<OrderProductValue> servicePackageInfosRemoved;
    protected List<OrderProductValue> servicePackageInfosAdded;
    @XmlElement(required = true)
    protected String activationDate;
    protected List<BusinessFeeValue> feeInfos;
    protected List<OrderContractValue> contractInfos;

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberId(String value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the subscriberInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberValue }
     *     
     */
    public SubscriberValue getSubscriberInfo() {
        return subscriberInfo;
    }

    /**
     * Sets the value of the subscriberInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberValue }
     *     
     */
    public void setSubscriberInfo(SubscriberValue value) {
        this.subscriberInfo = value;
    }

    /**
     * Gets the value of the rateplanInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderProductValue }
     *     
     */
    public OrderProductValue getRateplanInfo() {
        return rateplanInfo;
    }

    /**
     * Sets the value of the rateplanInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderProductValue }
     *     
     */
    public void setRateplanInfo(OrderProductValue value) {
        this.rateplanInfo = value;
    }

    /**
     * Gets the value of the servicePackageInfosRemoved property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicePackageInfosRemoved property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePackageInfosRemoved().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getServicePackageInfosRemoved() {
        if (servicePackageInfosRemoved == null) {
            servicePackageInfosRemoved = new ArrayList<OrderProductValue>();
        }
        return this.servicePackageInfosRemoved;
    }

    /**
     * Gets the value of the servicePackageInfosAdded property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicePackageInfosAdded property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePackageInfosAdded().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getServicePackageInfosAdded() {
        if (servicePackageInfosAdded == null) {
            servicePackageInfosAdded = new ArrayList<OrderProductValue>();
        }
        return this.servicePackageInfosAdded;
    }

    /**
     * Gets the value of the activationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationDate() {
        return activationDate;
    }

    /**
     * Sets the value of the activationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationDate(String value) {
        this.activationDate = value;
    }

    /**
     * Gets the value of the feeInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessFeeValue }
     * 
     * 
     */
    public List<BusinessFeeValue> getFeeInfos() {
        if (feeInfos == null) {
            feeInfos = new ArrayList<BusinessFeeValue>();
        }
        return this.feeInfos;
    }

    /**
     * Gets the value of the contractInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderContractValue }
     * 
     * 
     */
    public List<OrderContractValue> getContractInfos() {
        if (contractInfos == null) {
            contractInfos = new ArrayList<OrderContractValue>();
        }
        return this.contractInfos;
    }

}
