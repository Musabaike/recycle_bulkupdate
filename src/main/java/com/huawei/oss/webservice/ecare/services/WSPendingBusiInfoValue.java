
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSPendingBusiInfoValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSPendingBusiInfoValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pendingBusiInfo" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="accountID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="subscriberID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="busiSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="orderNO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="busiType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="busiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="iccid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="imsi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="createDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="toFinishDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="operCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="deptCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSPendingBusiInfoValue", propOrder = {
    "pendingBusiInfo"
})
public class WSPendingBusiInfoValue {

    protected List<WSPendingBusiInfoValue.PendingBusiInfo> pendingBusiInfo;

    /**
     * Gets the value of the pendingBusiInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pendingBusiInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPendingBusiInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSPendingBusiInfoValue.PendingBusiInfo }
     * 
     * 
     */
    public List<WSPendingBusiInfoValue.PendingBusiInfo> getPendingBusiInfo() {
        if (pendingBusiInfo == null) {
            pendingBusiInfo = new ArrayList<WSPendingBusiInfoValue.PendingBusiInfo>();
        }
        return this.pendingBusiInfo;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="accountID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="subscriberID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="busiSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="orderNO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="busiType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="busiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="iccid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="imsi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="createDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="toFinishDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="operCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="deptCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customerID",
        "accountID",
        "subscriberID",
        "busiSequence",
        "orderNO",
        "busiType",
        "busiStatus",
        "msisdn",
        "iccid",
        "imsi",
        "createDate",
        "toFinishDate",
        "operCode",
        "deptCode"
    })
    public static class PendingBusiInfo {

        @XmlElement(required = true)
        protected String customerID;
        @XmlElement(required = true)
        protected String accountID;
        @XmlElement(required = true)
        protected String subscriberID;
        @XmlElement(required = true)
        protected String busiSequence;
        @XmlElement(required = true)
        protected String orderNO;
        @XmlElement(required = true)
        protected String busiType;
        @XmlElement(required = true)
        protected String busiStatus;
        @XmlElement(required = true)
        protected String msisdn;
        @XmlElement(required = true)
        protected String iccid;
        @XmlElement(required = true)
        protected String imsi;
        @XmlElement(required = true)
        protected String createDate;
        @XmlElement(required = true)
        protected String toFinishDate;
        @XmlElement(required = true)
        protected String operCode;
        @XmlElement(required = true)
        protected String deptCode;

        /**
         * Gets the value of the customerID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerID() {
            return customerID;
        }

        /**
         * Sets the value of the customerID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerID(String value) {
            this.customerID = value;
        }

        /**
         * Gets the value of the accountID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountID() {
            return accountID;
        }

        /**
         * Sets the value of the accountID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountID(String value) {
            this.accountID = value;
        }

        /**
         * Gets the value of the subscriberID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubscriberID() {
            return subscriberID;
        }

        /**
         * Sets the value of the subscriberID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubscriberID(String value) {
            this.subscriberID = value;
        }

        /**
         * Gets the value of the busiSequence property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusiSequence() {
            return busiSequence;
        }

        /**
         * Sets the value of the busiSequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusiSequence(String value) {
            this.busiSequence = value;
        }

        /**
         * Gets the value of the orderNO property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrderNO() {
            return orderNO;
        }

        /**
         * Sets the value of the orderNO property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrderNO(String value) {
            this.orderNO = value;
        }

        /**
         * Gets the value of the busiType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusiType() {
            return busiType;
        }

        /**
         * Sets the value of the busiType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusiType(String value) {
            this.busiType = value;
        }

        /**
         * Gets the value of the busiStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusiStatus() {
            return busiStatus;
        }

        /**
         * Sets the value of the busiStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusiStatus(String value) {
            this.busiStatus = value;
        }

        /**
         * Gets the value of the msisdn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMsisdn() {
            return msisdn;
        }

        /**
         * Sets the value of the msisdn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMsisdn(String value) {
            this.msisdn = value;
        }

        /**
         * Gets the value of the iccid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIccid() {
            return iccid;
        }

        /**
         * Sets the value of the iccid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIccid(String value) {
            this.iccid = value;
        }

        /**
         * Gets the value of the imsi property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getImsi() {
            return imsi;
        }

        /**
         * Sets the value of the imsi property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setImsi(String value) {
            this.imsi = value;
        }

        /**
         * Gets the value of the createDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreateDate() {
            return createDate;
        }

        /**
         * Sets the value of the createDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreateDate(String value) {
            this.createDate = value;
        }

        /**
         * Gets the value of the toFinishDate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToFinishDate() {
            return toFinishDate;
        }

        /**
         * Sets the value of the toFinishDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToFinishDate(String value) {
            this.toFinishDate = value;
        }

        /**
         * Gets the value of the operCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperCode() {
            return operCode;
        }

        /**
         * Sets the value of the operCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperCode(String value) {
            this.operCode = value;
        }

        /**
         * Gets the value of the deptCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeptCode() {
            return deptCode;
        }

        /**
         * Sets the value of the deptCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeptCode(String value) {
            this.deptCode = value;
        }

    }

}
