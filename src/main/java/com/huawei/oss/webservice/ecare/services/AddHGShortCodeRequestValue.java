
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddHGShortCodeRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddHGShortCodeRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sGroupID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sMSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sShortCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddHGShortCodeRequestValue", propOrder = {
    "sGroupID",
    "smsisdn",
    "sShortCode"
})
public class AddHGShortCodeRequestValue {

    @XmlElement(required = true)
    protected String sGroupID;
    @XmlElement(name = "sMSISDN", required = true)
    protected String smsisdn;
    @XmlElement(required = true)
    protected String sShortCode;

    /**
     * Gets the value of the sGroupID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSGroupID() {
        return sGroupID;
    }

    /**
     * Sets the value of the sGroupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSGroupID(String value) {
        this.sGroupID = value;
    }

    /**
     * Gets the value of the smsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMSISDN() {
        return smsisdn;
    }

    /**
     * Sets the value of the smsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMSISDN(String value) {
        this.smsisdn = value;
    }

    /**
     * Gets the value of the sShortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSShortCode() {
        return sShortCode;
    }

    /**
     * Sets the value of the sShortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSShortCode(String value) {
        this.sShortCode = value;
    }

}
