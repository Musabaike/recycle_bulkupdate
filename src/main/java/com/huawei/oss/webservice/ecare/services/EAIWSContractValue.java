
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EAIWSContractValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EAIWSContractValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contractID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="cotractName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="contractDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EAIWSContractValue", propOrder = {
    "contractID",
    "cotractName",
    "contractDescription"
})
public class EAIWSContractValue {

    @XmlElement(required = true)
    protected String contractID;
    @XmlElement(required = true)
    protected String cotractName;
    protected String contractDescription;

    /**
     * Gets the value of the contractID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractID() {
        return contractID;
    }

    /**
     * Sets the value of the contractID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractID(String value) {
        this.contractID = value;
    }

    /**
     * Gets the value of the cotractName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCotractName() {
        return cotractName;
    }

    /**
     * Sets the value of the cotractName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotractName(String value) {
        this.cotractName = value;
    }

    /**
     * Gets the value of the contractDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractDescription() {
        return contractDescription;
    }

    /**
     * Sets the value of the contractDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractDescription(String value) {
        this.contractDescription = value;
    }

}
