
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModifyCorpAPNRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModifyCorpAPNRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="groupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="corpAPNInfo" type="{http://oss.huawei.com/webservice/ecare/services}CorpAPNValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModifyCorpAPNRequestValue", propOrder = {
    "groupId",
    "corpAPNInfo"
})
public class ModifyCorpAPNRequestValue {

    protected String groupId;
    protected CorpAPNValue corpAPNInfo;

    /**
     * Gets the value of the groupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the corpAPNInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CorpAPNValue }
     *     
     */
    public CorpAPNValue getCorpAPNInfo() {
        return corpAPNInfo;
    }

    /**
     * Sets the value of the corpAPNInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorpAPNValue }
     *     
     */
    public void setCorpAPNInfo(CorpAPNValue value) {
        this.corpAPNInfo = value;
    }

}
