
package com.huawei.oss.webservice.ecare.services;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDunningHistoryIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDunningHistoryIn"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ActionExecuteDateFrom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ActionExecuteDateTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ActionFlag" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="ActionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActionExecuteStatus" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDunningHistoryIn", propOrder = {
    "subId",
    "actionExecuteDateFrom",
    "actionExecuteDateTo",
    "actionFlag",
    "actionName",
    "actionExecuteStatus"
})
public class GetDunningHistoryIn {

    @XmlElement(required = true)
    protected String subId;
    @XmlElement(name = "ActionExecuteDateFrom", required = true)
    protected String actionExecuteDateFrom;
    @XmlElement(name = "ActionExecuteDateTo", required = true)
    protected String actionExecuteDateTo;
    @XmlElement(name = "ActionFlag", required = true)
    protected BigInteger actionFlag;
    @XmlElement(name = "ActionName")
    protected String actionName;
    @XmlElement(name = "ActionExecuteStatus")
    protected BigInteger actionExecuteStatus;

    /**
     * Gets the value of the subId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubId() {
        return subId;
    }

    /**
     * Sets the value of the subId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubId(String value) {
        this.subId = value;
    }

    /**
     * Gets the value of the actionExecuteDateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionExecuteDateFrom() {
        return actionExecuteDateFrom;
    }

    /**
     * Sets the value of the actionExecuteDateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionExecuteDateFrom(String value) {
        this.actionExecuteDateFrom = value;
    }

    /**
     * Gets the value of the actionExecuteDateTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionExecuteDateTo() {
        return actionExecuteDateTo;
    }

    /**
     * Sets the value of the actionExecuteDateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionExecuteDateTo(String value) {
        this.actionExecuteDateTo = value;
    }

    /**
     * Gets the value of the actionFlag property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getActionFlag() {
        return actionFlag;
    }

    /**
     * Sets the value of the actionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setActionFlag(BigInteger value) {
        this.actionFlag = value;
    }

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    /**
     * Gets the value of the actionExecuteStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getActionExecuteStatus() {
        return actionExecuteStatus;
    }

    /**
     * Sets the value of the actionExecuteStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setActionExecuteStatus(BigInteger value) {
        this.actionExecuteStatus = value;
    }

}
