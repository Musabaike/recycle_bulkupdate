/**
 * 
 */
package com.yannitech.recycle;

import java.math.BigDecimal;
import java.net.URL;

import javax.xml.ws.Holder;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

import com.huawei.oss.webservice.ecare.services.AccessSessionValue;
import com.huawei.oss.webservice.ecare.services.Boss4ECareInterfaceService;
import com.huawei.oss.webservice.ecare.services.Boss4ECareInterfaceServicePortType;
import com.huawei.oss.webservice.ecare.services.QuerySimMsisdnResRequestValue;
import com.huawei.oss.webservice.ecare.services.QuerySimMsisdnResResultValue;
import com.huawei.oss.webservice.ecare.services.ResultOfOperationValue;
import com.yannitech.recycle.util.RecycleUtils;

/**
 * @author bigsim
 *
 */
public class ConnectionTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ConnectionTest ct = new ConnectionTest();
		Holder<QuerySimMsisdnResResultValue> response = ct.getSimMsisdnResourceReplyStatus("0676621680", "0");
		String dept = response.value.getDepartName();
		BigDecimal status = response.value.getResStatus();
		String mnpStatus = response.value.getMnpStatus();
		
		System.out.println("Department --> " + dept + " | Status --> " + status + " | MNPStatus --> " + mnpStatus + "  <----" );

	}


	
	private Holder<QuerySimMsisdnResResultValue> getSimMsisdnResourceReplyStatus(String msisdn, String resType)   {	

        QuerySimMsisdnResRequestValue request = new QuerySimMsisdnResRequestValue();
    	request.setResCode(msisdn);
		request.setResType(resType);
        Holder<ResultOfOperationValue> resultOfOperationReply = new Holder<ResultOfOperationValue>();
        Holder<QuerySimMsisdnResResultValue> simMsisdnResourceReply = new Holder<QuerySimMsisdnResResultValue>();
        
        Boss4ECareInterfaceServicePortType port = getPort();
        addInInterceptors(port);
        
        port.getSimMsisdnResource(getAccessSessionRequest(), request, resultOfOperationReply, simMsisdnResourceReply);
       
        return simMsisdnResourceReply;

    }	
	
	private Boss4ECareInterfaceServicePortType getPort() {
		
		URL wsdlURL = Boss4ECareInterfaceService.WSDL_LOCATION;
		Boss4ECareInterfaceService ss = new Boss4ECareInterfaceService(wsdlURL, RecycleUtils.SERVICE_NAME);
        Boss4ECareInterfaceServicePortType port = ss.getBoss4ECareInterfaceServiceSOAP11PortHttp();
		return port;
	}	
	
	private AccessSessionValue getAccessSessionRequest() {
		   
		AccessSessionValue accessSessionRequest = new AccessSessionValue();
        accessSessionRequest.setAccessChannel(RecycleUtils.ACCESSCHANNEL);
        accessSessionRequest.setOperatorCode(RecycleUtils.OPERATORCODE);
        accessSessionRequest.setPassword(RecycleUtils.PSSD);
        
        return accessSessionRequest;
	}
	
	private void addInInterceptors(Boss4ECareInterfaceServicePortType port) {
		
		Client client = ClientProxy.getClient(port);
	    client.getInInterceptors().add(new LoggingInInterceptor());	   
	    client.getOutInterceptors().add(new LoggingOutInterceptor());
	}
}
