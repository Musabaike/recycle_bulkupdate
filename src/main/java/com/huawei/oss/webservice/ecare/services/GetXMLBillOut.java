
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetXMLBillOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetXMLBillOut"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="billMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="billAttribute" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cmdSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="billContent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetXMLBillOut", propOrder = {
    "accountID",
    "billMonth",
    "billAttribute",
    "cmdSeq",
    "billContent"
})
public class GetXMLBillOut {

    protected String accountID;
    protected String billMonth;
    protected String billAttribute;
    protected String cmdSeq;
    protected String billContent;

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountID(String value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the billMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillMonth() {
        return billMonth;
    }

    /**
     * Sets the value of the billMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillMonth(String value) {
        this.billMonth = value;
    }

    /**
     * Gets the value of the billAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillAttribute() {
        return billAttribute;
    }

    /**
     * Sets the value of the billAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillAttribute(String value) {
        this.billAttribute = value;
    }

    /**
     * Gets the value of the cmdSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmdSeq() {
        return cmdSeq;
    }

    /**
     * Sets the value of the cmdSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmdSeq(String value) {
        this.cmdSeq = value;
    }

    /**
     * Gets the value of the billContent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillContent() {
        return billContent;
    }

    /**
     * Sets the value of the billContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillContent(String value) {
        this.billContent = value;
    }

}
