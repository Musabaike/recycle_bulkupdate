
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LockMSISDNResply" type="{http://oss.huawei.com/webservice/ecare/services}LockMSISDNResplyDetail"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lockMSISDNResply"
})
@XmlRootElement(name = "lockMSISDNResponse")
public class LockMSISDNResponse {

    @XmlElement(name = "LockMSISDNResply", required = true)
    protected LockMSISDNResplyDetail lockMSISDNResply;

    /**
     * Gets the value of the lockMSISDNResply property.
     * 
     * @return
     *     possible object is
     *     {@link LockMSISDNResplyDetail }
     *     
     */
    public LockMSISDNResplyDetail getLockMSISDNResply() {
        return lockMSISDNResply;
    }

    /**
     * Sets the value of the lockMSISDNResply property.
     * 
     * @param value
     *     allowed object is
     *     {@link LockMSISDNResplyDetail }
     *     
     */
    public void setLockMSISDNResply(LockMSISDNResplyDetail value) {
        this.lockMSISDNResply = value;
    }

}
