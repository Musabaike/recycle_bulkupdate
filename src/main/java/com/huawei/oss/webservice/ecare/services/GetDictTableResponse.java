
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResultOfOperationReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *         &lt;element name="GetDictTableReply" type="{http://oss.huawei.com/webservice/ecare/services}GetDictTableResultList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultOfOperationReply",
    "getDictTableReply"
})
@XmlRootElement(name = "getDictTableResponse")
public class GetDictTableResponse {

    @XmlElement(name = "ResultOfOperationReply", required = true)
    protected ResultOfOperationValue resultOfOperationReply;
    @XmlElement(name = "GetDictTableReply", required = true)
    protected GetDictTableResultList getDictTableReply;

    /**
     * Gets the value of the resultOfOperationReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getResultOfOperationReply() {
        return resultOfOperationReply;
    }

    /**
     * Sets the value of the resultOfOperationReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setResultOfOperationReply(ResultOfOperationValue value) {
        this.resultOfOperationReply = value;
    }

    /**
     * Gets the value of the getDictTableReply property.
     * 
     * @return
     *     possible object is
     *     {@link GetDictTableResultList }
     *     
     */
    public GetDictTableResultList getGetDictTableReply() {
        return getDictTableReply;
    }

    /**
     * Sets the value of the getDictTableReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDictTableResultList }
     *     
     */
    public void setGetDictTableReply(GetDictTableResultList value) {
        this.getDictTableReply = value;
    }

}
