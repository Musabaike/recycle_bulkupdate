
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AccessSessionRequest" type="{http://oss.huawei.com/webservice/ecare/services}AccessSessionValue"/&gt;
 *         &lt;element name="GetSubscribersRequest" type="{http://oss.huawei.com/webservice/ecare/services}QuerySubscirberConditionValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accessSessionRequest",
    "getSubscribersRequest"
})
@XmlRootElement(name = "getSubscribers")
public class GetSubscribers {

    @XmlElement(name = "AccessSessionRequest", required = true)
    protected AccessSessionValue accessSessionRequest;
    @XmlElement(name = "GetSubscribersRequest", required = true)
    protected QuerySubscirberConditionValue getSubscribersRequest;

    /**
     * Gets the value of the accessSessionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AccessSessionValue }
     *     
     */
    public AccessSessionValue getAccessSessionRequest() {
        return accessSessionRequest;
    }

    /**
     * Sets the value of the accessSessionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessSessionValue }
     *     
     */
    public void setAccessSessionRequest(AccessSessionValue value) {
        this.accessSessionRequest = value;
    }

    /**
     * Gets the value of the getSubscribersRequest property.
     * 
     * @return
     *     possible object is
     *     {@link QuerySubscirberConditionValue }
     *     
     */
    public QuerySubscirberConditionValue getGetSubscribersRequest() {
        return getSubscribersRequest;
    }

    /**
     * Sets the value of the getSubscribersRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuerySubscirberConditionValue }
     *     
     */
    public void setGetSubscribersRequest(QuerySubscirberConditionValue value) {
        this.getSubscribersRequest = value;
    }

}
