
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetAvailableIPsRequestDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetAvailableIPsRequestDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryCondition" type="{http://oss.huawei.com/webservice/ecare/services}GetAvailableIPsIn"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAvailableIPsRequestDetails", propOrder = {
    "queryCondition"
})
public class GetAvailableIPsRequestDetails {

    @XmlElement(required = true)
    protected GetAvailableIPsIn queryCondition;

    /**
     * Gets the value of the queryCondition property.
     * 
     * @return
     *     possible object is
     *     {@link GetAvailableIPsIn }
     *     
     */
    public GetAvailableIPsIn getQueryCondition() {
        return queryCondition;
    }

    /**
     * Sets the value of the queryCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAvailableIPsIn }
     *     
     */
    public void setQueryCondition(GetAvailableIPsIn value) {
        this.queryCondition = value;
    }

}
