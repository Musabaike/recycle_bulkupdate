
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeProductValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeProductValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriberId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isChangeRateplan" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="isValidNextBillCycle" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="newRateplanInfo" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" minOccurs="0"/&gt;
 *         &lt;element name="additionServicePackageInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="modifyProductInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="unsubscribeServicePackageInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="feeInfos" type="{http://oss.huawei.com/webservice/ecare/services}BusinessFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="activationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contractInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderContractValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="payMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeProductValue", propOrder = {
    "subscriberId",
    "isChangeRateplan",
    "isValidNextBillCycle",
    "newRateplanInfo",
    "additionServicePackageInfos",
    "modifyProductInfos",
    "unsubscribeServicePackageInfos",
    "feeInfos",
    "activationDate",
    "contractInfos",
    "payMode"
})
public class ChangeProductValue {

    @XmlElement(required = true)
    protected String subscriberId;
    protected boolean isChangeRateplan;
    protected boolean isValidNextBillCycle;
    protected OrderProductValue newRateplanInfo;
    protected List<OrderProductValue> additionServicePackageInfos;
    protected List<OrderProductValue> modifyProductInfos;
    protected List<OrderProductValue> unsubscribeServicePackageInfos;
    protected List<BusinessFeeValue> feeInfos;
    protected String activationDate;
    protected List<OrderContractValue> contractInfos;
    protected String payMode;

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberId(String value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the isChangeRateplan property.
     * 
     */
    public boolean isIsChangeRateplan() {
        return isChangeRateplan;
    }

    /**
     * Sets the value of the isChangeRateplan property.
     * 
     */
    public void setIsChangeRateplan(boolean value) {
        this.isChangeRateplan = value;
    }

    /**
     * Gets the value of the isValidNextBillCycle property.
     * 
     */
    public boolean isIsValidNextBillCycle() {
        return isValidNextBillCycle;
    }

    /**
     * Sets the value of the isValidNextBillCycle property.
     * 
     */
    public void setIsValidNextBillCycle(boolean value) {
        this.isValidNextBillCycle = value;
    }

    /**
     * Gets the value of the newRateplanInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderProductValue }
     *     
     */
    public OrderProductValue getNewRateplanInfo() {
        return newRateplanInfo;
    }

    /**
     * Sets the value of the newRateplanInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderProductValue }
     *     
     */
    public void setNewRateplanInfo(OrderProductValue value) {
        this.newRateplanInfo = value;
    }

    /**
     * Gets the value of the additionServicePackageInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionServicePackageInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionServicePackageInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getAdditionServicePackageInfos() {
        if (additionServicePackageInfos == null) {
            additionServicePackageInfos = new ArrayList<OrderProductValue>();
        }
        return this.additionServicePackageInfos;
    }

    /**
     * Gets the value of the modifyProductInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modifyProductInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModifyProductInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getModifyProductInfos() {
        if (modifyProductInfos == null) {
            modifyProductInfos = new ArrayList<OrderProductValue>();
        }
        return this.modifyProductInfos;
    }

    /**
     * Gets the value of the unsubscribeServicePackageInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the unsubscribeServicePackageInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnsubscribeServicePackageInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getUnsubscribeServicePackageInfos() {
        if (unsubscribeServicePackageInfos == null) {
            unsubscribeServicePackageInfos = new ArrayList<OrderProductValue>();
        }
        return this.unsubscribeServicePackageInfos;
    }

    /**
     * Gets the value of the feeInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessFeeValue }
     * 
     * 
     */
    public List<BusinessFeeValue> getFeeInfos() {
        if (feeInfos == null) {
            feeInfos = new ArrayList<BusinessFeeValue>();
        }
        return this.feeInfos;
    }

    /**
     * Gets the value of the activationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationDate() {
        return activationDate;
    }

    /**
     * Sets the value of the activationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationDate(String value) {
        this.activationDate = value;
    }

    /**
     * Gets the value of the contractInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderContractValue }
     * 
     * 
     */
    public List<OrderContractValue> getContractInfos() {
        if (contractInfos == null) {
            contractInfos = new ArrayList<OrderContractValue>();
        }
        return this.contractInfos;
    }

    /**
     * Gets the value of the payMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayMode() {
        return payMode;
    }

    /**
     * Sets the value of the payMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayMode(String value) {
        this.payMode = value;
    }

}
