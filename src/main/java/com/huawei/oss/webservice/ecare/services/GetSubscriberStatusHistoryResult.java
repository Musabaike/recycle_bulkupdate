
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSubscriberStatusHistoryResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSubscriberStatusHistoryResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="getSubscriberStatusHistoryResult" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfSubStatusHistoryValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriberStatusHistoryResult", propOrder = {
    "getSubscriberStatusHistoryResult"
})
public class GetSubscriberStatusHistoryResult {

    protected List<ResultOfSubStatusHistoryValue> getSubscriberStatusHistoryResult;

    /**
     * Gets the value of the getSubscriberStatusHistoryResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getSubscriberStatusHistoryResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetSubscriberStatusHistoryResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResultOfSubStatusHistoryValue }
     * 
     * 
     */
    public List<ResultOfSubStatusHistoryValue> getGetSubscriberStatusHistoryResult() {
        if (getSubscriberStatusHistoryResult == null) {
            getSubscriberStatusHistoryResult = new ArrayList<ResultOfSubStatusHistoryValue>();
        }
        return this.getSubscriberStatusHistoryResult;
    }

}
