package com.yannitech.recycle.bean;

import org.apache.camel.builder.RouteBuilder;

public class MyRouteBuilder extends RouteBuilder{
	

	@Override
	public void configure() throws Exception {
		
		onException(Exception.class).maximumRedeliveries(3).useOriginalMessage().handled(true);

		 from("{{recycle.ceaselist.file}}").delay(1200000)//.delay(180000)//
 		.log("Received message. ")
 		//.choice()
 		//.when(header("CamelFileNameConsumed")
 		//		.endsWith(".xlsx"))
 		.to("{{excel.routein}}");
		
		from("{{excel.routeout.completedlist}}").delay(600000)//.delay(120000)//
		.log("Completed message. ")
		.to("{{recycle.complete.file}}");
		
		
	}

}
