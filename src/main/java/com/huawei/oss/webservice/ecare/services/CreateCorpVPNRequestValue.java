
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateCorpVPNRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCorpVPNRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="corpVPNInfo" type="{http://oss.huawei.com/webservice/ecare/services}CorpVPNValue" minOccurs="0"/&gt;
 *         &lt;element name="accountInfo" type="{http://oss.huawei.com/webservice/ecare/services}AccountValue" minOccurs="0"/&gt;
 *         &lt;element name="groupRatePlan" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" minOccurs="0"/&gt;
 *         &lt;element name="PICInfo" type="{http://oss.huawei.com/webservice/ecare/services}PICInfoValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCorpVPNRequestValue", propOrder = {
    "customerId",
    "corpVPNInfo",
    "accountInfo",
    "groupRatePlan",
    "picInfo"
})
public class CreateCorpVPNRequestValue {

    protected String customerId;
    protected CorpVPNValue corpVPNInfo;
    protected AccountValue accountInfo;
    protected OrderProductValue groupRatePlan;
    @XmlElement(name = "PICInfo")
    protected PICInfoValue picInfo;

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the corpVPNInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CorpVPNValue }
     *     
     */
    public CorpVPNValue getCorpVPNInfo() {
        return corpVPNInfo;
    }

    /**
     * Sets the value of the corpVPNInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorpVPNValue }
     *     
     */
    public void setCorpVPNInfo(CorpVPNValue value) {
        this.corpVPNInfo = value;
    }

    /**
     * Gets the value of the accountInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountValue }
     *     
     */
    public AccountValue getAccountInfo() {
        return accountInfo;
    }

    /**
     * Sets the value of the accountInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountValue }
     *     
     */
    public void setAccountInfo(AccountValue value) {
        this.accountInfo = value;
    }

    /**
     * Gets the value of the groupRatePlan property.
     * 
     * @return
     *     possible object is
     *     {@link OrderProductValue }
     *     
     */
    public OrderProductValue getGroupRatePlan() {
        return groupRatePlan;
    }

    /**
     * Sets the value of the groupRatePlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderProductValue }
     *     
     */
    public void setGroupRatePlan(OrderProductValue value) {
        this.groupRatePlan = value;
    }

    /**
     * Gets the value of the picInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PICInfoValue }
     *     
     */
    public PICInfoValue getPICInfo() {
        return picInfo;
    }

    /**
     * Sets the value of the picInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PICInfoValue }
     *     
     */
    public void setPICInfo(PICInfoValue value) {
        this.picInfo = value;
    }

}
