
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnLockIPReply" type="{http://oss.huawei.com/webservice/ecare/services}UnLockIPReplyDetail"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "unLockIPReply"
})
@XmlRootElement(name = "unLockIPResponse")
public class UnLockIPResponse {

    @XmlElement(name = "UnLockIPReply", required = true)
    protected UnLockIPReplyDetail unLockIPReply;

    /**
     * Gets the value of the unLockIPReply property.
     * 
     * @return
     *     possible object is
     *     {@link UnLockIPReplyDetail }
     *     
     */
    public UnLockIPReplyDetail getUnLockIPReply() {
        return unLockIPReply;
    }

    /**
     * Sets the value of the unLockIPReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnLockIPReplyDetail }
     *     
     */
    public void setUnLockIPReply(UnLockIPReplyDetail value) {
        this.unLockIPReply = value;
    }

}
