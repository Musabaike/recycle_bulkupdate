package com.yannitech.recycle.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;

public class RecycleUtils {

	public static final QName SERVICE_NAME = new QName("http://oss.huawei.com/webservice/ecare/services", "Boss4ECareInterfaceService");
	
		public static final String OPERATORCODE = "TM Production";
		public static final String PSSD = "%T3lkom9525#"; 
	
	/* Prod
	public static final String OPERATORCODE = "TMRecycle";
	public static final String PSSD = "recycle135"; */
	
	public static final String ACCESSCHANNEL = "3";	
	public static final String COMPLETEDLIST_DIR = "/home/recycle/data/completedlist";
	//public static final String COMPLETEDLIST_DIR = "/home/bigsim/dev/Recycle/eclipse/com.yannitech.recycle/data/completedlist";
	//public static final String COMPLETEDLIST_DIR_TMP = "/home/recycle/data/completedlist/.tmp";
	public static final String INVALIDRANGE_MSG = "Invalid range. Only Telkom's mobile range can be recycled";
	public static final String NOTAVAILABLEFORRECYCLE_MSG = "MSISDN is not available for re-use or recycling";
	public static final int COLUMN_CUSTOMER_ID	= 0;
	public static final int COLUMN_SERVICE_ID = 1;
	public static final int COLUMN_SUBSCRIBER_ID = 2;
	public static final int COLUMN_RESULT_CODE = 3;
	public static final int COLUMN_BULK_CEASED_DATE = 4;
	public static final int COLUMN_STATUS = 5;
	public static final String COLUMN_STATUS_HEADER = "STATUS";
	public static final String SUCCESSFUL = "Successful";
	public static final String FAIL = "Fail";
	
	public static boolean onlyDigits(String str) {
		 
		String regex ="[0-9]+";
		
		Pattern p = Pattern.compile(regex);
		
		if (str == null ) {
			return false;
		}
		
		Matcher m = p.matcher(str);
		
		return m.matches();
	}
	
	//Only numbers in the 081*, 061*, 065*, 067*,  ranges may be recycled. Subranges include 0658*, 0659*, 0670*, 0671*, 0672*, 0680*, 0681*, 0682*, 0683*, 0684*, 0685*, 0811*, 0813*, 0814*, 0815*, 0817*, 0691*, 0695*.
	public static boolean checkRange(String msisdn){
	  
		if(msisdn.length() == 10 ) {
		    if(msisdn.startsWith("081") || msisdn.startsWith("061")  || msisdn.startsWith("065") || msisdn.startsWith("067") 
		    		|| msisdn.startsWith("0680") || msisdn.startsWith("0681") || msisdn.startsWith("0682") || msisdn.startsWith("0683") 
		    		|| msisdn.startsWith("0684") || msisdn.startsWith("0685") || msisdn.startsWith("0691") || msisdn.startsWith("0692")
		    		|| msisdn.startsWith("0693") || msisdn.startsWith("0694") || msisdn.startsWith("0695")){
		        return true;
		       
		    }
		}

	    return false;
	}
}
