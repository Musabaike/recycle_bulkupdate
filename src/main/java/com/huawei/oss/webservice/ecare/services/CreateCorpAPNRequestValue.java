
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateCorpAPNRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCorpAPNRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="corpAPNInfo" type="{http://oss.huawei.com/webservice/ecare/services}CorpAPNValue" minOccurs="0"/&gt;
 *         &lt;element name="accountInfo" type="{http://oss.huawei.com/webservice/ecare/services}AccountValue" minOccurs="0"/&gt;
 *         &lt;element name="groupRatePlan" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" minOccurs="0"/&gt;
 *         &lt;element name="PICInfo" type="{http://oss.huawei.com/webservice/ecare/services}PICInfoValue" minOccurs="0"/&gt;
 *         &lt;element name="additionServicePackageInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCorpAPNRequestValue", propOrder = {
    "customerId",
    "corpAPNInfo",
    "accountInfo",
    "groupRatePlan",
    "picInfo",
    "additionServicePackageInfos"
})
public class CreateCorpAPNRequestValue {

    protected String customerId;
    protected CorpAPNValue corpAPNInfo;
    protected AccountValue accountInfo;
    protected OrderProductValue groupRatePlan;
    @XmlElement(name = "PICInfo")
    protected PICInfoValue picInfo;
    protected List<OrderProductValue> additionServicePackageInfos;

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the corpAPNInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CorpAPNValue }
     *     
     */
    public CorpAPNValue getCorpAPNInfo() {
        return corpAPNInfo;
    }

    /**
     * Sets the value of the corpAPNInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorpAPNValue }
     *     
     */
    public void setCorpAPNInfo(CorpAPNValue value) {
        this.corpAPNInfo = value;
    }

    /**
     * Gets the value of the accountInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountValue }
     *     
     */
    public AccountValue getAccountInfo() {
        return accountInfo;
    }

    /**
     * Sets the value of the accountInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountValue }
     *     
     */
    public void setAccountInfo(AccountValue value) {
        this.accountInfo = value;
    }

    /**
     * Gets the value of the groupRatePlan property.
     * 
     * @return
     *     possible object is
     *     {@link OrderProductValue }
     *     
     */
    public OrderProductValue getGroupRatePlan() {
        return groupRatePlan;
    }

    /**
     * Sets the value of the groupRatePlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderProductValue }
     *     
     */
    public void setGroupRatePlan(OrderProductValue value) {
        this.groupRatePlan = value;
    }

    /**
     * Gets the value of the picInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PICInfoValue }
     *     
     */
    public PICInfoValue getPICInfo() {
        return picInfo;
    }

    /**
     * Sets the value of the picInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PICInfoValue }
     *     
     */
    public void setPICInfo(PICInfoValue value) {
        this.picInfo = value;
    }

    /**
     * Gets the value of the additionServicePackageInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionServicePackageInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionServicePackageInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getAdditionServicePackageInfos() {
        if (additionServicePackageInfos == null) {
            additionServicePackageInfos = new ArrayList<OrderProductValue>();
        }
        return this.additionServicePackageInfos;
    }

}
