
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AccessSessionRequest" type="{http://oss.huawei.com/webservice/ecare/services}AccessSessionValue"/&gt;
 *         &lt;element name="PrepaidActivationRequest" type="{http://oss.huawei.com/webservice/ecare/services}PrepaidActivationValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accessSessionRequest",
    "prepaidActivationRequest"
})
@XmlRootElement(name = "prepaidActivation")
public class PrepaidActivation {

    @XmlElement(name = "AccessSessionRequest", required = true)
    protected AccessSessionValue accessSessionRequest;
    @XmlElement(name = "PrepaidActivationRequest", required = true)
    protected PrepaidActivationValue prepaidActivationRequest;

    /**
     * Gets the value of the accessSessionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AccessSessionValue }
     *     
     */
    public AccessSessionValue getAccessSessionRequest() {
        return accessSessionRequest;
    }

    /**
     * Sets the value of the accessSessionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessSessionValue }
     *     
     */
    public void setAccessSessionRequest(AccessSessionValue value) {
        this.accessSessionRequest = value;
    }

    /**
     * Gets the value of the prepaidActivationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link PrepaidActivationValue }
     *     
     */
    public PrepaidActivationValue getPrepaidActivationRequest() {
        return prepaidActivationRequest;
    }

    /**
     * Sets the value of the prepaidActivationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrepaidActivationValue }
     *     
     */
    public void setPrepaidActivationRequest(PrepaidActivationValue value) {
        this.prepaidActivationRequest = value;
    }

}
