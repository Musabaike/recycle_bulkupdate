
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryZGCConditionValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryZGCConditionValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="zgcId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="zgcName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="startRow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="endRow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryZGCConditionValue", propOrder = {
    "zgcId",
    "zgcName",
    "startRow",
    "endRow"
})
public class QueryZGCConditionValue {

    protected String zgcId;
    protected String zgcName;
    protected String startRow;
    protected String endRow;

    /**
     * Gets the value of the zgcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZgcId() {
        return zgcId;
    }

    /**
     * Sets the value of the zgcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZgcId(String value) {
        this.zgcId = value;
    }

    /**
     * Gets the value of the zgcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZgcName() {
        return zgcName;
    }

    /**
     * Sets the value of the zgcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZgcName(String value) {
        this.zgcName = value;
    }

    /**
     * Gets the value of the startRow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartRow() {
        return startRow;
    }

    /**
     * Sets the value of the startRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartRow(String value) {
        this.startRow = value;
    }

    /**
     * Gets the value of the endRow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndRow() {
        return endRow;
    }

    /**
     * Sets the value of the endRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndRow(String value) {
        this.endRow = value;
    }

}
