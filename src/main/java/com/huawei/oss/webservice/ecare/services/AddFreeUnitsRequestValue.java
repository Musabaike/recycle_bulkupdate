
package com.huawei.oss.webservice.ecare.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddFreeUnitsRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddFreeUnitsRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriberID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fuType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="freeVolume" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="expDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddFreeUnitsRequestValue", propOrder = {
    "subscriberID",
    "fuType",
    "freeVolume",
    "expDate"
})
public class AddFreeUnitsRequestValue {

    @XmlElement(required = true)
    protected String subscriberID;
    @XmlElement(required = true)
    protected String fuType;
    @XmlElement(required = true)
    protected BigDecimal freeVolume;
    protected String expDate;

    /**
     * Gets the value of the subscriberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberID() {
        return subscriberID;
    }

    /**
     * Sets the value of the subscriberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberID(String value) {
        this.subscriberID = value;
    }

    /**
     * Gets the value of the fuType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuType() {
        return fuType;
    }

    /**
     * Sets the value of the fuType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuType(String value) {
        this.fuType = value;
    }

    /**
     * Gets the value of the freeVolume property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFreeVolume() {
        return freeVolume;
    }

    /**
     * Sets the value of the freeVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFreeVolume(BigDecimal value) {
        this.freeVolume = value;
    }

    /**
     * Gets the value of the expDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpDate() {
        return expDate;
    }

    /**
     * Sets the value of the expDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpDate(String value) {
        this.expDate = value;
    }

}
