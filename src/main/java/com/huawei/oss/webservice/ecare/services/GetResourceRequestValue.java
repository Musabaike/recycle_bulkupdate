
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetResourceRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetResourceRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deptid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resourcetype" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="modelid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetResourceRequestValue", propOrder = {
    "deptid",
    "status",
    "mac",
    "resourcetype",
    "modelid"
})
public class GetResourceRequestValue {

    @XmlElement(required = true)
    protected String deptid;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected String mac;
    @XmlElement(required = true)
    protected String resourcetype;
    @XmlElement(required = true)
    protected String modelid;

    /**
     * Gets the value of the deptid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeptid() {
        return deptid;
    }

    /**
     * Sets the value of the deptid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeptid(String value) {
        this.deptid = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the mac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMac() {
        return mac;
    }

    /**
     * Sets the value of the mac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMac(String value) {
        this.mac = value;
    }

    /**
     * Gets the value of the resourcetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourcetype() {
        return resourcetype;
    }

    /**
     * Sets the value of the resourcetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourcetype(String value) {
        this.resourcetype = value;
    }

    /**
     * Gets the value of the modelid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelid() {
        return modelid;
    }

    /**
     * Sets the value of the modelid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelid(String value) {
        this.modelid = value;
    }

}
