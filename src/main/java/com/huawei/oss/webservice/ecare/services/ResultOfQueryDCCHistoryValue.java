
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResultOfQueryDCCHistoryValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResultOfQueryDCCHistoryValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QueryDCCHistoryResult" type="{http://oss.huawei.com/webservice/ecare/services}DCCHistory" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultOfQueryDCCHistoryValue", propOrder = {
    "queryDCCHistoryResult"
})
public class ResultOfQueryDCCHistoryValue {

    @XmlElement(name = "QueryDCCHistoryResult")
    protected List<DCCHistory> queryDCCHistoryResult;

    /**
     * Gets the value of the queryDCCHistoryResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the queryDCCHistoryResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQueryDCCHistoryResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DCCHistory }
     * 
     * 
     */
    public List<DCCHistory> getQueryDCCHistoryResult() {
        if (queryDCCHistoryResult == null) {
            queryDCCHistoryResult = new ArrayList<DCCHistory>();
        }
        return this.queryDCCHistoryResult;
    }

}
