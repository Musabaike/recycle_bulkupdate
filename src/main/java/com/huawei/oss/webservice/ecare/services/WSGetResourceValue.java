
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSGetResourceValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSGetResourceValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resourceid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resorcecode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mac" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resourcetype" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="modelid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSGetResourceValue", propOrder = {
    "resourceid",
    "resorcecode",
    "mac",
    "resourcetype",
    "modelid",
    "status"
})
public class WSGetResourceValue {

    @XmlElement(required = true)
    protected String resourceid;
    @XmlElement(required = true)
    protected String resorcecode;
    @XmlElement(required = true)
    protected String mac;
    @XmlElement(required = true)
    protected String resourcetype;
    @XmlElement(required = true)
    protected String modelid;
    @XmlElement(required = true)
    protected String status;

    /**
     * Gets the value of the resourceid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceid() {
        return resourceid;
    }

    /**
     * Sets the value of the resourceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceid(String value) {
        this.resourceid = value;
    }

    /**
     * Gets the value of the resorcecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResorcecode() {
        return resorcecode;
    }

    /**
     * Sets the value of the resorcecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResorcecode(String value) {
        this.resorcecode = value;
    }

    /**
     * Gets the value of the mac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMac() {
        return mac;
    }

    /**
     * Sets the value of the mac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMac(String value) {
        this.mac = value;
    }

    /**
     * Gets the value of the resourcetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourcetype() {
        return resourcetype;
    }

    /**
     * Sets the value of the resourcetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourcetype(String value) {
        this.resourcetype = value;
    }

    /**
     * Gets the value of the modelid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelid() {
        return modelid;
    }

    /**
     * Sets the value of the modelid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelid(String value) {
        this.modelid = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
