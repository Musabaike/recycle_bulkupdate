
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChangeSubscriberProductsReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "changeSubscriberProductsReply"
})
@XmlRootElement(name = "changeSubscriberProductsResponse")
public class ChangeSubscriberProductsResponse {

    @XmlElement(name = "ChangeSubscriberProductsReply", required = true)
    protected ResultOfOperationValue changeSubscriberProductsReply;

    /**
     * Gets the value of the changeSubscriberProductsReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getChangeSubscriberProductsReply() {
        return changeSubscriberProductsReply;
    }

    /**
     * Sets the value of the changeSubscriberProductsReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setChangeSubscriberProductsReply(ResultOfOperationValue value) {
        this.changeSubscriberProductsReply = value;
    }

}
