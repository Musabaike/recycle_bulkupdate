
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetUsageAmountReplyDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetUsageAmountReplyDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetUsageAmountDetails" type="{http://oss.huawei.com/webservice/ecare/services}UserUsageAmountDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetUsageAmountReplyDetails", propOrder = {
    "getUsageAmountDetails"
})
public class GetUsageAmountReplyDetails {

    @XmlElement(name = "GetUsageAmountDetails")
    protected List<UserUsageAmountDetails> getUsageAmountDetails;

    /**
     * Gets the value of the getUsageAmountDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getUsageAmountDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetUsageAmountDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserUsageAmountDetails }
     * 
     * 
     */
    public List<UserUsageAmountDetails> getGetUsageAmountDetails() {
        if (getUsageAmountDetails == null) {
            getUsageAmountDetails = new ArrayList<UserUsageAmountDetails>();
        }
        return this.getUsageAmountDetails;
    }

}
