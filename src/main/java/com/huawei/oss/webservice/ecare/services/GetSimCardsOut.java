
package com.huawei.oss.webservice.ecare.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSimCardsOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSimCardsOut"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="iccid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="puk1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="puk2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pin1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pin2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="imsi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="esn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="akey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resStatus" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSimCardsOut", propOrder = {
    "iccid",
    "puk1",
    "puk2",
    "pin1",
    "pin2",
    "imsi",
    "esn",
    "akey",
    "resStatus"
})
public class GetSimCardsOut {

    @XmlElement(required = true)
    protected String iccid;
    @XmlElement(required = true)
    protected String puk1;
    @XmlElement(required = true)
    protected String puk2;
    @XmlElement(required = true)
    protected String pin1;
    @XmlElement(required = true)
    protected String pin2;
    @XmlElement(required = true)
    protected String imsi;
    @XmlElement(required = true)
    protected String esn;
    @XmlElement(required = true)
    protected String akey;
    @XmlElement(required = true)
    protected BigDecimal resStatus;

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIccid() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIccid(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the puk1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuk1() {
        return puk1;
    }

    /**
     * Sets the value of the puk1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuk1(String value) {
        this.puk1 = value;
    }

    /**
     * Gets the value of the puk2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuk2() {
        return puk2;
    }

    /**
     * Sets the value of the puk2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuk2(String value) {
        this.puk2 = value;
    }

    /**
     * Gets the value of the pin1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPin1() {
        return pin1;
    }

    /**
     * Sets the value of the pin1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPin1(String value) {
        this.pin1 = value;
    }

    /**
     * Gets the value of the pin2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPin2() {
        return pin2;
    }

    /**
     * Sets the value of the pin2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPin2(String value) {
        this.pin2 = value;
    }

    /**
     * Gets the value of the imsi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * Sets the value of the imsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImsi(String value) {
        this.imsi = value;
    }

    /**
     * Gets the value of the esn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsn() {
        return esn;
    }

    /**
     * Sets the value of the esn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsn(String value) {
        this.esn = value;
    }

    /**
     * Gets the value of the akey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAkey() {
        return akey;
    }

    /**
     * Sets the value of the akey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAkey(String value) {
        this.akey = value;
    }

    /**
     * Gets the value of the resStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getResStatus() {
        return resStatus;
    }

    /**
     * Sets the value of the resStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setResStatus(BigDecimal value) {
        this.resStatus = value;
    }

}
