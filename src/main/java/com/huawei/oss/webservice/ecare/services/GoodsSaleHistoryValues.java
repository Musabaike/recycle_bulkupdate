
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GoodsSaleHistoryValues complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GoodsSaleHistoryValues"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GoodsSaleHistoryValue" type="{http://oss.huawei.com/webservice/ecare/services}GoodsSaleHistoryValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GoodsSaleHistoryValues", propOrder = {
    "goodsSaleHistoryValue"
})
public class GoodsSaleHistoryValues {

    @XmlElement(name = "GoodsSaleHistoryValue")
    protected List<GoodsSaleHistoryValue> goodsSaleHistoryValue;

    /**
     * Gets the value of the goodsSaleHistoryValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the goodsSaleHistoryValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGoodsSaleHistoryValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GoodsSaleHistoryValue }
     * 
     * 
     */
    public List<GoodsSaleHistoryValue> getGoodsSaleHistoryValue() {
        if (goodsSaleHistoryValue == null) {
            goodsSaleHistoryValue = new ArrayList<GoodsSaleHistoryValue>();
        }
        return this.goodsSaleHistoryValue;
    }

}
