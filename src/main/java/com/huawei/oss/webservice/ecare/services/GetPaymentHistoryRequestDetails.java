
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPaymentHistoryRequestDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPaymentHistoryRequestDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QueryCondition" type="{http://oss.huawei.com/webservice/ecare/services}GetPaymentHistoryIn"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPaymentHistoryRequestDetails", propOrder = {
    "queryCondition"
})
public class GetPaymentHistoryRequestDetails {

    @XmlElement(name = "QueryCondition", required = true)
    protected GetPaymentHistoryIn queryCondition;

    /**
     * Gets the value of the queryCondition property.
     * 
     * @return
     *     possible object is
     *     {@link GetPaymentHistoryIn }
     *     
     */
    public GetPaymentHistoryIn getQueryCondition() {
        return queryCondition;
    }

    /**
     * Sets the value of the queryCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPaymentHistoryIn }
     *     
     */
    public void setQueryCondition(GetPaymentHistoryIn value) {
        this.queryCondition = value;
    }

}
