
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryCUGMembersResultValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryCUGMembersResultValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUGMemberValues" type="{http://oss.huawei.com/webservice/ecare/services}CUGMemberValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCUGMembersResultValue", propOrder = {
    "cugMemberValues"
})
public class QueryCUGMembersResultValue {

    @XmlElement(name = "CUGMemberValues")
    protected List<CUGMemberValue> cugMemberValues;

    /**
     * Gets the value of the cugMemberValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cugMemberValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCUGMemberValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CUGMemberValue }
     * 
     * 
     */
    public List<CUGMemberValue> getCUGMemberValues() {
        if (cugMemberValues == null) {
            cugMemberValues = new ArrayList<CUGMemberValue>();
        }
        return this.cugMemberValues;
    }

}
