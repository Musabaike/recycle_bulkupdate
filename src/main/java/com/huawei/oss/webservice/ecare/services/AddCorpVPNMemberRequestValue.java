
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddCorpVPNMemberRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddCorpVPNMemberRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="groupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="memberSubId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shortNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isFullPay" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="fullPayAcctId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fullPayRelaMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fullPayRelaValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="splitInfos" type="{http://oss.huawei.com/webservice/ecare/services}AcctSplitValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddCorpVPNMemberRequestValue", propOrder = {
    "groupId",
    "memberSubId",
    "shortNo",
    "isFullPay",
    "fullPayAcctId",
    "fullPayRelaMode",
    "fullPayRelaValue",
    "splitInfos"
})
public class AddCorpVPNMemberRequestValue {

    protected String groupId;
    protected String memberSubId;
    protected String shortNo;
    protected Boolean isFullPay;
    protected String fullPayAcctId;
    protected String fullPayRelaMode;
    protected String fullPayRelaValue;
    protected List<AcctSplitValue> splitInfos;

    /**
     * Gets the value of the groupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the memberSubId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberSubId() {
        return memberSubId;
    }

    /**
     * Sets the value of the memberSubId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberSubId(String value) {
        this.memberSubId = value;
    }

    /**
     * Gets the value of the shortNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortNo() {
        return shortNo;
    }

    /**
     * Sets the value of the shortNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortNo(String value) {
        this.shortNo = value;
    }

    /**
     * Gets the value of the isFullPay property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsFullPay() {
        return isFullPay;
    }

    /**
     * Sets the value of the isFullPay property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFullPay(Boolean value) {
        this.isFullPay = value;
    }

    /**
     * Gets the value of the fullPayAcctId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullPayAcctId() {
        return fullPayAcctId;
    }

    /**
     * Sets the value of the fullPayAcctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullPayAcctId(String value) {
        this.fullPayAcctId = value;
    }

    /**
     * Gets the value of the fullPayRelaMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullPayRelaMode() {
        return fullPayRelaMode;
    }

    /**
     * Sets the value of the fullPayRelaMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullPayRelaMode(String value) {
        this.fullPayRelaMode = value;
    }

    /**
     * Gets the value of the fullPayRelaValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullPayRelaValue() {
        return fullPayRelaValue;
    }

    /**
     * Sets the value of the fullPayRelaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullPayRelaValue(String value) {
        this.fullPayRelaValue = value;
    }

    /**
     * Gets the value of the splitInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splitInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplitInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AcctSplitValue }
     * 
     * 
     */
    public List<AcctSplitValue> getSplitInfos() {
        if (splitInfos == null) {
            splitInfos = new ArrayList<AcctSplitValue>();
        }
        return this.splitInfos;
    }

}
