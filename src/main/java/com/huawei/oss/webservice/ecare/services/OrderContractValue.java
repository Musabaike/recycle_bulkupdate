
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderContractValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderContractValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contractDefId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relatedType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="relatedId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderContractValue", propOrder = {
    "contractDefId",
    "relatedType",
    "relatedId"
})
public class OrderContractValue {

    protected String contractDefId;
    protected String relatedType;
    protected String relatedId;

    /**
     * Gets the value of the contractDefId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractDefId() {
        return contractDefId;
    }

    /**
     * Sets the value of the contractDefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractDefId(String value) {
        this.contractDefId = value;
    }

    /**
     * Gets the value of the relatedType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedType() {
        return relatedType;
    }

    /**
     * Sets the value of the relatedType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedType(String value) {
        this.relatedType = value;
    }

    /**
     * Gets the value of the relatedId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedId() {
        return relatedId;
    }

    /**
     * Sets the value of the relatedId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedId(String value) {
        this.relatedId = value;
    }

}
