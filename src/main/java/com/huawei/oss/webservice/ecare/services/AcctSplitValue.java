
package com.huawei.oss.webservice.ecare.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcctSplitValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AcctSplitValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="payEntityAcctId" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="relaMode" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="relaValue1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="feeItemsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="feeItemGroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctSplitValue", propOrder = {
    "payEntityAcctId",
    "relaMode",
    "relaValue1",
    "priority",
    "feeItemsId",
    "feeItemGroupId"
})
public class AcctSplitValue {

    @XmlElement(required = true)
    protected BigDecimal payEntityAcctId;
    @XmlElement(required = true)
    protected BigDecimal relaMode;
    protected String relaValue1;
    protected String priority;
    @XmlElement(required = true)
    protected String feeItemsId;
    protected String feeItemGroupId;

    /**
     * Gets the value of the payEntityAcctId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayEntityAcctId() {
        return payEntityAcctId;
    }

    /**
     * Sets the value of the payEntityAcctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayEntityAcctId(BigDecimal value) {
        this.payEntityAcctId = value;
    }

    /**
     * Gets the value of the relaMode property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRelaMode() {
        return relaMode;
    }

    /**
     * Sets the value of the relaMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRelaMode(BigDecimal value) {
        this.relaMode = value;
    }

    /**
     * Gets the value of the relaValue1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelaValue1() {
        return relaValue1;
    }

    /**
     * Sets the value of the relaValue1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelaValue1(String value) {
        this.relaValue1 = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the feeItemsId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeItemsId() {
        return feeItemsId;
    }

    /**
     * Sets the value of the feeItemsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeItemsId(String value) {
        this.feeItemsId = value;
    }

    /**
     * Gets the value of the feeItemGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeItemGroupId() {
        return feeItemGroupId;
    }

    /**
     * Sets the value of the feeItemGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeItemGroupId(String value) {
        this.feeItemGroupId = value;
    }

}
