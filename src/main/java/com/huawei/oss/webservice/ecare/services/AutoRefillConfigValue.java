
package com.huawei.oss.webservice.ecare.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutoRefillConfigValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoRefillConfigValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="paymentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="autoRefillType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="informBalanceAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="autoRefillDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="autoRefillAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="bonusType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bankAcctno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creditCardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creditcardeffdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creditcardexpdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoRefillConfigValue", propOrder = {
    "paymentId",
    "autoRefillType",
    "informBalanceAmount",
    "autoRefillDate",
    "autoRefillAmount",
    "bonusType",
    "bankAcctno",
    "creditCardType",
    "creditcardeffdate",
    "creditcardexpdate"
})
public class AutoRefillConfigValue {

    protected String paymentId;
    protected String autoRefillType;
    protected BigDecimal informBalanceAmount;
    protected String autoRefillDate;
    protected BigDecimal autoRefillAmount;
    protected String bonusType;
    protected String bankAcctno;
    protected String creditCardType;
    protected String creditcardeffdate;
    protected String creditcardexpdate;

    /**
     * Gets the value of the paymentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * Sets the value of the paymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentId(String value) {
        this.paymentId = value;
    }

    /**
     * Gets the value of the autoRefillType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoRefillType() {
        return autoRefillType;
    }

    /**
     * Sets the value of the autoRefillType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoRefillType(String value) {
        this.autoRefillType = value;
    }

    /**
     * Gets the value of the informBalanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInformBalanceAmount() {
        return informBalanceAmount;
    }

    /**
     * Sets the value of the informBalanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInformBalanceAmount(BigDecimal value) {
        this.informBalanceAmount = value;
    }

    /**
     * Gets the value of the autoRefillDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoRefillDate() {
        return autoRefillDate;
    }

    /**
     * Sets the value of the autoRefillDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoRefillDate(String value) {
        this.autoRefillDate = value;
    }

    /**
     * Gets the value of the autoRefillAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAutoRefillAmount() {
        return autoRefillAmount;
    }

    /**
     * Sets the value of the autoRefillAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAutoRefillAmount(BigDecimal value) {
        this.autoRefillAmount = value;
    }

    /**
     * Gets the value of the bonusType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBonusType() {
        return bonusType;
    }

    /**
     * Sets the value of the bonusType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBonusType(String value) {
        this.bonusType = value;
    }

    /**
     * Gets the value of the bankAcctno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAcctno() {
        return bankAcctno;
    }

    /**
     * Sets the value of the bankAcctno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAcctno(String value) {
        this.bankAcctno = value;
    }

    /**
     * Gets the value of the creditCardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardType() {
        return creditCardType;
    }

    /**
     * Sets the value of the creditCardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardType(String value) {
        this.creditCardType = value;
    }

    /**
     * Gets the value of the creditcardeffdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditcardeffdate() {
        return creditcardeffdate;
    }

    /**
     * Sets the value of the creditcardeffdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditcardeffdate(String value) {
        this.creditcardeffdate = value;
    }

    /**
     * Gets the value of the creditcardexpdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditcardexpdate() {
        return creditcardexpdate;
    }

    /**
     * Sets the value of the creditcardexpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditcardexpdate(String value) {
        this.creditcardexpdate = value;
    }

}
