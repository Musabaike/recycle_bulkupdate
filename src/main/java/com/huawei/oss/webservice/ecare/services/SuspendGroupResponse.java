
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SuspendGroupReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "suspendGroupReply"
})
@XmlRootElement(name = "suspendGroupResponse")
public class SuspendGroupResponse {

    @XmlElement(name = "SuspendGroupReply", required = true)
    protected ResultOfOperationValue suspendGroupReply;

    /**
     * Gets the value of the suspendGroupReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getSuspendGroupReply() {
        return suspendGroupReply;
    }

    /**
     * Sets the value of the suspendGroupReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setSuspendGroupReply(ResultOfOperationValue value) {
        this.suspendGroupReply = value;
    }

}
