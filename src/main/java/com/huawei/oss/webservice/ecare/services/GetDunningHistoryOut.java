
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDunningHistoryOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDunningHistoryOut"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActionReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActionDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActionExecuteStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActionExecuteBusinessStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActionFailReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDunningHistoryOut", propOrder = {
    "actionDate",
    "actionReason",
    "actionDescription",
    "actionName",
    "actionExecuteStatus",
    "actionExecuteBusinessStatus",
    "actionFailReason"
})
public class GetDunningHistoryOut {

    @XmlElement(name = "ActionDate")
    protected String actionDate;
    @XmlElement(name = "ActionReason")
    protected String actionReason;
    @XmlElement(name = "ActionDescription")
    protected String actionDescription;
    @XmlElement(name = "ActionName")
    protected String actionName;
    @XmlElement(name = "ActionExecuteStatus")
    protected String actionExecuteStatus;
    @XmlElement(name = "ActionExecuteBusinessStatus")
    protected String actionExecuteBusinessStatus;
    @XmlElement(name = "ActionFailReason")
    protected String actionFailReason;

    /**
     * Gets the value of the actionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionDate() {
        return actionDate;
    }

    /**
     * Sets the value of the actionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionDate(String value) {
        this.actionDate = value;
    }

    /**
     * Gets the value of the actionReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionReason() {
        return actionReason;
    }

    /**
     * Sets the value of the actionReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionReason(String value) {
        this.actionReason = value;
    }

    /**
     * Gets the value of the actionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionDescription() {
        return actionDescription;
    }

    /**
     * Sets the value of the actionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionDescription(String value) {
        this.actionDescription = value;
    }

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    /**
     * Gets the value of the actionExecuteStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionExecuteStatus() {
        return actionExecuteStatus;
    }

    /**
     * Sets the value of the actionExecuteStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionExecuteStatus(String value) {
        this.actionExecuteStatus = value;
    }

    /**
     * Gets the value of the actionExecuteBusinessStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionExecuteBusinessStatus() {
        return actionExecuteBusinessStatus;
    }

    /**
     * Sets the value of the actionExecuteBusinessStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionExecuteBusinessStatus(String value) {
        this.actionExecuteBusinessStatus = value;
    }

    /**
     * Gets the value of the actionFailReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionFailReason() {
        return actionFailReason;
    }

    /**
     * Sets the value of the actionFailReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionFailReason(String value) {
        this.actionFailReason = value;
    }

}
