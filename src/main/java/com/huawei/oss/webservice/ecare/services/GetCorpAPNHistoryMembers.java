
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AccessSessionRequest" type="{http://oss.huawei.com/webservice/ecare/services}AccessSessionValue"/&gt;
 *         &lt;element name="GetCorpAPNHistoryMembersRequest" type="{http://oss.huawei.com/webservice/ecare/services}GetCorpAPNHistoryMembersRequestValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accessSessionRequest",
    "getCorpAPNHistoryMembersRequest"
})
@XmlRootElement(name = "getCorpAPNHistoryMembers")
public class GetCorpAPNHistoryMembers {

    @XmlElement(name = "AccessSessionRequest", required = true)
    protected AccessSessionValue accessSessionRequest;
    @XmlElement(name = "GetCorpAPNHistoryMembersRequest", required = true)
    protected GetCorpAPNHistoryMembersRequestValue getCorpAPNHistoryMembersRequest;

    /**
     * Gets the value of the accessSessionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AccessSessionValue }
     *     
     */
    public AccessSessionValue getAccessSessionRequest() {
        return accessSessionRequest;
    }

    /**
     * Sets the value of the accessSessionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessSessionValue }
     *     
     */
    public void setAccessSessionRequest(AccessSessionValue value) {
        this.accessSessionRequest = value;
    }

    /**
     * Gets the value of the getCorpAPNHistoryMembersRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetCorpAPNHistoryMembersRequestValue }
     *     
     */
    public GetCorpAPNHistoryMembersRequestValue getGetCorpAPNHistoryMembersRequest() {
        return getCorpAPNHistoryMembersRequest;
    }

    /**
     * Sets the value of the getCorpAPNHistoryMembersRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCorpAPNHistoryMembersRequestValue }
     *     
     */
    public void setGetCorpAPNHistoryMembersRequest(GetCorpAPNHistoryMembersRequestValue value) {
        this.getCorpAPNHistoryMembersRequest = value;
    }

}
