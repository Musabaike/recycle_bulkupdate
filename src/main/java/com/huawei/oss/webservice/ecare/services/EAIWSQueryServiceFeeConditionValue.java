
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EAIWSQueryServiceFeeConditionValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EAIWSQueryServiceFeeConditionValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="prodIdList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="goodsSalesIdList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="contractIdList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ccBusinessCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="actionList" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSBusinessChargeAction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EAIWSQueryServiceFeeConditionValue", propOrder = {
    "msisdn",
    "prodIdList",
    "goodsSalesIdList",
    "contractIdList",
    "ccBusinessCode",
    "actionList"
})
public class EAIWSQueryServiceFeeConditionValue {

    protected String msisdn;
    protected List<String> prodIdList;
    protected List<String> goodsSalesIdList;
    protected List<String> contractIdList;
    @XmlElement(required = true)
    protected String ccBusinessCode;
    protected List<EAIWSBusinessChargeAction> actionList;

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the prodIdList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prodIdList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProdIdList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getProdIdList() {
        if (prodIdList == null) {
            prodIdList = new ArrayList<String>();
        }
        return this.prodIdList;
    }

    /**
     * Gets the value of the goodsSalesIdList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the goodsSalesIdList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGoodsSalesIdList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getGoodsSalesIdList() {
        if (goodsSalesIdList == null) {
            goodsSalesIdList = new ArrayList<String>();
        }
        return this.goodsSalesIdList;
    }

    /**
     * Gets the value of the contractIdList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractIdList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractIdList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getContractIdList() {
        if (contractIdList == null) {
            contractIdList = new ArrayList<String>();
        }
        return this.contractIdList;
    }

    /**
     * Gets the value of the ccBusinessCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcBusinessCode() {
        return ccBusinessCode;
    }

    /**
     * Sets the value of the ccBusinessCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcBusinessCode(String value) {
        this.ccBusinessCode = value;
    }

    /**
     * Gets the value of the actionList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actionList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActionList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EAIWSBusinessChargeAction }
     * 
     * 
     */
    public List<EAIWSBusinessChargeAction> getActionList() {
        if (actionList == null) {
            actionList = new ArrayList<EAIWSBusinessChargeAction>();
        }
        return this.actionList;
    }

}
