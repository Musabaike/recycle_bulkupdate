
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeMainAccountValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeMainAccountValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SubscriberId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OperType" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="MutiSimSubscriberId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeMainAccountValue", propOrder = {
    "subscriberId",
    "operType",
    "mutiSimSubscriberId"
})
public class ChangeMainAccountValue {

    @XmlElement(name = "SubscriberId", required = true)
    protected String subscriberId;
    @XmlElement(name = "OperType")
    protected int operType;
    @XmlElement(name = "MutiSimSubscriberId")
    protected String mutiSimSubscriberId;

    /**
     * Gets the value of the subscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberId() {
        return subscriberId;
    }

    /**
     * Sets the value of the subscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberId(String value) {
        this.subscriberId = value;
    }

    /**
     * Gets the value of the operType property.
     * 
     */
    public int getOperType() {
        return operType;
    }

    /**
     * Sets the value of the operType property.
     * 
     */
    public void setOperType(int value) {
        this.operType = value;
    }

    /**
     * Gets the value of the mutiSimSubscriberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMutiSimSubscriberId() {
        return mutiSimSubscriberId;
    }

    /**
     * Sets the value of the mutiSimSubscriberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMutiSimSubscriberId(String value) {
        this.mutiSimSubscriberId = value;
    }

}
