
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetSignedContractsReply" type="{http://oss.huawei.com/webservice/ecare/services}WSConstractSignedInfoValue"/&gt;
 *         &lt;element name="ResultOfOperationReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSignedContractsReply",
    "resultOfOperationReply"
})
@XmlRootElement(name = "getSignedContractsResponse")
public class GetSignedContractsResponse {

    @XmlElement(name = "GetSignedContractsReply", required = true)
    protected WSConstractSignedInfoValue getSignedContractsReply;
    @XmlElement(name = "ResultOfOperationReply", required = true)
    protected ResultOfOperationValue resultOfOperationReply;

    /**
     * Gets the value of the getSignedContractsReply property.
     * 
     * @return
     *     possible object is
     *     {@link WSConstractSignedInfoValue }
     *     
     */
    public WSConstractSignedInfoValue getGetSignedContractsReply() {
        return getSignedContractsReply;
    }

    /**
     * Sets the value of the getSignedContractsReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSConstractSignedInfoValue }
     *     
     */
    public void setGetSignedContractsReply(WSConstractSignedInfoValue value) {
        this.getSignedContractsReply = value;
    }

    /**
     * Gets the value of the resultOfOperationReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getResultOfOperationReply() {
        return resultOfOperationReply;
    }

    /**
     * Sets the value of the resultOfOperationReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setResultOfOperationReply(ResultOfOperationValue value) {
        this.resultOfOperationReply = value;
    }

}
