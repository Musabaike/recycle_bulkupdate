
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateAdditionalNumberRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateAdditionalNumberRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sMSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nZoneIndex" type="{http://www.w3.org/2001/XMLSchema}short"/&gt;
 *         &lt;element name="sAdditionalNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateAdditionalNumberRequestValue", propOrder = {
    "smsisdn",
    "nZoneIndex",
    "sAdditionalNumber"
})
public class UpdateAdditionalNumberRequestValue {

    @XmlElement(name = "sMSISDN", required = true)
    protected String smsisdn;
    protected short nZoneIndex;
    @XmlElement(required = true)
    protected String sAdditionalNumber;

    /**
     * Gets the value of the smsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMSISDN() {
        return smsisdn;
    }

    /**
     * Sets the value of the smsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMSISDN(String value) {
        this.smsisdn = value;
    }

    /**
     * Gets the value of the nZoneIndex property.
     * 
     */
    public short getNZoneIndex() {
        return nZoneIndex;
    }

    /**
     * Sets the value of the nZoneIndex property.
     * 
     */
    public void setNZoneIndex(short value) {
        this.nZoneIndex = value;
    }

    /**
     * Gets the value of the sAdditionalNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSAdditionalNumber() {
        return sAdditionalNumber;
    }

    /**
     * Sets the value of the sAdditionalNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSAdditionalNumber(String value) {
        this.sAdditionalNumber = value;
    }

}
