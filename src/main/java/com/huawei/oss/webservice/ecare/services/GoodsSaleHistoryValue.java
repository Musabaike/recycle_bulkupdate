
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GoodsSaleHistoryValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GoodsSaleHistoryValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriberID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="goodsType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="goodsModel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="goodsCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="salesID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="progID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="saleDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GoodsSaleHistoryValue", propOrder = {
    "subscriberID",
    "goodsType",
    "goodsModel",
    "goodsCode",
    "salesID",
    "progID",
    "price",
    "saleDate"
})
public class GoodsSaleHistoryValue {

    @XmlElement(required = true)
    protected String subscriberID;
    @XmlElement(required = true)
    protected String goodsType;
    @XmlElement(required = true)
    protected String goodsModel;
    @XmlElement(required = true)
    protected String goodsCode;
    @XmlElement(required = true)
    protected String salesID;
    @XmlElement(required = true)
    protected String progID;
    @XmlElement(required = true)
    protected String price;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar saleDate;

    /**
     * Gets the value of the subscriberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberID() {
        return subscriberID;
    }

    /**
     * Sets the value of the subscriberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberID(String value) {
        this.subscriberID = value;
    }

    /**
     * Gets the value of the goodsType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsType() {
        return goodsType;
    }

    /**
     * Sets the value of the goodsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsType(String value) {
        this.goodsType = value;
    }

    /**
     * Gets the value of the goodsModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsModel() {
        return goodsModel;
    }

    /**
     * Sets the value of the goodsModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsModel(String value) {
        this.goodsModel = value;
    }

    /**
     * Gets the value of the goodsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsCode() {
        return goodsCode;
    }

    /**
     * Sets the value of the goodsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsCode(String value) {
        this.goodsCode = value;
    }

    /**
     * Gets the value of the salesID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesID() {
        return salesID;
    }

    /**
     * Sets the value of the salesID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesID(String value) {
        this.salesID = value;
    }

    /**
     * Gets the value of the progID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgID() {
        return progID;
    }

    /**
     * Sets the value of the progID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgID(String value) {
        this.progID = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrice(String value) {
        this.price = value;
    }

    /**
     * Gets the value of the saleDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSaleDate() {
        return saleDate;
    }

    /**
     * Sets the value of the saleDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSaleDate(XMLGregorianCalendar value) {
        this.saleDate = value;
    }

}
