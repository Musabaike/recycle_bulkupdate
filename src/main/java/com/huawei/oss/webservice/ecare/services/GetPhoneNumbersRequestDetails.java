
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPhoneNumbersRequestDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPhoneNumbersRequestDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QueryCondition" type="{http://oss.huawei.com/webservice/ecare/services}GetPhoneNumbersIn"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPhoneNumbersRequestDetails", propOrder = {
    "queryCondition"
})
public class GetPhoneNumbersRequestDetails {

    @XmlElement(name = "QueryCondition", required = true)
    protected GetPhoneNumbersIn queryCondition;

    /**
     * Gets the value of the queryCondition property.
     * 
     * @return
     *     possible object is
     *     {@link GetPhoneNumbersIn }
     *     
     */
    public GetPhoneNumbersIn getQueryCondition() {
        return queryCondition;
    }

    /**
     * Sets the value of the queryCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPhoneNumbersIn }
     *     
     */
    public void setQueryCondition(GetPhoneNumbersIn value) {
        this.queryCondition = value;
    }

}
