
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EAIWSGoodsIntValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EAIWSGoodsIntValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="goodsDetail" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSGoodsValue" minOccurs="0"/&gt;
 *         &lt;element name="installmentDetail" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSInstallmentValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EAIWSGoodsIntValue", propOrder = {
    "goodsDetail",
    "installmentDetail"
})
public class EAIWSGoodsIntValue {

    protected EAIWSGoodsValue goodsDetail;
    protected EAIWSInstallmentValue installmentDetail;

    /**
     * Gets the value of the goodsDetail property.
     * 
     * @return
     *     possible object is
     *     {@link EAIWSGoodsValue }
     *     
     */
    public EAIWSGoodsValue getGoodsDetail() {
        return goodsDetail;
    }

    /**
     * Sets the value of the goodsDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAIWSGoodsValue }
     *     
     */
    public void setGoodsDetail(EAIWSGoodsValue value) {
        this.goodsDetail = value;
    }

    /**
     * Gets the value of the installmentDetail property.
     * 
     * @return
     *     possible object is
     *     {@link EAIWSInstallmentValue }
     *     
     */
    public EAIWSInstallmentValue getInstallmentDetail() {
        return installmentDetail;
    }

    /**
     * Sets the value of the installmentDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAIWSInstallmentValue }
     *     
     */
    public void setInstallmentDetail(EAIWSInstallmentValue value) {
        this.installmentDetail = value;
    }

}
