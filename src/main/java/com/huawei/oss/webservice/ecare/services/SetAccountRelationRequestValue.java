
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SetAccountRelationRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SetAccountRelationRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subGroupId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="subMemberId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="memberMdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="busiRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isFullPay" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="fullPayRelaMode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fullPayValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="currentAcctID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="splitInfos" type="{http://oss.huawei.com/webservice/ecare/services}AcctSplitValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetAccountRelationRequestValue", propOrder = {
    "subGroupId",
    "subMemberId",
    "memberMdn",
    "busiRemark",
    "isFullPay",
    "fullPayRelaMode",
    "fullPayValue",
    "currentAcctID",
    "splitInfos"
})
public class SetAccountRelationRequestValue {

    @XmlElement(required = true)
    protected String subGroupId;
    protected String subMemberId;
    protected String memberMdn;
    protected String busiRemark;
    protected boolean isFullPay;
    @XmlElement(required = true)
    protected String fullPayRelaMode;
    protected String fullPayValue;
    protected String currentAcctID;
    protected List<AcctSplitValue> splitInfos;

    /**
     * Gets the value of the subGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubGroupId() {
        return subGroupId;
    }

    /**
     * Sets the value of the subGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubGroupId(String value) {
        this.subGroupId = value;
    }

    /**
     * Gets the value of the subMemberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubMemberId() {
        return subMemberId;
    }

    /**
     * Sets the value of the subMemberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubMemberId(String value) {
        this.subMemberId = value;
    }

    /**
     * Gets the value of the memberMdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberMdn() {
        return memberMdn;
    }

    /**
     * Sets the value of the memberMdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberMdn(String value) {
        this.memberMdn = value;
    }

    /**
     * Gets the value of the busiRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusiRemark() {
        return busiRemark;
    }

    /**
     * Sets the value of the busiRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusiRemark(String value) {
        this.busiRemark = value;
    }

    /**
     * Gets the value of the isFullPay property.
     * 
     */
    public boolean isIsFullPay() {
        return isFullPay;
    }

    /**
     * Sets the value of the isFullPay property.
     * 
     */
    public void setIsFullPay(boolean value) {
        this.isFullPay = value;
    }

    /**
     * Gets the value of the fullPayRelaMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullPayRelaMode() {
        return fullPayRelaMode;
    }

    /**
     * Sets the value of the fullPayRelaMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullPayRelaMode(String value) {
        this.fullPayRelaMode = value;
    }

    /**
     * Gets the value of the fullPayValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullPayValue() {
        return fullPayValue;
    }

    /**
     * Sets the value of the fullPayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullPayValue(String value) {
        this.fullPayValue = value;
    }

    /**
     * Gets the value of the currentAcctID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentAcctID() {
        return currentAcctID;
    }

    /**
     * Sets the value of the currentAcctID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentAcctID(String value) {
        this.currentAcctID = value;
    }

    /**
     * Gets the value of the splitInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splitInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplitInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AcctSplitValue }
     * 
     * 
     */
    public List<AcctSplitValue> getSplitInfos() {
        if (splitInfos == null) {
            splitInfos = new ArrayList<AcctSplitValue>();
        }
        return this.splitInfos;
    }

}
