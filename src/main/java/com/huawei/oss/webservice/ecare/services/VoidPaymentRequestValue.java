
package com.huawei.oss.webservice.ecare.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoidPaymentRequestValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoidPaymentRequestValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="serialNo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="accountId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="rolledTransId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="feeInfos" type="{http://oss.huawei.com/webservice/ecare/services}BusinessFeeValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoidPaymentRequestValue", propOrder = {
    "serialNo",
    "accountId",
    "rolledTransId",
    "feeInfos"
})
public class VoidPaymentRequestValue {

    protected BigDecimal serialNo;
    protected BigDecimal accountId;
    protected BigDecimal rolledTransId;
    protected BusinessFeeValue feeInfos;

    /**
     * Gets the value of the serialNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSerialNo(BigDecimal value) {
        this.serialNo = value;
    }

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAccountId(BigDecimal value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the rolledTransId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRolledTransId() {
        return rolledTransId;
    }

    /**
     * Sets the value of the rolledTransId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRolledTransId(BigDecimal value) {
        this.rolledTransId = value;
    }

    /**
     * Gets the value of the feeInfos property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessFeeValue }
     *     
     */
    public BusinessFeeValue getFeeInfos() {
        return feeInfos;
    }

    /**
     * Sets the value of the feeInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessFeeValue }
     *     
     */
    public void setFeeInfos(BusinessFeeValue value) {
        this.feeInfos = value;
    }

}
