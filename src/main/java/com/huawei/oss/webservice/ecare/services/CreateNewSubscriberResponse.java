
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateNewSubscriberReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfCreateNewSubscriberValue" minOccurs="0"/&gt;
 *         &lt;element name="ResultOfOperationReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createNewSubscriberReply",
    "resultOfOperationReply"
})
@XmlRootElement(name = "createNewSubscriberResponse")
public class CreateNewSubscriberResponse {

    @XmlElement(name = "CreateNewSubscriberReply")
    protected ResultOfCreateNewSubscriberValue createNewSubscriberReply;
    @XmlElement(name = "ResultOfOperationReply", required = true)
    protected ResultOfOperationValue resultOfOperationReply;

    /**
     * Gets the value of the createNewSubscriberReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfCreateNewSubscriberValue }
     *     
     */
    public ResultOfCreateNewSubscriberValue getCreateNewSubscriberReply() {
        return createNewSubscriberReply;
    }

    /**
     * Sets the value of the createNewSubscriberReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfCreateNewSubscriberValue }
     *     
     */
    public void setCreateNewSubscriberReply(ResultOfCreateNewSubscriberValue value) {
        this.createNewSubscriberReply = value;
    }

    /**
     * Gets the value of the resultOfOperationReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getResultOfOperationReply() {
        return resultOfOperationReply;
    }

    /**
     * Sets the value of the resultOfOperationReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setResultOfOperationReply(ResultOfOperationValue value) {
        this.resultOfOperationReply = value;
    }

}
