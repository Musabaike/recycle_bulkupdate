
package com.huawei.oss.webservice.ecare.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FreeAdjustValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FreeAdjustValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subscriberID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isAdjustVolume" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="freeVolume" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="adjustType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="adjustExpiredCycleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="freeSourceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FreeAdjustValue", propOrder = {
    "subscriberID",
    "isAdjustVolume",
    "freeVolume",
    "adjustType",
    "adjustExpiredCycleId",
    "freeSourceId"
})
public class FreeAdjustValue {

    protected String subscriberID;
    protected String isAdjustVolume;
    protected BigDecimal freeVolume;
    protected String adjustType;
    protected String adjustExpiredCycleId;
    protected String freeSourceId;

    /**
     * Gets the value of the subscriberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberID() {
        return subscriberID;
    }

    /**
     * Sets the value of the subscriberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberID(String value) {
        this.subscriberID = value;
    }

    /**
     * Gets the value of the isAdjustVolume property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAdjustVolume() {
        return isAdjustVolume;
    }

    /**
     * Sets the value of the isAdjustVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAdjustVolume(String value) {
        this.isAdjustVolume = value;
    }

    /**
     * Gets the value of the freeVolume property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFreeVolume() {
        return freeVolume;
    }

    /**
     * Sets the value of the freeVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFreeVolume(BigDecimal value) {
        this.freeVolume = value;
    }

    /**
     * Gets the value of the adjustType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustType() {
        return adjustType;
    }

    /**
     * Sets the value of the adjustType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustType(String value) {
        this.adjustType = value;
    }

    /**
     * Gets the value of the adjustExpiredCycleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustExpiredCycleId() {
        return adjustExpiredCycleId;
    }

    /**
     * Sets the value of the adjustExpiredCycleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustExpiredCycleId(String value) {
        this.adjustExpiredCycleId = value;
    }

    /**
     * Gets the value of the freeSourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeSourceId() {
        return freeSourceId;
    }

    /**
     * Sets the value of the freeSourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeSourceId(String value) {
        this.freeSourceId = value;
    }

}
