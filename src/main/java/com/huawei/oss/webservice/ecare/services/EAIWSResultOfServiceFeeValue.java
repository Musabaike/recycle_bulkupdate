
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EAIWSResultOfServiceFeeValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EAIWSResultOfServiceFeeValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productFees" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSProductFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EAIWSResultOfServiceFeeValue", propOrder = {
    "productFees"
})
public class EAIWSResultOfServiceFeeValue {

    protected List<EAIWSProductFeeValue> productFees;

    /**
     * Gets the value of the productFees property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productFees property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductFees().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EAIWSProductFeeValue }
     * 
     * 
     */
    public List<EAIWSProductFeeValue> getProductFees() {
        if (productFees == null) {
            productFees = new ArrayList<EAIWSProductFeeValue>();
        }
        return this.productFees;
    }

}
