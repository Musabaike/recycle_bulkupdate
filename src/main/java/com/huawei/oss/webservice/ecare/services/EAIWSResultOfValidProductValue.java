
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EAIWSResultOfValidProductValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EAIWSResultOfValidProductValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productSubFees" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSProductFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="serviceInfos" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSServiceDetailValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="productDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isMustSign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contracts" type="{http://oss.huawei.com/webservice/ecare/services}EAIWSContractValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="relationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EAIWSResultOfValidProductValue", propOrder = {
    "productID",
    "productName",
    "productSubFees",
    "serviceInfos",
    "productDescription",
    "isMustSign",
    "contracts",
    "relationType"
})
public class EAIWSResultOfValidProductValue {

    protected String productID;
    protected String productName;
    protected List<EAIWSProductFeeValue> productSubFees;
    protected List<EAIWSServiceDetailValue> serviceInfos;
    protected String productDescription;
    protected String isMustSign;
    protected List<EAIWSContractValue> contracts;
    protected String relationType;

    /**
     * Gets the value of the productID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductID(String value) {
        this.productID = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductName(String value) {
        this.productName = value;
    }

    /**
     * Gets the value of the productSubFees property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productSubFees property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductSubFees().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EAIWSProductFeeValue }
     * 
     * 
     */
    public List<EAIWSProductFeeValue> getProductSubFees() {
        if (productSubFees == null) {
            productSubFees = new ArrayList<EAIWSProductFeeValue>();
        }
        return this.productSubFees;
    }

    /**
     * Gets the value of the serviceInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EAIWSServiceDetailValue }
     * 
     * 
     */
    public List<EAIWSServiceDetailValue> getServiceInfos() {
        if (serviceInfos == null) {
            serviceInfos = new ArrayList<EAIWSServiceDetailValue>();
        }
        return this.serviceInfos;
    }

    /**
     * Gets the value of the productDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * Sets the value of the productDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductDescription(String value) {
        this.productDescription = value;
    }

    /**
     * Gets the value of the isMustSign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMustSign() {
        return isMustSign;
    }

    /**
     * Sets the value of the isMustSign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMustSign(String value) {
        this.isMustSign = value;
    }

    /**
     * Gets the value of the contracts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contracts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContracts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EAIWSContractValue }
     * 
     * 
     */
    public List<EAIWSContractValue> getContracts() {
        if (contracts == null) {
            contracts = new ArrayList<EAIWSContractValue>();
        }
        return this.contracts;
    }

    /**
     * Gets the value of the relationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationType() {
        return relationType;
    }

    /**
     * Sets the value of the relationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationType(String value) {
        this.relationType = value;
    }

}
