
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RemoveCorpAPNMemberReply" type="{http://oss.huawei.com/webservice/ecare/services}ResultOfOperationValue"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "removeCorpAPNMemberReply"
})
@XmlRootElement(name = "removeCorpAPNMemberResponse")
public class RemoveCorpAPNMemberResponse {

    @XmlElement(name = "RemoveCorpAPNMemberReply", required = true)
    protected ResultOfOperationValue removeCorpAPNMemberReply;

    /**
     * Gets the value of the removeCorpAPNMemberReply property.
     * 
     * @return
     *     possible object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public ResultOfOperationValue getRemoveCorpAPNMemberReply() {
        return removeCorpAPNMemberReply;
    }

    /**
     * Sets the value of the removeCorpAPNMemberReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultOfOperationValue }
     *     
     */
    public void setRemoveCorpAPNMemberReply(ResultOfOperationValue value) {
        this.removeCorpAPNMemberReply = value;
    }

}
