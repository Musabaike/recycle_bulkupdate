
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AccessSessionRequest" type="{http://oss.huawei.com/webservice/ecare/services}AccessSessionValue"/&gt;
 *         &lt;element name="GetOutstandingRequest" type="{http://oss.huawei.com/webservice/ecare/services}GetOutstandingRequestDetails"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accessSessionRequest",
    "getOutstandingRequest"
})
@XmlRootElement(name = "getOutstanding")
public class GetOutstanding {

    @XmlElement(name = "AccessSessionRequest", required = true)
    protected AccessSessionValue accessSessionRequest;
    @XmlElement(name = "GetOutstandingRequest", required = true)
    protected GetOutstandingRequestDetails getOutstandingRequest;

    /**
     * Gets the value of the accessSessionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link AccessSessionValue }
     *     
     */
    public AccessSessionValue getAccessSessionRequest() {
        return accessSessionRequest;
    }

    /**
     * Sets the value of the accessSessionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessSessionValue }
     *     
     */
    public void setAccessSessionRequest(AccessSessionValue value) {
        this.accessSessionRequest = value;
    }

    /**
     * Gets the value of the getOutstandingRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetOutstandingRequestDetails }
     *     
     */
    public GetOutstandingRequestDetails getGetOutstandingRequest() {
        return getOutstandingRequest;
    }

    /**
     * Sets the value of the getOutstandingRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetOutstandingRequestDetails }
     *     
     */
    public void setGetOutstandingRequest(GetOutstandingRequestDetails value) {
        this.getOutstandingRequest = value;
    }

}
