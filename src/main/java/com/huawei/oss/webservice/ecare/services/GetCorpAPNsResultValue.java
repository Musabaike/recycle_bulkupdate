
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCorpAPNsResultValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCorpAPNsResultValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="corpAPNs" type="{http://oss.huawei.com/webservice/ecare/services}CorpAPNValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCorpAPNsResultValue", propOrder = {
    "corpAPNs"
})
public class GetCorpAPNsResultValue {

    protected List<CorpAPNValue> corpAPNs;

    /**
     * Gets the value of the corpAPNs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the corpAPNs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorpAPNs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorpAPNValue }
     * 
     * 
     */
    public List<CorpAPNValue> getCorpAPNs() {
        if (corpAPNs == null) {
            corpAPNs = new ArrayList<CorpAPNValue>();
        }
        return this.corpAPNs;
    }

}
