package com.yannitech.recycle.bean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Iterator;

import javax.xml.ws.Holder;

import org.apache.camel.Consume;
import org.apache.camel.Header;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huawei.oss.webservice.ecare.services.AccessSessionValue;
import com.huawei.oss.webservice.ecare.services.Boss4ECareInterfaceService;
import com.huawei.oss.webservice.ecare.services.Boss4ECareInterfaceServicePortType;
import com.huawei.oss.webservice.ecare.services.ManageResourceRequestValue;
import com.huawei.oss.webservice.ecare.services.QuerySimMsisdnResRequestValue;
import com.huawei.oss.webservice.ecare.services.QuerySimMsisdnResResultValue;
import com.huawei.oss.webservice.ecare.services.ResultOfOperationValue;
import com.yannitech.recycle.util.RecycleUtils;

public class ExcelFileConsumer {
	
	Logger log = LoggerFactory.getLogger(ExcelFileConsumer.class.getName());
	
	 @Consume(uri = "{{excel.routein}}") 
	 public void route(@Header("CamelFileAbsolutePath") String filePath, @Header("CamelFileNameOnly") String fileName) throws Exception {
		 
		 if(fileName.endsWith(".csv")) {
			 ProcessCSV(filePath, fileName);
		 }else if (fileName.endsWith(".xlsx")) {
			 ProcessExcel(filePath, fileName);
		 }
		 
	 }

	/*
	 * @Consume(uri = "{{excel.routein}}") public void
	 * route(@Header("CamelFileAbsolutePath") String
	 * filePath, @Header("CamelFileNameOnly") String fileName) throws Exception {
	 * 
	 * try{
	 * 
	 * File in = new File(filePath);
	 * 
	 * log.info("------------------- " + filePath + " -----------------");
	 * 
	 * XSSFWorkbook workbook = XSSFWorkbookFactory.createWorkbook(in, true);//new
	 * XSSFWorkbook(in);
	 * 
	 * XSSFWorkbook workbook2 = new XSSFWorkbook(); XSSFSheet sheet =
	 * workbook2.createSheet("Completed MSISDN");
	 * 
	 * Sheet datatypeSheet = workbook.getSheetAt(0);
	 * 
	 * Iterator<Row> iterator = datatypeSheet.iterator();
	 * 
	 * int rowNum = 0;
	 * 
	 * while (iterator.hasNext()) {
	 * 
	 * Row row = sheet.createRow(rowNum++); int colNum = 0;
	 * 
	 * Row currentRow = iterator.next(); Iterator<Cell> cellIterator =
	 * currentRow.iterator();
	 * 
	 * while (cellIterator.hasNext()) {
	 * 
	 * Cell currentCell = cellIterator.next(); Cell cell = row.createCell(colNum++);
	 * 
	 * if (currentCell.getCellType() == CellType.STRING) {
	 * cell.setCellValue(currentCell.getStringCellValue()); } else if
	 * (currentCell.getCellType() == CellType.NUMERIC) {
	 * cell.setCellValue(currentCell.getNumericCellValue()); } } if(rowNum==1) {
	 * Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
	 * cell.setCellValue(RecycleUtils.COLUMN_STATUS_HEADER); }else {
	 * 
	 * if (!checkIfRowIsEmpty(currentRow)) { String msisdn = null; try { msisdn =
	 * currentRow.getCell(1).getStringCellValue(); if
	 * (RecycleUtils.checkRange(msisdn) && RecycleUtils.onlyDigits(msisdn)) {
	 * if(isCorrectStatus(msisdn)) { log.info("UPDATE MSISDN " + msisdn +
	 * "\n=====================" ); manageSimMsisdnResource(msisdn, "1", "0", "2");
	 * log.info("CHANGE TO NORMAL REQUEST " + msisdn + " \n=====================");
	 * manageSimMsisdnResource(msisdn, "2", "0", "2");
	 * log.info("MOVE TO TM PROD REQUEST " + msisdn + "\n=====================");
	 * ResultOfOperationValue resp = manageSimMsisdnResource(msisdn, "0", "0",
	 * "10181"); String res; if
	 * (RecycleUtils.SUCCESSFUL.equalsIgnoreCase(resp.getResultMessage())) { res =
	 * RecycleUtils.SUCCESSFUL; }else { res = RecycleUtils.FAIL; } Cell cell =
	 * row.createCell(RecycleUtils.COLUMN_STATUS); cell.setCellValue(res);
	 * //log.info("QUERY STATUS " + msisdn + "\n=====================");
	 * //Holder<QuerySimMsisdnResResultValue> response =
	 * getSimMsisdnResourceReplyStatus(msisdn, "0"); //Cell cell =
	 * row.createCell(colNum);
	 * //cell.setCellValue(response.value.getResStatus().toString()); }else { Cell
	 * cell = row.createCell(RecycleUtils.COLUMN_STATUS);
	 * cell.setCellValue(RecycleUtils.NOTAVAILABLEFORRECYCLE_MSG); }
	 * 
	 * }else { Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
	 * cell.setCellValue(RecycleUtils.INVALIDRANGE_MSG); }
	 * }catch(IllegalStateException e) { log.info(e.toString()); Cell cell =
	 * row.createCell(RecycleUtils.COLUMN_STATUS);
	 * cell.setCellValue(RecycleUtils.FAIL); }catch(Exception e) {
	 * log.info(e.toString()); Cell cell =
	 * row.createCell(RecycleUtils.COLUMN_STATUS);
	 * cell.setCellValue(RecycleUtils.FAIL); } } } }
	 * 
	 * String[] arrOfStr = in.toString().split("/"); String dir =
	 * RecycleUtils.COMPLETEDLIST_DIR; String localFile = dir +"/" +
	 * arrOfStr[arrOfStr.length-1]; localFile = localFile.replace(".xlsx",
	 * "_processed.xlsx"); //String tempFile = "/tmp/"+ arrOfStr[arrOfStr.length-1];
	 * log.info("Writing to : " +localFile); FileOutputStream outputStream = new
	 * FileOutputStream(localFile);
	 * 
	 * 
	 * workbook2.write(outputStream);
	 * 
	 * workbook2.close();
	 * 
	 * } catch (FileNotFoundException e) {
	 * log.info("FileNotException in processor is : " + e); e.printStackTrace(); }
	 * catch (IOException e) { log.info("IOException in processor is : " + e);
	 * e.printStackTrace(); }catch (Exception e){
	 * log.info("Exception in processor is : " + e); e.printStackTrace(); } }
	 */
	
	private String processMSISDN(String msisdn) {
		
		if (RecycleUtils.checkRange(msisdn) && RecycleUtils.onlyDigits(msisdn)) {
			if(isCorrectStatus(msisdn)) {
				log.info("UPDATE MSISDN " + msisdn + "\n=====================" );
				manageSimMsisdnResource(msisdn, "1", "0", "2");
				log.info("CHANGE TO NORMAL REQUEST " + msisdn + " \n=====================");
				manageSimMsisdnResource(msisdn, "2", "0", "2");
		    	log.info("MOVE TO TM PROD REQUEST " + msisdn + "\n=====================");
		    	ResultOfOperationValue resp = manageSimMsisdnResource(msisdn, "0", "0", "10181");
		    	//String res;
		    	if (RecycleUtils.SUCCESSFUL.equalsIgnoreCase(resp.getResultMessage())) {
		    		//res = RecycleUtils.SUCCESSFUL;
		    		return RecycleUtils.SUCCESSFUL;
		    	}else {
		    		//res = RecycleUtils.FAIL;	
		    		return RecycleUtils.FAIL;
		    	}
			}else {
    		   // Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
              //  cell.setCellValue(RecycleUtils.NOTAVAILABLEFORRECYCLE_MSG);
				return RecycleUtils.NOTAVAILABLEFORRECYCLE_MSG;
    		}   
		}else {
		    //Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
            //cell.setCellValue(RecycleUtils.INVALIDRANGE_MSG);
			return RecycleUtils.INVALIDRANGE_MSG;
		}
	}
	
	private void ProcessCSV(String filePath, String fileName) {
		
		log.info("Reading Order from SAP : " +fileName );
		try{
			BufferedReader br = null;
			XSSFWorkbook workbook2 = new XSSFWorkbook();
		    XSSFSheet sheet = workbook2.createSheet("Completed MSISDN");
		    
		    int rowNum = 0;

				br = new BufferedReader(new FileReader(filePath));					
				String st; 
				while ((st = br.readLine()) != null) {
					  Row row = sheet.createRow(rowNum++);
					  
					  if (st.length() > 0) {					
						  
						  	String[] items = st.split(",");
						  	
						  	String msisdn = null;
						  	if(items.length > 1) {
						  		msisdn = items[1];
						  	}
							
							String res = "";
							if(rowNum == 1) {
								res = RecycleUtils.COLUMN_STATUS_HEADER;
							}else {
								if(msisdn != null) {
									res = processMSISDN(msisdn);
								}									
							}
							
							for(int i=0; i<items.length; i++){
								Cell cell = row.createCell(i);
						        cell.setCellValue(items[i]);
							}
						    
						
							/*
							 * cell = row.createCell(RecycleUtils.COLUMN_SERVICE_ID);
							 * cell.setCellValue(items[RecycleUtils.COLUMN_SERVICE_ID]); cell =
							 * row.createCell(RecycleUtils.COLUMN_SUBSCRIBER_ID);
							 * cell.setCellValue(items[RecycleUtils.COLUMN_SUBSCRIBER_ID]); cell =
							 * row.createCell(RecycleUtils.COLUMN_RESULT_CODE);
							 * cell.setCellValue(items[RecycleUtils.COLUMN_RESULT_CODE]); cell =
							 * row.createCell(RecycleUtils.COLUMN_BULK_CEASED_DATE);
							 * cell.setCellValue(items[RecycleUtils.COLUMN_BULK_CEASED_DATE]); cell =
							 * row.createCell(RecycleUtils.COLUMN_STATUS); cell.setCellValue(res);
							 * 	cell.setCellValue(items[RecycleUtils.COLUMN_BULK_CEASED_DATE]); 
							 */							
						
							Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS); 
							cell.setCellValue(res);
					  }
				}
					
					File in = new File(filePath);
				  	String[] arrOfStr = in.toString().split("/");
		            String dir = RecycleUtils.COMPLETEDLIST_DIR; 
		    		String localFile = dir +"/" + arrOfStr[arrOfStr.length-1];
		    		localFile = localFile.replace(".csv", "_processed.xlsx");
		    		//String tempFile = "/tmp/"+ arrOfStr[arrOfStr.length-1];
		    		log.info("Writing to : " +localFile);
		            FileOutputStream outputStream = new FileOutputStream(localFile);		           
	
		            workbook2.write(outputStream);
		           
		            workbook2.close();
		            br.close();
			
	    } catch (FileNotFoundException e) {
	    	log.info("FileNotException in processor is : " + e);
	        e.printStackTrace();
	    } catch (IOException e) {
	    	log.info("IOException in processor is : " + e);
	        e.printStackTrace();
	    }catch (Exception e){
	    	log.info("Exception in processor is : " + e);
	        e.printStackTrace();
	    }
	}
	
	private void ProcessExcel(String filePath, String fileName) {
		 
		try{
		      
	        	File in = new File(filePath);	        	
	        	
	        	log.info("------------------- " + filePath + " -----------------");
     	
	        	XSSFWorkbook workbook = XSSFWorkbookFactory.createWorkbook(in, true);//new XSSFWorkbook(in);
	        	
	            XSSFWorkbook workbook2 = new XSSFWorkbook();
		        XSSFSheet sheet = workbook2.createSheet("Completed MSISDN");
	        	
	            Sheet datatypeSheet = workbook.getSheetAt(0);
	           
	            Iterator<Row> iterator = datatypeSheet.iterator();
	       
		        int rowNum = 0;

	            while (iterator.hasNext()) {
	            	
	            	Row row = sheet.createRow(rowNum++);
	 	           // int colNum = 0;
	 	            
	                Row currentRow = iterator.next();
	                Iterator<Cell> cellIterator = currentRow.iterator();                
	                
	                while (cellIterator.hasNext()) {

	                    Cell currentCell = cellIterator.next();  
	                    int colNum = currentCell.getColumnIndex();
	                    Cell cell = row.createCell(colNum++);
	                
	                   if (currentCell.getCellType() == CellType.STRING) {
	                	   cell.setCellValue(currentCell.getStringCellValue());
	                    } else if (currentCell.getCellType() == CellType.NUMERIC) {
	                    	cell.setCellValue(currentCell.getNumericCellValue());
	                    }                   
	                }
	                if(rowNum==1) {
	                    Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
	                    cell.setCellValue(RecycleUtils.COLUMN_STATUS_HEADER);
	                }else {                	
	                	
	                	if (!checkIfRowIsEmpty(currentRow)) {
	                		String msisdn = null;
	                		try {
	                			msisdn = currentRow.getCell(1).getStringCellValue();
	                			String res = processMSISDN(msisdn);
	                			Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
		                        cell.setCellValue(res);
	                       		
	                		}catch(IllegalStateException e) {
	                			log.info(e.toString());
	                			Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
	                			cell.setCellValue(RecycleUtils.FAIL);
	                		}catch(Exception e) {
	                			log.info(e.toString());
	                			Cell cell = row.createCell(RecycleUtils.COLUMN_STATUS);
	                			cell.setCellValue(RecycleUtils.FAIL);
	                		}	                  
	                	}               
	                }        
	            }            

	            String[] arrOfStr = in.toString().split("/");
	            String dir = RecycleUtils.COMPLETEDLIST_DIR; 
	    		String localFile = dir +"/" + arrOfStr[arrOfStr.length-1];
	    		localFile = localFile.replace(".xlsx", "_processed.xlsx");
	    		//String tempFile = "/tmp/"+ arrOfStr[arrOfStr.length-1];
	    		log.info("Writing to : " +localFile);
	            FileOutputStream outputStream = new FileOutputStream(localFile);	           

	            workbook2.write(outputStream);	           
	            workbook2.close();
	            
	        } catch (FileNotFoundException e) {
	        	log.info("FileNotException in processor is : " + e);
	            e.printStackTrace();
	        } catch (IOException e) {
	        	log.info("IOException in processor is : " + e);
	            e.printStackTrace();
	        }catch (Exception e){
	        	log.info("Exception in processor is : " + e);
	            e.printStackTrace();
	        }
	}
	
	
	private Boss4ECareInterfaceServicePortType getPort() {
		
		URL wsdlURL = Boss4ECareInterfaceService.WSDL_LOCATION;
		Boss4ECareInterfaceService ss = new Boss4ECareInterfaceService(wsdlURL, RecycleUtils.SERVICE_NAME);
        Boss4ECareInterfaceServicePortType port = ss.getBoss4ECareInterfaceServiceSOAP11PortHttp();
		return port;
	}
	
	private AccessSessionValue getAccessSessionRequest() {
	   
		AccessSessionValue accessSessionRequest = new AccessSessionValue();
        accessSessionRequest.setAccessChannel(RecycleUtils.ACCESSCHANNEL);
        accessSessionRequest.setOperatorCode(RecycleUtils.OPERATORCODE);
        accessSessionRequest.setPassword(RecycleUtils.PSSD);
        
        return accessSessionRequest;
	}
	
	private ResultOfOperationValue manageSimMsisdnResource(String msisdn, String opType, String resType, String updatedValue) {
		
        ManageResourceRequestValue manageSimMsisdnResourceRequest = new ManageResourceRequestValue();
        manageSimMsisdnResourceRequest.setOperType(opType);
        manageSimMsisdnResourceRequest.setResCode(msisdn);
        manageSimMsisdnResourceRequest.setResType(resType);
        manageSimMsisdnResourceRequest.setUpdatedValue(updatedValue); 
        
        Boss4ECareInterfaceServicePortType port = getPort();
        addInInterceptors(port);
	        
	    ResultOfOperationValue _manageSimMsisdnResource__return = port.manageSimMsisdnResource(getAccessSessionRequest(), manageSimMsisdnResourceRequest);
		
		log.info("Result code : " +_manageSimMsisdnResource__return.getResultCode());
		log.info("Result message : " +_manageSimMsisdnResource__return.getResultMessage());		
		
		return _manageSimMsisdnResource__return;
		
	}
	
	private void addInInterceptors(Boss4ECareInterfaceServicePortType port) {
		
		Client client = ClientProxy.getClient(port);
	    client.getInInterceptors().add(new LoggingInInterceptor());	   
	    client.getOutInterceptors().add(new LoggingOutInterceptor());
	}
	
	private boolean checkIfRowIsEmpty(Row row) {
	    if (row == null) {
	        return true;
	    }
	    if (row.getLastCellNum() <= 0) {
	        return true;
	    }
	    for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
	        Cell cell = row.getCell(cellNum);
	        if (cell != null && cell.getCellType() != CellType.BLANK && !cell.toString().isEmpty()) {
	            return false;
	        }
	    }
	    return true;
	}
	
	private Holder<QuerySimMsisdnResResultValue> getSimMsisdnResourceReplyStatus(String msisdn, String resType)   {	

        QuerySimMsisdnResRequestValue request = new QuerySimMsisdnResRequestValue();
    	request.setResCode(msisdn);
		request.setResType(resType);
        Holder<ResultOfOperationValue> resultOfOperationReply = new Holder<ResultOfOperationValue>();
        Holder<QuerySimMsisdnResResultValue> simMsisdnResourceReply = new Holder<QuerySimMsisdnResResultValue>();
        
        Boss4ECareInterfaceServicePortType port = getPort();
        addInInterceptors(port);
        
        port.getSimMsisdnResource(getAccessSessionRequest(), request, resultOfOperationReply, simMsisdnResourceReply);
       
        return simMsisdnResourceReply;

    }		

	private boolean isCorrectStatus(String msisdn) {
		
		Holder<QuerySimMsisdnResResultValue> response = getSimMsisdnResourceReplyStatus(msisdn, "0");
		String dept = response.value.getDepartName();
		BigDecimal status = response.value.getResStatus();
		String mnpStatus = response.value.getMnpStatus();
		
		log.info("Department --> " + dept + " | Status --> " + status + " | MNPStatus --> " + mnpStatus + "  <----" );

		if(dept != null) {
			   if (dept.equalsIgnoreCase("HQ")) {
				   if (status.compareTo(new BigDecimal("5")) == 0) {	            	
			            	return true;
			       }	            	
			   }else  if (dept.equalsIgnoreCase("Telkom Mobile Production") || dept.equalsIgnoreCase("Telkom Mobile Vanity")) {
				   if (mnpStatus.equalsIgnoreCase("0")) {
					   return true;
				   }
			   }
		}
		
		return false;
	}

}
