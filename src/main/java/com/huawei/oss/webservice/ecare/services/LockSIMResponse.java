
package com.huawei.oss.webservice.ecare.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LockSIMResply" type="{http://oss.huawei.com/webservice/ecare/services}LockSIMResplyDetail"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lockSIMResply"
})
@XmlRootElement(name = "lockSIMResponse")
public class LockSIMResponse {

    @XmlElement(name = "LockSIMResply", required = true)
    protected LockSIMResplyDetail lockSIMResply;

    /**
     * Gets the value of the lockSIMResply property.
     * 
     * @return
     *     possible object is
     *     {@link LockSIMResplyDetail }
     *     
     */
    public LockSIMResplyDetail getLockSIMResply() {
        return lockSIMResply;
    }

    /**
     * Sets the value of the lockSIMResply property.
     * 
     * @param value
     *     allowed object is
     *     {@link LockSIMResplyDetail }
     *     
     */
    public void setLockSIMResply(LockSIMResplyDetail value) {
        this.lockSIMResply = value;
    }

}
