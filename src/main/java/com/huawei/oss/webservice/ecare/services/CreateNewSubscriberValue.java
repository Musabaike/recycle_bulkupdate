
package com.huawei.oss.webservice.ecare.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateNewSubscriberValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateNewSubscriberValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerInfo" type="{http://oss.huawei.com/webservice/ecare/services}CustomerValue"/&gt;
 *         &lt;element name="accountInfo" type="{http://oss.huawei.com/webservice/ecare/services}AccountValue"/&gt;
 *         &lt;element name="subscriberInfo" type="{http://oss.huawei.com/webservice/ecare/services}SubscriberValue"/&gt;
 *         &lt;element name="rateplanInfo" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" minOccurs="0"/&gt;
 *         &lt;element name="servicePackageInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderProductValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="contractInfos" type="{http://oss.huawei.com/webservice/ecare/services}OrderContractValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="feeInfos" type="{http://oss.huawei.com/webservice/ecare/services}BusinessFeeValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="prepaidActivationFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateNewSubscriberValue", propOrder = {
    "customerInfo",
    "accountInfo",
    "subscriberInfo",
    "rateplanInfo",
    "servicePackageInfos",
    "contractInfos",
    "feeInfos",
    "prepaidActivationFlag"
})
public class CreateNewSubscriberValue {

    @XmlElement(required = true)
    protected CustomerValue customerInfo;
    @XmlElement(required = true)
    protected AccountValue accountInfo;
    @XmlElement(required = true)
    protected SubscriberValue subscriberInfo;
    protected OrderProductValue rateplanInfo;
    protected List<OrderProductValue> servicePackageInfos;
    protected List<OrderContractValue> contractInfos;
    protected List<BusinessFeeValue> feeInfos;
    protected String prepaidActivationFlag;

    /**
     * Gets the value of the customerInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerValue }
     *     
     */
    public CustomerValue getCustomerInfo() {
        return customerInfo;
    }

    /**
     * Sets the value of the customerInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerValue }
     *     
     */
    public void setCustomerInfo(CustomerValue value) {
        this.customerInfo = value;
    }

    /**
     * Gets the value of the accountInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountValue }
     *     
     */
    public AccountValue getAccountInfo() {
        return accountInfo;
    }

    /**
     * Sets the value of the accountInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountValue }
     *     
     */
    public void setAccountInfo(AccountValue value) {
        this.accountInfo = value;
    }

    /**
     * Gets the value of the subscriberInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriberValue }
     *     
     */
    public SubscriberValue getSubscriberInfo() {
        return subscriberInfo;
    }

    /**
     * Sets the value of the subscriberInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriberValue }
     *     
     */
    public void setSubscriberInfo(SubscriberValue value) {
        this.subscriberInfo = value;
    }

    /**
     * Gets the value of the rateplanInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderProductValue }
     *     
     */
    public OrderProductValue getRateplanInfo() {
        return rateplanInfo;
    }

    /**
     * Sets the value of the rateplanInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderProductValue }
     *     
     */
    public void setRateplanInfo(OrderProductValue value) {
        this.rateplanInfo = value;
    }

    /**
     * Gets the value of the servicePackageInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicePackageInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePackageInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderProductValue }
     * 
     * 
     */
    public List<OrderProductValue> getServicePackageInfos() {
        if (servicePackageInfos == null) {
            servicePackageInfos = new ArrayList<OrderProductValue>();
        }
        return this.servicePackageInfos;
    }

    /**
     * Gets the value of the contractInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderContractValue }
     * 
     * 
     */
    public List<OrderContractValue> getContractInfos() {
        if (contractInfos == null) {
            contractInfos = new ArrayList<OrderContractValue>();
        }
        return this.contractInfos;
    }

    /**
     * Gets the value of the feeInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessFeeValue }
     * 
     * 
     */
    public List<BusinessFeeValue> getFeeInfos() {
        if (feeInfos == null) {
            feeInfos = new ArrayList<BusinessFeeValue>();
        }
        return this.feeInfos;
    }

    /**
     * Gets the value of the prepaidActivationFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrepaidActivationFlag() {
        return prepaidActivationFlag;
    }

    /**
     * Sets the value of the prepaidActivationFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrepaidActivationFlag(String value) {
        this.prepaidActivationFlag = value;
    }

}
